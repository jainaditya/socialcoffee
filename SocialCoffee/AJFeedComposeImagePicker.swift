//
//  AJFeedComposeImagePicker.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/8/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit

extension AJFeedComposeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func openCamera()
    {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            imagePickerController!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePickerController!, animated: true, completion: nil)
        } else {
            self.openPhotoLibrary()
        }
    }
    
    func openPhotoLibrary()
    {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
            imagePickerController!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(imagePickerController!, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!)
    {
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.feedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.chooseImageButton.imageView?.contentMode =  .scaleAspectFill
        self.chooseImageButton.imageView?.layer.cornerRadius = kCameraImageWH / 2
        self.chooseImageButton.setImage(self.feedImage, for: UIControlState())
        self.imagePickerController?.dismiss(animated: true, completion: nil)
        if (picker.sourceType == UIImagePickerControllerSourceType.camera) {
            if let image = self.feedImage {
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.imagePickerController?.dismiss(animated: true, completion: nil)
    }
    
}
