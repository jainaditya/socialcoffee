
//
//  AJFeedComposeViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/27/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

let kCameraImageWH = CGFloat(64)

class AJFeedComposeViewController: AJViewController, UITextViewDelegate, UIPopoverControllerDelegate, FeedRequestSynchronizerDelegate, RequestNearByPlacesDelegate, UIAlertViewDelegate
{
    var feedComposeView: AJFeedComposeView!
    let feedTextView = UITextView()
    var chooseImageButton = UIButton(type: UIButtonType.custom)
    let kFeedTextViewPlaceholderString = "Let people know about this place...This place is amazing, buzzing and crowded..."
    let kFeedViewPadding:CGFloat = 10
    var imagePickerController: UIImagePickerController? =  UIImagePickerController()
    var feedImage: UIImage?
    var feedRequestSynchronizer: AJFeedRequestSynchronizer?
    var keyboardHeight: CGRect = CGRect()
    var requestNearByPlaces: AJRequestNearByPlaces?
    var progressView: UIProgressView?
    var selectedPlace: AJPlace?

    override func viewDidLoad() {
        super.viewDidLoad()
        if super.isModal() {
//            self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: #selector(AJFeedComposeViewController.tappedReturnHome)), animated: false)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AJFeedComposeViewController.tappedPostFeed))
        }
        imagePickerController?.delegate = self
        let screenSize: CGRect = UIScreen.main.bounds
        self.feedComposeView = AJFeedComposeView(frame: CGRect(x: 0, y: 0, width: screenSize.width - 10, height: 10))
        self.view.addSubview(self.feedComposeView)

        self.createSubviews()
        self.feedTextView.enablesReturnKeyAutomatically = false

        NotificationCenter.default.addObserver(self, selector: #selector(AJFeedComposeViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardDidChangeFrame, object: self.view.window)

        self.feedComposeView.selectedPlaceLabel.text = self.selectedPlace?.name

//        Add this back later with better handling if there are no places found
//        requestNearByPlaces = AJRequestNearByPlaces()
//        requestNearByPlaces?.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.feedTextView.becomeFirstResponder()
        //self.toggleSendButton(self.feedImage != nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.feedTextView.resignFirstResponder()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
    }
    
    func createSubviews() {

        self.feedTextView.text = kFeedTextViewPlaceholderString
        self.feedTextView.textColor = UIColor.lightGray
        self.feedTextView.delegate = self
        self.feedTextView.becomeFirstResponder()

        self.chooseImageButton.layer.borderWidth = 2
        self.chooseImageButton.layer.borderColor = UIColor.secondaryColor().cgColor
        self.chooseImageButton.layer.cornerRadius = kCameraImageWH / 2
        self.chooseImageButton.frame = CGRect(x: self.view.frame.size.width / 2 - kCameraImageWH / 2, y: self.view.frame.size.height, width: kCameraImageWH, height: kCameraImageWH)
        self.setChooseButtonNormalImage()
        self.chooseImageButton.addTarget(self, action: #selector(AJFeedComposeViewController.tappedChooseImage(_:)), for: UIControlEvents.touchUpInside)
        self.view.addSubview(feedTextView)
        self.view.addSubview(chooseImageButton)
        
        self.feedTextView.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(20)
        self.chooseImageButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoLightWithSize(16)
        self.chooseImageButton.isHidden = true

        self.progressView = UIProgressView(progressViewStyle: UIProgressViewStyle.default)
        self.progressView?.progressTintColor = UIColor.primaryColor()
        self.progressView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width,height: 6)
        self.progressView?.trackTintColor = UIColor.white
        self.view.addSubview(self.progressView!)
    }

    func tappedReturnHome() {
        super.returnToPreviousView()
    }
    
    func tappedPostFeed() {
        feedRequestSynchronizer = AJFeedRequestSynchronizer(feedImage: self.feedImage, feedMessage: feedTextView.text, place: self.selectedPlace!,params: [:])
        self.feedRequestSynchronizer!.feedSaveDelegate =  self
        if (feedTextView.text.characters.count > 0 && feedTextView.text != kFeedTextViewPlaceholderString) {
            self.feedRequestSynchronizer!.performFeedSaveOperation()
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

    @objc func feedSaveOperationWithImage(_ progress:Float) {
        self.progressView?.setProgress(progress, animated: true)
    }
    
    func feedSaveOperationDidEnd() {
        super.returnToPreviousView()
    }

    @objc func feedSaveOperationFailed(_ error: AnyObject) {
        if error.isKind(of: NSDictionary.self) {
            if let message = error["message"] as? String {
                UIAlertView(title: "Error", message: message, delegate: self, cancelButtonTitle: "Okay").show()
            }
        }
        self.feedSaveOperationDidEnd()
    }

    func tappedChooseImage(_ sender: UIButton!) {
        let alert: UIAlertController = UIAlertController(title: "Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) {
            UIAlertAction in self.openCamera()
        }
        
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in self.openPhotoLibrary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }

        if (self.feedImage != nil) {
            let removeImageAction = UIAlertAction(title: "Remove", style: .destructive) {
                UIAlertAction in self.setChooseButtonNormalImage()
            }
            alert.addAction(removeImageAction)
        }

        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    func setChooseButtonNormalImage() {
        if(self.feedImage != nil) {
            self.chooseImageButton.setImage(self.feedImage, for: UIControlState())
            self.feedImage = nil
        }
        self.toggleSendButton(false)
        self.chooseImageButton.imageView?.layer.cornerRadius = 0
        self.chooseImageButton.setImage(UIImage(named: "camera"), for: UIControlState())
    }

    //mark - UITextView delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        resetFeedTextViewRange()
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.text == kFeedTextViewPlaceholderString {
            resetFeedTextViewRange()
            self.toggleSendButton(false)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.characters.count == 0 {
            textView.textColor = UIColor.secondaryTextColor()
            textView.text = kFeedTextViewPlaceholderString
            resetFeedTextViewRange()
        }

        let string = textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if string.characters.count > 0  && string != kFeedTextViewPlaceholderString {
            self.toggleSendButton(true)
        }else {
            self.toggleSendButton(false)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") { return false }

        if textView.text == kFeedTextViewPlaceholderString && text != "" {
            textView.textColor = UIColor.primaryTextColor()
            textView.text = text
            textView.text.removeAll(keepingCapacity: false)
        }
        return true
    }

    fileprivate func resetFeedTextViewRange() {
        feedTextView.selectedRange = NSMakeRange(0, 0)
    }

    fileprivate func toggleSendButton(_ enable: Bool) {
//        if self.feedImage != nil {
//            enable = true
//        }
        self.navigationItem.rightBarButtonItem?.isEnabled = enable
    }

    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }

    func keyboardWillShow(_ sender: Notification) {
//        if self.chooseImageButton.hidden == false {
//            return
//        }
        self.chooseImageButton.isHidden = false
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let keyboardRect: CGRect = ((userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue)!
        self.keyboardHeight =  keyboardRect
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size

        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.chooseImageButton.frame.origin.y = keyboardRect.origin.y - 140
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.chooseImageButton.frame.origin.y += keyboardSize.height - offset.height
            })
        }
        self.feedTextView.frame = CGRect(x: 10, y: 35, width: self.view.frame.size.width - 20, height: self.view.frame.size.height / 2 - kCameraImageWH - 30)
    }

    func didReceiveNearByPlaces(_ responseObjects: JSON) {
        for (_, placeDictionary): (String, JSON) in responseObjects {
            print(placeDictionary["name"])
        }
    }

    func didFailNearByPlaces() {

    }
}
