//
//  AJFeedImageTableViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/29/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import AlamofireImage
import Cartography

class AJFeedImageTableViewCell: AJFeedTableViewCell {

    let imageCache = AutoPurgingImageCache(
        memoryCapacity: 100_000_000,
        preferredMemoryUsageAfterPurge: 60_000_000
    )


    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier!)
    }
    
    override func createSubviews() {
        super.createSubviews()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
//        contentView.translatesAutoresizingMaskIntoConstraints = false
        feedImageView.translatesAutoresizingMaskIntoConstraints = false
        constrain(feedImageView, feedMessageLabel) { feedImageView, feedMessageLabel in
            feedImageView.width == (feedMessageLabel.superview?.superview!.width)!
            feedImageView.height == 250
            feedImageView.top == feedMessageLabel.bottom + 60
        }
        super.updateConstraints()
    }
    
    override func populateWithFeed(_ feed: Feed, feedViewController: Any) {
        downloadImage(feed)
        super.populateWithFeed(feed, feedViewController: feedViewController)
        self.layoutIfNeeded()
    }
    
    func downloadImage(_ feed: Feed) {
        if var imageURL = feed.image?.path {
            imageURL = imageURL.replacingOccurrences(of: "http://", with: "https://")
//            if let image = imageCache.image(withIdentifier: imageURL) {
//                feedImageView.image = image
//                return
//            } else {
//                let imageDownloader = UIImageView.af_sharedImageDownloader
//                imageDownloader.downloadImage(URLRequest: NSURLRequest(URL:  NSURL(string: imageURL)!)) { response in
//                    if let image = response.result.value {
//                        imageCache.addImage(image, withIdentifier: imageURL)
//                        feedImageView.image = image
//                    }
//                }
//            }

            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: feedImageView.frame.size,
                radius: 0.0
            )

            feedImageView.af_setImage(withURL:URL(string: imageURL)!, placeholderImage: UIImage(named: "placeholder"), filter: nil, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: false)

//            imageCache.add(feedImageView.image!, withIdentifier: imageURL)
        }

    }
}
