//
//  AJFeedTableViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/8/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Cartography

protocol AJFeedTableViewCellDelegate : NSObjectProtocol {
    func upVoteButtonClickedInCell(_ cell: UITableViewCell)
    func downVoteButtonClickedInCell(_ cell: UITableViewCell)
}

class AJFeedTableViewCell: UITableViewCell
{
    let nameLabel = UILabel()
    let favouritesButton =  UIButton(type: UIButtonType.custom)
    let actionButton = UIButton(type: UIButtonType.system)
    weak var feedImageView: UIImageView!
    let feedMessageLabel: UILabel = UILabel()
    let timeLabel: UILabel = UILabel()
    weak var timeImageView:  AJImagedIconView!
    var request: Alamofire.Request?
    var feedVoteCount: UILabel!
    var upVoteButton: UIButton!
    var downVoteButton: UIButton!
    var cellSeparatorView:UIView!
    var flagFeedButton: UIButton!
    let locationIconImageView: UIImageView = UIImageView()
    let selectedPlaceLabel = UILabel()
    let busyLabel = UILabel()
    let waitLabel = UILabel()

    let vicinityLabel = UILabel()

    var feedViewController: AJFeedViewController?
    
    var delegate: AJFeedTableViewCellDelegate?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier!)
        self.selectionStyle = .none
        self.isUserInteractionEnabled = true
        createSubviews()
        self.setNeedsUpdateConstraints()
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func createSubviews()
    {
        nameLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(15)
        self.nameLabel.preferredMaxLayoutWidth = self.contentView.bounds.size.width
        self.nameLabel.numberOfLines = 0
        self.contentView.addSubview(nameLabel)
        
        self.feedMessageLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(15)
        self.feedMessageLabel.numberOfLines = 0
        self.feedMessageLabel.preferredMaxLayoutWidth = self.contentView.frame.size.width
        self.feedMessageLabel.textColor =  UIColor.primaryTextColor()
        self.feedMessageLabel.sizeToFit()
        self.contentView.addSubview(feedMessageLabel)
        
        self.timeLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(12)
        self.timeLabel.numberOfLines = 0
        self.timeLabel.textColor =  UIColor.lightGray
        self.contentView.addSubview(self.timeLabel)
        
        let timeImageView = AJImagedIconView()
        self.timeImageView = timeImageView
        self.timeImageView.configureFor(.time)
        self.contentView.addSubview(self.timeImageView!)
        
        let feedImage = UIImageView()
        self.feedImageView = feedImage
        self.feedImageView.contentMode = UIViewContentMode.scaleAspectFit
        self.feedImageView.clipsToBounds =  true
        self.contentView.addSubview(self.feedImageView)
        self.feedImageView.backgroundColor = UIColor.defaultImageBackgroundColor()
        
        let feedVoteCount =  UILabel()
        self.feedVoteCount = feedVoteCount
        self.feedVoteCount.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        self.feedVoteCount.textColor =  UIColor.primaryTextColor()
        self.feedVoteCount.textAlignment = .center
        self.contentView.addSubview(self.feedVoteCount)
        self.feedVoteCount.textColor = UIColor.primaryColor()
        self.feedVoteCount.textAlignment = .center

        let upVoteButton = UIButton(type: .custom)
        upVoteButton.addTarget(self, action:#selector(AJFeedTableViewCell.upVoteButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        upVoteButton.setImage(UIImage(named: "up"), for: UIControlState())
        upVoteButton.setImage(UIImage(named: "up_selected"), for: UIControlState.selected)
        upVoteButton.tag = 1
        self.upVoteButton = upVoteButton
        self.addSubview(self.upVoteButton)

        let downVoteButton = UIButton(type: .custom)
        self.downVoteButton = downVoteButton
        self.downVoteButton.addTarget(self, action: #selector(AJFeedTableViewCell.upVoteButtonClicked(_:)), for: .touchUpInside)
        self.downVoteButton.setImage(UIImage(named: "down"), for: UIControlState())
        self.downVoteButton.tag = 2
        self.downVoteButton.setImage(UIImage(named: "down_selected"), for: UIControlState.selected)
        self.addSubview(self.downVoteButton)

        let cellSeparatorView = UIView()
        self.cellSeparatorView = cellSeparatorView
        self.cellSeparatorView.backgroundColor=UIColor.groupTableViewBackground
        self.cellSeparatorView.layer.masksToBounds=false
        self.contentView.addSubview(self.cellSeparatorView)

        let flagFeedButton = UIButton(type: .custom)
        flagFeedButton.addTarget(self, action:#selector(AJFeedTableViewCell.flagFeedButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        flagFeedButton.setImage(UIImage(named: "flag"), for: UIControlState())
        flagFeedButton.setImage(UIImage(named: "flag"), for: UIControlState.selected)
        self.flagFeedButton = flagFeedButton
        self.addSubview(self.flagFeedButton)

        self.locationIconImageView.image = UIImage(named: "location_pin")
        self.contentView.addSubview(self.locationIconImageView)

        self.selectedPlaceLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(16)
        self.selectedPlaceLabel.textColor = UIColor.black
        self.selectedPlaceLabel.numberOfLines = 0
        self.selectedPlaceLabel.preferredMaxLayoutWidth = self.contentView.frame.size.width
        self.selectedPlaceLabel.textColor =  UIColor.primaryTextColor()
        self.selectedPlaceLabel.sizeToFit()
        self.contentView.addSubview(self.selectedPlaceLabel)

        self.vicinityLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(14.0)
        self.vicinityLabel.textColor =  UIColor.lightGray
        self.contentView.addSubview(self.vicinityLabel)

        busyLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(14.0)
        busyLabel.textColor =  UIColor.white
        busyLabel.backgroundColor = UIColor.drinkCategoryColor()
        busyLabel.sizeToFit()
        busyLabel.numberOfLines = 0
        busyLabel.text = "crowded"
        busyLabel.textAlignment = .center
        contentView.addSubview(busyLabel)

        self.waitLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(14.0)
        self.waitLabel.textColor =  UIColor.lightGray
        self.contentView.addSubview(self.waitLabel)

    }

    override func updateConstraints()
    {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        feedMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        cellSeparatorView.translatesAutoresizingMaskIntoConstraints = false
        downVoteButton.translatesAutoresizingMaskIntoConstraints = false
        upVoteButton.translatesAutoresizingMaskIntoConstraints = false
        feedVoteCount.translatesAutoresizingMaskIntoConstraints = false
        feedImageView.translatesAutoresizingMaskIntoConstraints = false
        flagFeedButton.translatesAutoresizingMaskIntoConstraints = false
        locationIconImageView.translatesAutoresizingMaskIntoConstraints = false
        selectedPlaceLabel.translatesAutoresizingMaskIntoConstraints = false
        vicinityLabel.translatesAutoresizingMaskIntoConstraints = false
        busyLabel.translatesAutoresizingMaskIntoConstraints = false
        waitLabel.translatesAutoresizingMaskIntoConstraints = false

        constrain(contentView, locationIconImageView, selectedPlaceLabel, vicinityLabel, busyLabel) { view, locationIconImageView, selectedPlaceLabel, vicinityLabel, busyLabel in
            locationIconImageView.leading == view.leading + 10
            locationIconImageView.top == view.top + 10
            locationIconImageView.width ==  24
            locationIconImageView.height ==  24

            selectedPlaceLabel.leading == locationIconImageView.trailing + 5
            selectedPlaceLabel.centerY == locationIconImageView.centerY
            vicinityLabel.leading == selectedPlaceLabel.leading
//            vicinityLabel.top == selectedPlaceLabel.bottom
//            feedMessageLabel.leading == selectedPlaceLabel.leading
//            feedMessageLabel.top == vicinityLabel.bottom + 5
            busyLabel.leading == selectedPlaceLabel.leading
            busyLabel.width == 60
//            busyLabel.height == 25
            distribute(by: 5, vertically: selectedPlaceLabel, vicinityLabel, busyLabel)
        }

        constrain(busyLabel, feedMessageLabel, feedImageView) { busyLabel, feedMessageLabel, feedImageView in
            feedMessageLabel.leading == busyLabel.leading
            feedMessageLabel.top == busyLabel.bottom + 5
            feedImageView.top == feedMessageLabel.bottom
        }

        constrain(busyLabel, waitLabel) { busyLabel, waitLabel in
            waitLabel.top == busyLabel.top
            distribute(by: 10, horizontally: busyLabel, waitLabel)
            waitLabel.width == busyLabel.superview!.width - 100
        }

        constrain(feedMessageLabel, feedImageView, timeImageView, cellSeparatorView) { feedMessageLabel, feedImageView, timeImageView, cellSeparatorView in
            distribute(by: 10, vertically: feedMessageLabel, feedImageView, timeImageView, cellSeparatorView)
        }

        constrain(feedImageView, timeImageView, timeLabel) { feedImageView, timeImageView, timeLabel in
            timeImageView.leading == feedImageView.superview!.leading + 10
            timeImageView.top == feedImageView.bottom
            timeImageView.width == 16
            timeImageView.height == 16
            timeLabel.leading == timeImageView.trailing + 5
            timeLabel.centerY == timeImageView.centerY
        }

        constrain(feedMessageLabel, upVoteButton, downVoteButton, feedVoteCount) { feedMessageLabel, upVoteButton, downVoteButton, feedVoteCount in
            upVoteButton.leading == (feedMessageLabel.superview?.superview!.trailing)! - 50
            upVoteButton.top == (feedMessageLabel.superview?.superview!.top)!
            feedVoteCount.leading == upVoteButton.leading
            downVoteButton.leading == upVoteButton.leading
            feedVoteCount.width == upVoteButton.width
            
            distribute(by: 0, vertically: upVoteButton, feedVoteCount, downVoteButton)
        }

        constrain(timeLabel, flagFeedButton) { timeLabel, flagFeedButton in
            flagFeedButton.top == timeLabel.top
            flagFeedButton.centerY == timeLabel.centerY
            flagFeedButton.centerX ==  (timeLabel.superview?.superview!.centerX)!
            flagFeedButton.width == 16
            flagFeedButton.height == 16
        }

        constrain(cellSeparatorView) { cellSeparatorView in
            cellSeparatorView.bottom == (cellSeparatorView.superview?.bottom)!
            cellSeparatorView.height == 10
            cellSeparatorView.leading == (cellSeparatorView.superview?.leading)!
            cellSeparatorView.trailing == (cellSeparatorView.superview?.trailing)!
        }
        super.updateConstraints()
        layoutIfNeeded()
    }
    
    func populateWithFeed(_ feed: Feed, feedViewController: Any)
    {
        nameLabel.text = ""
        feedMessageLabel.text = feed.message
        timeLabel.text = feed.relativeDateString()
        if let _  = feedViewController as? AJFeedViewController {
            self.feedViewController = feedViewController as? AJFeedViewController
        }
        if let name = feed.place?.name {
            self.selectedPlaceLabel.text = name
            self.selectedPlaceLabel.isHidden = false
            self.locationIconImageView.isHidden = false
        } else {
            self.selectedPlaceLabel.isHidden = true
            self.locationIconImageView.isHidden = true
        }
        if let vicinity = feed.place?.vicinity {
            self.vicinityLabel.text = vicinity
            self.vicinityLabel.isHidden = false
        } else {
            self.vicinityLabel.isHidden = true
        }
        updateBusyLabel(feed)
        if let _waitTime = feed.waitTime?.int64Value {
            if _waitTime > 0 {
                waitLabel.text = PlaceInfoProvider.getWaitTimeText(waitTime: _waitTime, waitTimeCount: 1)
            }
        }
        self.layoutIfNeeded()
    }

    func updateBusyLabel(_ feed: Feed) {
        var bgColor = UIColor.calmColor()
        switch feed.crowd!.intValue {
        case 2:
            bgColor = UIColor.busyColor()
            break
        case 3:
            bgColor = UIColor.crowdedColor()
            break
        default:
            bgColor = UIColor.calmColor()
        }
        busyLabel.backgroundColor = bgColor
        busyLabel.text = feed.crowdText
    }
    
    func upVoteButtonClicked(_ sender:UIButton!) {
        if (self.delegate != nil ) {
            sender.isHighlighted = true
            sender.isEnabled = false
            
            if (sender.tag == 1) {
                self.delegate?.upVoteButtonClickedInCell(self)
                self.animateVoteButton(self.upVoteButton)
            }
            else if(sender.tag == 2) {
                self.delegate?.downVoteButtonClickedInCell(self)
                self.animateVoteButton(self.downVoteButton)
            }

        }
    }

    func flagFeedButtonClicked(_ sender: UIButton) {
        self.feedViewController?.flagFeedButtonClickedIncell(self)
    }

    func animateVoteButton(_ button: UIButton) {
        button.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            button.transform = CGAffineTransform(scaleX: 1, y: 1)

        })
    }

}
