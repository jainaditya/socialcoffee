//
//  AJFeedViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/23/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import Alamofire

let kFeedImageCell = "FeedImageCell"
let kFeedCell = "FeedCell"
let kNoResultsCell = "NoResultsCell"

class AJFeedViewController: AJViewController, UITableViewDataSource, NSFetchedResultsControllerDelegate, AJFeedTableViewCellDelegate, AJGeoLocationProviderDelegate, FeedRequestSynchronizerDelegate, UIActionSheetDelegate
{
    lazy var feedItems: [AnyObject]  = []
    @IBOutlet weak var tableView : UITableView!
    var fetchedResultsController : NSFetchedResultsController<NSFetchRequestResult>?
    let feedRequestSynchronizer = AJFeedRequestSynchronizer()
    var locationProvider = AJGeoLocationProvider.sharedInstance
    var loadUserFeeds = false
    var feeds = [AnyObject]()
    var loadingFeeds = true
    var currentUser: User?
    var selectedCell: UITableViewCell?
    var selectedPlace: AJPlace?
    var floatingAddButton: UIButton!
    let addButtonSize = CGFloat(55)
    var hideFloatingButton = false

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(AJFeedViewController.refreshFeeds(_:)), for: UIControlEvents.valueChanged)

        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createSubviews()
        self.tableView.register(AJFeedTableViewCell.self, forCellReuseIdentifier: kFeedCell)
        self.tableView.register(AJFeedImageTableViewCell.self, forCellReuseIdentifier: kFeedImageCell)
        self.tableView.register(AJNoResultsTableViewCell.self, forCellReuseIdentifier: kNoResultsCell)
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 68.0
        tableView.separatorStyle = .none
        self.tableView.addSubview(self.refreshControl)
        refreshFeeds(refreshControl)
        self.feedRequestSynchronizer.feedSaveDelegate = self
//        if !self.loadUserFeeds {
//            self.feedRequestSynchronizer.willDeleteLocalFeeds()
//        }
        self.currentUser = AJSessionService.currentUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.fetchCachedFeedItems()
    }
    
    func willUpdateLocalFeed() {
        self.loadingFeeds = true
        var params = [String: AnyObject]()
        if let place = selectedPlace {
            params["place.id"] = place.id as AnyObject?
        }
        self.feedRequestSynchronizer.updateDatabaseForFetchRequest(loadUserFeeds, params: params)
    }

    func refreshFeeds(_ sender: UIRefreshControl) {
        sender.beginRefreshing()
        AJGeoLocationProvider.sharedInstance.delegate = self
        AJGeoLocationProvider.sharedInstance.startUpdatingLocation()
    }

    func createSubviews() {
        if hideFloatingButton {
            return
        }
        floatingAddButton = UIButton(type: .custom)
        floatingAddButton.frame = CGRect(x: view.frame.size.width - (addButtonSize * 1.5), y: (self.tabBarController?.tabBar.frame.origin.y)! - (addButtonSize * 2.5), width: addButtonSize, height: addButtonSize)
        floatingAddButton.backgroundColor = UIColor.defaultBarColor()
        floatingAddButton.setImage(UIImage(named: "add"), for: .normal)
        floatingAddButton.layer.cornerRadius = CGFloat(addButtonSize / 2)
        floatingAddButton.setTitleShadowColor(.black, for: .normal)
        floatingAddButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(45)
        floatingAddButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        floatingAddButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        floatingAddButton.layer.shadowOpacity = 1.0
        floatingAddButton.layer.shadowRadius = 6
        floatingAddButton.layer.masksToBounds = false
        floatingAddButton.titleLabel?.adjustsFontSizeToFitWidth = true
        view.addSubview(floatingAddButton)
        floatingAddButton.addTarget(self, action: #selector(self.composeFeedItem(_:)), for: .touchUpInside)

    }

    func composeFeedItem(_ sender: UIButton) {
        let placePickerViewController: AJPlacePickerController = self.storyboard?.instantiateViewController(withIdentifier: "AJPlacePickerController") as! AJPlacePickerController
        self.navigationController?.pushViewController(placePickerViewController, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination.isKind(of: AJFeedCommentsViewController.self)) {
            let feedCommentsViewController = segue.destination as! AJFeedCommentsViewController
            feedCommentsViewController.feed = sender as? Feed
            segue.destination.hidesBottomBarWhenPushed = true
        } else if (segue.destination.isKind(of: AJPlacePickerController.self)) {
            segue.destination.hidesBottomBarWhenPushed = true
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections =  self.fetchedResultsController?.sections{
            let _ = sections[section]
            if feeds.count > 0 {
                return feeds.count
            }
        }
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsController?.sections?.count ?? 0;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if feeds.count > 0 {
            self.tableView.rowHeight = UITableViewAutomaticDimension
            let feed : Feed = feeds[indexPath.row] as! Feed
            feed.votes.filtered(using: NSPredicate(format: "objectId = %@", (self.currentUser?.objectId)! ))
            if let _ = feed.image?.path {
                let cell: AJFeedImageTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: kFeedImageCell) as! AJFeedImageTableViewCell
                self.setupCell(cell, feed: feed)
                return cell
            } else {
                let cell:AJFeedTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: kFeedCell) as! AJFeedTableViewCell
                self.setupCell(cell, feed: feed)
                return cell
            }
        } else {
            let cell:AJNoResultsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: kNoResultsCell) as! AJNoResultsTableViewCell
            cell.contentView.frame = self.view.frame
            self.tableView.rowHeight = self.view.frame.size.height
            cell.createSubviews()
            cell.populateWithNoResultText(self.loadingFeeds)
            return cell
        }

    }

    func setupCell(_ cell: AJFeedTableViewCell, feed: Feed) {
        cell.contentView.frame = self.view.frame
        cell.delegate = self
        cell.feedVoteCount.text = "\(feed.votes.value(forKeyPath: "@sum.updown")!)"
        cell.populateWithFeed(feed, feedViewController: self)

        let vote =  feed.votes.filtered(using: NSPredicate(format: "user.objectId = %@", (self.currentUser?.objectId)! ))

        // let cell = cell as! AJFeedTableViewCell
        if vote.count > 0 {
            let vote = vote.first as! Vote
            if vote.updown == NSNumber(value: 1 as Int) {
                cell.upVoteButton.isSelected = true
                cell.downVoteButton.isSelected = false
            } else if vote.updown == NSNumber(value: -1 as Int) {
                cell.downVoteButton.isSelected = true
                cell.upVoteButton.isSelected = false
            } else {
                cell.downVoteButton.isSelected = false
                cell.upVoteButton.isSelected = false
            }
        } else {
            cell.upVoteButton.isSelected = false
            cell.downVoteButton.isSelected = false
        }
        cell.downVoteButton.isEnabled = true
        cell.upVoteButton.isEnabled = true
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 500
    }

    private func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    private func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if feeds.count > 0 && indexPath.row <= feeds.count {
            if let feed = feeds[indexPath.row] as? Feed {
                self.performSegue(withIdentifier: "feedCommentsViewSegue", sender: feed)
            }
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.feeds.removeAll()
        if self.loadUserFeeds != true {
            for (index, feed) in (self.fetchedResultsController?.fetchedObjects?.enumerated())! {
                let feed = feed as! Feed

                if feed.objectId != nil && feed.getDistanceInMilesFromCurrentLocation() <= kFilterDistanceInMiles {
                    self.feeds.append((self.fetchedResultsController?.fetchedObjects![index])!)
                }
            }
        } else {
            self.feeds = (self.fetchedResultsController?.fetchedObjects)!
        }
        self.tableView.reloadData()
    }

    func fetchCachedFeedItems() {
        if let context = AJCachingService.sharedInstance.managedObjectContext{
            let fetch = NSFetchRequest<NSFetchRequestResult>()
            let entity  = NSEntityDescription.entity(forEntityName: Feed.entityName(), in: context)
            fetch.entity = entity
            fetch.sortDescriptors = [NSSortDescriptor(key: FeedAttributes.createdAt.rawValue, ascending: false)]
            if loadUserFeeds {
                let resultPredicate = NSPredicate(format: "user.objectId = %@", (self.currentUser?.objectId)!)
                fetch.predicate = resultPredicate
            }
            if let _ = selectedPlace {
                let resultPredicate = NSPredicate(format: "place.id = %@", selectedPlace!.id)
                fetch.predicate = resultPredicate
            }
            self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetch, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            self.fetchedResultsController?.delegate = self

            var error : NSError?

            do {
                try fetchedResultsController?.performFetch()
                self.feeds = (fetchedResultsController?.fetchedObjects)!
            } catch let error1 as NSError {
                error = error1
            }
            if error != nil{
                print("Error while fetching core data product entities")
            }
        }
    }

    //Voting button delegate
    func upVoteButtonClickedInCell(_ cell: UITableViewCell) {
        if let feed: Feed = getFeedForSelectedCell(cell) {
            let _ = AJVoteSynchronizer.init(feedId: feed.objectId, voteUpDown: 1, feedObject: feed)
        }
    }

    func downVoteButtonClickedInCell(_ cell: UITableViewCell) {
        if let feed: Feed = getFeedForSelectedCell(cell) {
            let _ = AJVoteSynchronizer.init(feedId: feed.objectId, voteUpDown: -1, feedObject: feed)
        }
    }

    func flagFeedButtonClickedIncell(_ cell: UITableViewCell) {
        let flagActionSheet: UIActionSheet = UIActionSheet(title: "Are you sure, you want to flag this inapropriate?", delegate: self, cancelButtonTitle: "No", destructiveButtonTitle: "Yes")
        flagActionSheet.show(from: (self.tabBarController?.tabBar)!)
        selectedCell = cell;
    }

    func getFeedForSelectedCell(_ cell: UITableViewCell) -> Feed {
        if let indexPath: IndexPath = (self.tableView.indexPath(for: cell)) {
            if (indexPath.row <= feeds.count - 1) {
                let feed : Feed = self.feeds[indexPath.row] as! Feed
                return feed;
            }
        }
        return Feed()
    }

    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            if let feed: Feed = getFeedForSelectedCell(self.selectedCell!) {
                let _ = AJFlagFeedSynchronizer.init(feed: feed)
            }
        }
    }

    //LocationProvide Delegate
    func didUpdateFeedLocation() {
        AJGeoLocationProvider.sharedInstance.stopUpdatingLocation()
        self.willUpdateLocalFeed()
    }

    func didFailToUpdateLocation() {
        self.loadingFeeds = false
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }

    // AJFeedRequestSyncronizer
    func feedSaveOperationDidEnd() {}

    func didEndFeedFetchOperation(_ fetchedObjects: [AnyObject]) {
        self.loadingFeeds = false
        refreshControl.endRefreshing()
        if (fetchedObjects.count == 0) {
            self.tableView.reloadData()
        }
    }
    
}
