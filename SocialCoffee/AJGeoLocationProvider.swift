//
//  AJGeoLocationProvider.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/14/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

struct CurrentLocation {
    static var latitude:  Double = 0
    static var longitude: Double = 0
    static var available: Bool = false
    static var changed: Bool = false
}

protocol AJGeoLocationProviderDelegate {
    func didUpdateFeedLocation()
    func didFailToUpdateLocation()
}


class AJGeoLocationProvider : NSObject, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager?
    var delegate: AJGeoLocationProviderDelegate?
    var updatedLocation = false

    class var sharedInstance : AJGeoLocationProvider {
        struct Singleton {
            static let instance = AJGeoLocationProvider()
        }
        return Singleton.instance
    }
    
    override init() {
        super.init()
        self.locationManager = CLLocationManager()
        guard self.locationManager != nil else {
            return
        }
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager?.delegate = self
            self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager?.distanceFilter = 200
            self.locationManager?.requestWhenInUseAuthorization()
        } else {
            print("location disabled")
            self.notifyFailToUpdateLocation()
        }
    }

    func startUpdatingLocation() {
        self.locationManager?.startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        self.locationManager!.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
            case .notDetermined:
                print("Not Determined")
                CurrentLocation.available = false
            case .authorizedWhenInUse:
                print("Authorized when in use")
                CurrentLocation.available = true
            case .denied:
                print("Denied")
                CurrentLocation.available = false
        default:
            print("DO DEFAULT STUFF")
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error \(error.localizedDescription)")
        self.notifyFailToUpdateLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.setCurrentLocation()
        self.locationManager?.stopUpdatingLocation()
        if self.delegate != nil {
            self.delegate?.didUpdateFeedLocation()
            self.delegate = nil
        }
    }

    func setCurrentLocation() {
        CurrentLocation.changed = true
        CurrentLocation.available = true

        if let location = self.locationManager?.location {
            let distance = CLLocation(latitude: CurrentLocation.latitude, longitude: CurrentLocation.longitude).distance(from: location) / 1069.344
            if distance < 0.1 {
                CurrentLocation.changed = false
            }
            CurrentLocation.latitude = location.coordinate.latitude
            CurrentLocation.longitude = location.coordinate.longitude
        }

    }

    func notifyFailToUpdateLocation() {
        if self.delegate != nil {
            self.delegate?.didUpdateFeedLocation()
        }
    }
}
