//
//  AJGlobalConstants.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/26/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
struct Constants {
    static let TermsAndConditions = "Terms and Conditions"
    static let PrivacyPolicy = "Privacy Policy"
    static let WhatToPost = "What NOT to Post"
    static let ShareSippers = "Share CrowdWave"
    static let RateApp = "Rate CrowdWave"
    static let LikeOnFacebook = "Like Us on Facebook"
    static let FollowOnTwitter = "Follow Us on Twitter"
    static let Support = "Support"
    static let LoveApp = "Love CrowdWave"
    static let ShareText = "CrowdWave let's you find what's happing in near by cafes, restaurants and bars."
    static let LocationDisabled = "Turn on location for a personalized experience and to see what cafes, restaurants and bars are busy nearby."
    static let Account = "Account"
    static let Verified = "Verified"
    static let VerifiedAccountText = "You have successfully verified your account."
    static let VerifyAccount = "Verify Your Account"
    static let VerifyAccountText = "This will allow you to access your account on multiple devices"
    static let ContactUs = "Contact Us"
    static let UpdateInterest = "Update your interests"
    static let PlaceCategoriesArray = ["Dinner", "Restaurant", "Party", "Nightclub", "Bar", "Travel"]

    struct Title {
        static let More = "More"
    }

    struct FeedView {
        static let LoadingFeeds = "Loading activity..."
        static let NoFeeds = "No activity around you.\r\nAnonymously share a place and let people know what bar or restaurant is buzzy..."
    }

    struct PlaceType {
        static let Food = "food"
        static let Drinks = "drinks"
        static let Coffee = "coffee"
        static let Trending = "specials"
    }

    struct LiveFeedType {
        static let Crowd = "Crowd"
        static let WaitTime = "Wait Time"
        static let WaitTimeParam = "waitTime"
        static let CrowdParam = "crowd"
    }
}
