//
//  AJImagedIconView.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 12/30/15.
//  Copyright © 2015 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit

enum AJImagedIconViewType: Int {
    case time = 1
    case up
    case down
}

class AJImagedIconView: UIImageView {
    
    init() {
        super.init(frame: .zero)
        self.createSubViews()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.createSubViews()
    }

    required init?(coder aDecoder: NSCoder) {
        print("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        self.createSubViews()
    }
    
    func createSubViews() {    }
    
    override func updateConstraints() {
        super.updateConstraints()
        layoutIfNeeded()
        
    }
    
    func configureFor(_ type:AJImagedIconViewType) {
        var image = UIImage()
        switch type {
        case .up:
            image = UIImage(named: "up")!
        case .down:
            image =  UIImage(named: "down")!
        case .time:
            image = UIImage(named: "recents")!
        }
        self.clipsToBounds = true
        self.image = image
    }
}
