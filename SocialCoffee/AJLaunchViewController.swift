
//  AJLaunchViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/23/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Crashlytics
import APESuperHUD

class AJLaunchViewController: PlaceCategoryViewController {
    
    var mainTabBarController : AJTabBarController = AJTabBarController()
//    var locationProvider : AJGeoLocationProvider!
//    var placeCategorySelectionView: PlaceCategorySelectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        setupProgressHUD()
        if ((AJSessionService.currentUser()?.isKind(of: User.self)) != nil) {
            if AJPlaceCategoryContentProvider().getSelectedCategories().count > 0 {
                self.presentMainView()
            }
        } else {
//            self.locationProvider = AJGeoLocationProvider()
            let _ = AJUserData().createNewUser(completion)
        }
        self.initializeSubviews()
    }

    func setupProgressHUD() {
        APESuperHUD.appearance.titleFontName = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(0).fontName
//        APESuperHUD.appearance.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        APESuperHUD.appearance.animateOutTime = 0.5
        APESuperHUD.appearance.backgroundBlurEffect = .light
    }

    func completion(_ user: User) -> Void {
        if (user.isKind(of: User.self)) {
//            self.presentMainView()
        }
    }

    func initializeSubviews() {
//        let placeCategoryViewController = PlaceCategoryViewController()
//        view.addSubview(placeCategoryViewController.view)
//        placeCategoryViewController.placeCategorySelectionView.continueButton.addTarget(self, action: #selector(AJLaunchViewController.tappedGetStartedButton(_:)), forControlEvents: .TouchUpInside)
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        self.view.layoutIfNeeded()
    }

    func presentMainView() -> Void {
        mainTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "AJTabBarController") as! AJTabBarController
        UIApplication.shared.delegate?.window!?.rootViewController = mainTabBarController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func clickedSaveCategory(_ sender: UIButton) {
        presentMainView()
        //self.performSegueWithIdentifier("mainTabBarSegue", sender: sender)
    }
    
    @IBAction func tappedLoginButton(_ sender: UIButton) {
        
    }
}
