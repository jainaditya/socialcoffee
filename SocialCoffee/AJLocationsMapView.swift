//
//  AJLocationsMapView.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/2/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import MapKit

extension AJProfileViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: MKAnnotationView?
        if let annotation = annotation as? AJSippingLocationAnnotation {
            let identifier = "Pin"
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                annotationView = dequeuedView
            } else {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                annotationView?.image = annotation.pinImage()
            }
        }

        return annotationView
    }
}
