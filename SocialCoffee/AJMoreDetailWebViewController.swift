//
//  AJMoreDetailViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/24/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

enum MoreWebViewType: Int {
    case post
    case terms
    case privacy
}

class AJMoreDetailWebViewController: AJViewController, UIWebViewDelegate {

    @IBOutlet weak var webView : UIWebView!
    var activityIndicator = UIActivityIndicatorView()
    var webViewType = Int()
    var queryType = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.delegate = self
        if super.isModal() {
            self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(AJMoreDetailWebViewController.tappedReturnHome)), animated: false)
        }
        self.setModalTitle()
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.secondaryColor()
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.center = self.view.center

        self.webView.addSubview(self.activityIndicator)
        self.webView.bringSubview(toFront: self.activityIndicator)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadTermsAndConditions()
    }

    func tappedReturnHome() {
        super.returnToPreviousView()
    }

    func loadTermsAndConditions() {
//        let query = PFQuery(className: "Setting")
//        query.whereKey("type", equalTo: self.queryType)
//        self.activityIndicator.startAnimating()
//        query.findObjectsInBackgroundWithBlock {
//            (objects, error) -> Void in
//            if error == nil {
//                if let object = objects?.first as? PFObject {
//                    if let url = object["url"] {
//                        self.webView.loadRequest(NSURLRequest(URL: NSURL(string: url as! String)!))
//                    } else {
//                        self.webView.loadHTMLString(object["content"] as! String, baseURL: nil)
//                    }
//                }
//            }
//        }

        Alamofire.request(RequestUrlProvider.Router.readSettings())
            .responseJSON{ response in
                if response.result.isSuccess {
                    if let responseValue = response.result.value {
                        let responseObjects = JSON(responseValue)
                        self.showWebViewForSelectedContent(responseObjects)
                    }
                }
        }
    }

    func showWebViewForSelectedContent(_ responseObjects: JSON) {
        for (_, responseObject): (String, JSON) in responseObjects {
            let contentDictionary = responseObject.dictionaryValue
            if (self.queryType == (contentDictionary["contentType"]?.string)!) {
                if let url = contentDictionary["url"]?.string {
                    self.webView.loadRequest(NSURLRequest(url: NSURL(string: url)! as URL) as URLRequest)
                } else {
                    self.webView.loadHTMLString((contentDictionary["content"]?.string)!, baseURL: nil)
                }
            }
        }


    }

    func setModalTitle() {

        switch webViewType {
        case MoreWebViewType.terms.rawValue:
            title = Constants.TermsAndConditions
            self.queryType = "terms"
            break
        case MoreWebViewType.privacy.rawValue:
            title = Constants.PrivacyPolicy
            self.queryType =  "privacy"
            break
        case MoreWebViewType.post.rawValue:
            title = Constants.WhatToPost
            self.queryType = "post"
            break
        default:
            title = Constants.Title.More
            self.queryType = "terms"
        }
        self.title = title
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.startAnimating()
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
}
