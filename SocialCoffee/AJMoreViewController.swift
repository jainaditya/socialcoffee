//
//  AJMoreViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/17/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import DigitsKit
import MessageUI

class AJMoreViewController: AJViewController, UITableViewDataSource, UIAlertViewDelegate, MFMailComposeViewControllerDelegate {

    lazy var moreItems: [Any]  = []
    lazy var moreSections: [Any] = []
    var currentUser: User?

    @IBOutlet weak var tableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 68.0
        self.initializeDataSource()
        currentUser = AJSessionService.currentUser()
    }

    func initializeDataSource() {
        moreItems = [[Constants.VerifyAccount, Constants.UpdateInterest],[Constants.ShareSippers, Constants.RateApp, Constants.LikeOnFacebook]
                    ,[Constants.WhatToPost, Constants.TermsAndConditions, Constants.ContactUs]]
        moreSections = [Constants.Account as AnyObject, Constants.LoveApp as AnyObject, Constants.Support as AnyObject]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let items = self.moreItems[section] as? [AnyObject] {
            return items.count
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.moreSections.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.moreSections[section] as? String
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell", for: indexPath)
        if (currentUser != nil && currentUser?.verified == 1 && indexPath.section == 0 && indexPath.row == 0){
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                            reuseIdentifier: "MoreCell")
            cell.detailTextLabel?.text = Constants.Verified
            cell.detailTextLabel?.textColor = UIColor.defaultBarColor()
        } else {
            cell.detailTextLabel?.text = ""
        }
        cell.selectionStyle = .none
        let sectionArray: [String] = self.moreItems[indexPath.section] as! [String]
        cell.textLabel?.text = sectionArray[indexPath.row]
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                if currentUser?.verified == 0 {
                    self.presentDigitsAccountVerificationController()
                } else {
                    UIAlertView(title: Constants.Verified, message: Constants.VerifiedAccountText, delegate: self, cancelButtonTitle: "Okay").show()
                }
                break
            case 1:
                self.present(PlaceCategoryViewController(), animated: true , completion: nil)
                break
            default:
                break
            }
        }
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                self.presentShareActivityViewController()
            }
            if indexPath.row == 1 {
                UIApplication.tryURL([kRateAppUrl, ""])
            }
            if indexPath.row == 2 {
                UIApplication.tryURL([kFacebookAppUrl,kFacebookWebUrl])
            }
        }

        if indexPath.section == 2 {
            if indexPath.row == 2 {
                self.presentEmailComposeView()
            }
            else {
                self.presentMoreDetailWebViewController(indexPath.row)
            }
        }
    }

    func presentEmailComposeView() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["hello@sippersapp.com"])
        mailComposerVC.setSubject("CrowdWave Support - Inquiry")
        mailComposerVC.setMessageBody("Hello, \r\n", isHTML: false)

        return mailComposerVC
    }

    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }

    // MARK: MFMailComposeViewControllerDelegate Method

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    func presentDigitsAccountVerificationController() {
        let digits = Digits.sharedInstance()
        let dgtAppearance = DGTAppearance()
        dgtAppearance.accentColor = UIColor.defaultBarColor()
        dgtAppearance.backgroundColor = UIColor.defaultImageBackgroundColor()
        dgtAppearance.bodyFont = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(16.0)
        dgtAppearance.labelFont = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(16.0)

        let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
        configuration?.appearance = dgtAppearance
        self.navigationController?.navigationBar.barTintColor = UIColor.defaultBarColor()
        digits.session()
        DispatchQueue.main.async(execute: {
            digits.authenticate(with: self, configuration: configuration!){ (session, error) -> Void in
                if (session != nil) {
                    AJUserData().updateCurrentUserVerification(session)
                }
                else {
                    print(error?.localizedDescription)
                }
                
            }
        });
    }

    func presentShareActivityViewController() {
        let string: String = Constants.ShareText
        let URL: Foundation.URL = Foundation.URL(string: "http://sippersapp.com")!
        let activityViewController = UIActivityViewController(activityItems: [string, URL], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)

    }

    func presentMoreDetailWebViewController(_ webViewType: Int) {
        let moreDetailWebViewController = self.storyboard?.instantiateViewController(withIdentifier: "AJMoreDetailWebViewController") as! AJMoreDetailWebViewController
        let navigationController = AJNavigationController(rootViewController: moreDetailWebViewController)
        moreDetailWebViewController.webViewType = webViewType

        DispatchQueue.main.async(execute: {
            self.present(navigationController, animated: true, completion: nil)
        })
    }
}
