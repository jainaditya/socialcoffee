//
//  AJNavigationController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/19/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit

class AJNavigationController :  UINavigationController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationBar.barStyle = UIBarStyle.default
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.isTranslucent = false
        self.navigationBar.tintColor = UIColor.defaultBarColor()//(red: 0.904593, green: 0.785233, blue: 0.14738, alpha: 1)
        self.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont.ajCustomFontAppleSDGothicNeoSemiBoldWithSize(18), NSStrokeColorAttributeName: UIColor.defaultBarColor(), NSForegroundColorAttributeName: UIColor.defaultBarColor()]
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(15)], for: UIControlState())
        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.ajCustomFontAppleSDGothicNeoLightWithSize(11)], for: UIControlState())
    }
}
