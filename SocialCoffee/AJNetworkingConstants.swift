//
//  AJNetworkingConstants.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/16/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation

let kGooglePlacesApiBaseUrl : String = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
let kGoogleApiKey : String = "AIzaSyBbExmOlfgpLFM5Y2b0ZiIAnEfWSz27nOQ"
let kParseBaseUrl : String = ""
let kFilterDistanceInMiles:Double = 10
let kFacebookAppUrl = "fb://profile/928382100580771"
let kFacebookWebUrl = "https://www.facebook.com/sippersapp/"
let kRateAppUrl = "itms-apps://itunes.apple.com/app/id1085982995"
let PROD = 1
var kBaseUrl: String = {
    var _kBaseUrl = "http://localhost:1337/api/v1"
    if PROD == 1 {
        _kBaseUrl = "https://sippersapp.herokuapp.com/api/v1"
    }

    return _kBaseUrl
}()
let kRadiusInMeter: Double = 1609 * 5 // 5 mile radius
var kSubmittedLiveFeed = false
