//
//  AJNoResultsTableViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/31/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit

class AJNoResultsTableViewCell: UITableViewCell {

    let noResultsTextLabel = UILabel()
    let iconImageView = UIImageView()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier!)
        selectionStyle = .none
        isUserInteractionEnabled = true
        backgroundColor = UIColor.clear
    }


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {
        noResultsTextLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(18)
        noResultsTextLabel.preferredMaxLayoutWidth = contentView.bounds.size.width
        noResultsTextLabel.numberOfLines = 0
        noResultsTextLabel.sizeToFit()
        noResultsTextLabel.textColor = UIColor.secondaryTextColor()
        noResultsTextLabel.frame = CGRect(x: 20, y: 0, width: contentView.frame.size.width - 40, height: contentView.frame.size.height)
        noResultsTextLabel.textAlignment = .center
        contentView.addSubview(noResultsTextLabel)
        contentView.addSubview(iconImageView)

        self.frame = contentView.frame
    }

    func populateWithNoResultText(_ isLoadingFeeds: Bool) {
        if CurrentLocation.available || isLoadingFeeds {
            if isLoadingFeeds {
                noResultsTextLabel.text = Constants.FeedView.LoadingFeeds
            } else {
                noResultsTextLabel.text = Constants.FeedView.NoFeeds
            }
        } else {
            noResultsTextLabel.text = Constants.LocationDisabled
        }
    }

    func populateNoResultView(_ message: String, icon: UIImage) {

        iconImageView.frame = CGRect(x: contentView.frame.size.width / 2 - (icon.size.width / 2), y: 20, width: icon.size.width, height: icon.size.height)
        noResultsTextLabel.frame = CGRect(x: 20, y: 0, width: contentView.frame.size.width - 40, height: contentView.frame.size.height - icon.size.height)
        iconImageView.image = icon
        noResultsTextLabel.text = message
        noResultsTextLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(16)
        noResultsTextLabel.textColor = UIColor.lightGray.withAlphaComponent(0.5)
    }
}
