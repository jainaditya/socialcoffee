//
//  AJPlaceCategoryCollectionViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/16/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class AJPlaceCategoryCollectionViewCell: UICollectionViewCell {

    var cellFadeView: UIView!
    var backgroundImageView: UIImageView!
    var iconImageView: UIImageView!
    var cellTitleLabel: UILabel!
    var selectImageView: UIImageView!
    let cellFadeViewBGColor = UIColor.defaultBarColor().withAlphaComponent(0.5)

    override init(frame:CGRect) {
        super.init(frame:frame)
        self.createSubviews()
        self.setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews()  {
        self.backgroundImageView = UIImageView()
        self.contentView.addSubview(self.backgroundImageView)

        self.cellFadeView = UIView()
        self.cellFadeView.backgroundColor = cellFadeViewBGColor
        self.contentView.addSubview(cellFadeView)

        self.iconImageView = UIImageView()
        self.contentView.addSubview(self.iconImageView)

        self.cellTitleLabel = UILabel()
        self.cellTitleLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(18)
        self.cellTitleLabel.textColor = UIColor.white
        self.contentView.addSubview(self.cellTitleLabel)

        self.selectImageView = UIImageView()
        self.contentView.addSubview(self.selectImageView)
        self.selectImageView.image = UIImage(named: "heart")
    }

    override func updateConstraints() {
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        self.cellFadeView.translatesAutoresizingMaskIntoConstraints = false
        self.iconImageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.translatesAutoresizingMaskIntoConstraints = false

        constrain(self, self.backgroundImageView) { view, backgroundImageView in
            backgroundImageView.width ==  view.width
            backgroundImageView.height ==  view.height
        }
        constrain(self, self.selectImageView) { view, selectImageView in
            selectImageView.top == view.top + 10
            selectImageView.trailing == view.trailing - 10
        }

        constrain(self, self.iconImageView) { view, iconImageView in
            iconImageView.centerX == view.centerX
            iconImageView.centerY == view.centerY - 20
        }

        constrain(self.iconImageView, self.cellTitleLabel) { iconImageView, cellTitleLabel in
            cellTitleLabel.centerX == iconImageView.centerX
//            cellTitleLabel.centerY == (cellTitleLabel.superview?.superview!.centerY)! + 15
            cellTitleLabel.bottom == (cellTitleLabel.superview?.superview!.bottom)! - 15
            cellTitleLabel.width == (iconImageView.superview?.width)!
        }

        constrain(self.backgroundImageView, self.cellFadeView) { backgroundImageView, cellFadeView in
            cellFadeView.edges == backgroundImageView.edges
            cellFadeView.size == backgroundImageView.size
        }

        super.updateConstraints()
        self.layoutIfNeeded()
    }

    func configureCell(_ title: String, icon: String, background: String, tag: Int, selected: Bool) {
        self.cellTitleLabel.text = title
        self.iconImageView.image = UIImage(named:  icon.lowercased().replacingOccurrences(of: " ", with: ""))
        if selected {
            selectImageView.image = UIImage(named: "heart-filled")
        } else {
            selectImageView.image = UIImage(named: "heart")
        }

    }

}
