//
//  AJPlaceCategoryContentProvider.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/17/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
enum PlaceCategories: Int {
    case dinner = 0
    case restaurant = 1
    case party = 2
    case nightclub = 3
    case bar = 4
    case fun = 5
}
class AJPlaceCategoryContentProvider: NSObject {
    let categories: Array = Constants.PlaceCategoriesArray
    let placeCategoryKey = "PlaceCategory"
    override init() {
        super.init()
        initUserDefaults()
    }

    func titleForIndexPath(_ indexPath: IndexPath) -> String {
        return categories[indexPath.row]
    }

    func initUserDefaults() {
        let userDefaults = UserDefaults.standard
        guard let _ = userDefaults.stringArray(forKey: placeCategoryKey) else {
            userDefaults.set([], forKey: placeCategoryKey)
            return
        }
    }

    func saveSelectedCategory(_ indexPath: IndexPath) -> String? {
        let userDefaults = UserDefaults.standard
        if var placeCategories = userDefaults.stringArray(forKey: placeCategoryKey) {
            let category = categories[indexPath.row]
            if let index = placeCategories.index(of: category) {
                placeCategories.remove(at: index)
            } else {
                placeCategories.append(category)
            }
            userDefaults.set(placeCategories, forKey: placeCategoryKey)
            userDefaults.synchronize()
            return category
        }
        return nil
    }

    func getSelectedCategories() -> [String] {
        let userDefaults = UserDefaults.standard
        guard let selectedCategories = userDefaults.stringArray(forKey: placeCategoryKey) else {
            return []
        }

        return selectedCategories
    }

    func isSelectedCategory(_ category: String) -> Bool {
        return getSelectedCategories().contains(category)
    }

}
