//
//  AJPlacePickerController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 12/14/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AlamofireImage

class AJPlacePickerController: AJViewController, UISearchResultsUpdating, RequestNearByPlacesDelegate, UISearchControllerDelegate, UITableViewDataSource {
    lazy var placeItems: [AJPlaceFourSquare]  = []
    lazy var filteredPlaceItems: [AJPlaceFourSquare]  = []
    @IBOutlet weak var tableView : UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var fetchedResultsController : NSFetchedResultsController<NSFetchRequestResult>?
    var requestNearByPlaces: AJRequestFourSquarePlaces?
    var presentedNextView = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSearchController()
        loadPlaces(query: nil)
        if self.isModal() {
            self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(AJPlacePickerController.tappedReturnHome)), animated: false)
        }
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.register(ExploreSimpleTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(ExploreSimpleTableViewCell.self))
        tableView.register(AJNoResultsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(AJNoResultsTableViewCell.self))
        tableView.keyboardDismissMode = .onDrag
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }

    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        self.navigationItem.titleView = searchController.searchBar
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = UIColor.defaultBarColor()
        searchController.searchBar.searchBarStyle = .default
        searchController.searchBar.setShowsCancelButton(false, animated: true)
        searchController.delegate = self
        searchController.searchBar.placeholder = "e.g. bars, italian, tacos, Name               "
    }

    override func viewDidLayoutSubviews() {
        if let rect = self.navigationController?.navigationBar.frame {
            if isSearching() {
                let y = rect.size.height + rect.origin.y
                self.tableView.contentInset = UIEdgeInsetsMake( y, 0, 0, 0)
            }
        }
    }

    func tappedReturnHome() {
        super.returnToPreviousView()
    }

    func loadPlaces(query: String?) {
        var params: [String: AnyObject] = [:]
        if query == nil {
            params["section"] = "drinks" as AnyObject?
        }
        params["time"] = "any" as AnyObject?
        params["day"] = "any" as AnyObject?
        params["venuePhotos"] = "1" as AnyObject?
        params["sortByDistance"] = "1" as AnyObject?
        params["radius"] = String(1609 * 20)  as AnyObject?

        requestNearByPlaces = AJRequestFourSquarePlaces(uri: "/venues/explore", params: params, searchQuery: query)
        requestNearByPlaces?.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (filteredPlaceItems.count == 0 && isSearching()) || placeItems.count == 0 {
            return 1
        }
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredPlaceItems.count
        }
        return placeItems.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (filteredPlaceItems.count == 0 && isSearching()) || placeItems.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AJNoResultsTableViewCell.self)) as! AJNoResultsTableViewCell
            cell.contentView.frame = self.view.frame
            self.tableView.rowHeight = self.view.frame.size.height / 2
            cell.createSubviews()
            cell.populateNoResultView("No places found for your search", icon: UIImage())
            return cell
        } else {

            self.tableView.rowHeight = UITableViewAutomaticDimension
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(ExploreSimpleTableViewCell.self)) as! ExploreSimpleTableViewCell
            let place = getPlaceForIndexPath(indexPath)
            cell.addButton.isHidden = true
            cell.populateCellWithPlace(place: place, type: "bars", placeMetrics: [])
            return cell
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    @nonobjc internal func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    @nonobjc internal func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    private func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var place: AJPlaceFourSquare
        if  isSearching() {
            place = filteredPlaceItems[indexPath.row]
        } else {
            place = placeItems[indexPath.row]
        }
        let feedOptionsViewController = FeedOptionsViewController(title: "Report", place:  place)
        feedOptionsViewController.hidesBottomBarWhenPushed = true

        self.navigationController?.pushViewController(feedOptionsViewController, animated: true)
        self.hidesBottomBarWhenPushed = true
        self.presentedNextView = true
        if #available(iOS 10.0, *) {
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        } else {
            // Fallback on earlier versions
        }
    }

    func getPlaceForIndexPath(_ indexPath: IndexPath) -> AJPlaceFourSquare {
        if  isSearching() {
            return filteredPlaceItems[indexPath.row]
        } else {
            return placeItems[indexPath.row]
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "composeViewControllerSegue" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! AJFeedComposeViewController
                let place: AJPlace
                if  isSearching() {
                    place = filteredPlaceItems[indexPath.row]
                } else {
                    place = placeItems[indexPath.row]
                }
                controller.selectedPlace = place
            }
        }
    }

    func isSearching() -> Bool {
        return searchController.isActive && searchController.searchBar.text != ""
    }

    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        if searchText.characters.count > 2 {
            filteredPlaceItems.removeAll()
            loadPlaces(query: searchText)
        } else {
            self.filteredPlaceItems = self.placeItems.filter { place in
                return place.name.lowercased().contains(searchText.lowercased())
            }
            tableView.reloadData()
        }
    }

    func updateSearchResults(for searchController: UISearchController) {
        if !presentedNextView {
            filterContentForSearchText(searchController.searchBar.text!)
        } else {
            presentedNextView = false
        }
    }

    func didReceiveNearByPlaces(_ responseObjects: JSON) {
        for (_, placeDictionary): (String, JSON) in responseObjects {
            if let place: AJPlaceFourSquare =  AJPlaceFourSquare(json: placeDictionary["venue"]) {
                if (isSearching()) {
                    filteredPlaceItems.append(place)
                } else {
                    self.placeItems.append(place)
                }
            }
        }
        self.tableView.reloadData()
    }

    func didFailNearByPlaces() {

    }
}
