//
//  AJProfileMapTableViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/1/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import MapKit
import Cartography

class AJProfileMapTableViewCell: UITableViewCell {

    weak var mapImageView: UIImageView!
    var mapView: MKMapView!
    var currentLocationView: UIView!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier!)
        self.selectionStyle = .none
        self.isUserInteractionEnabled = true
        createSubviews()
    }


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {
        let mapImageView = UIImageView()
        self.mapImageView = mapImageView
        self.mapImageView.isUserInteractionEnabled = false
        self.mapImageView.backgroundColor = UIColor.secondaryColor()
        self.mapImageView.contentMode = .scaleAspectFill
        self.mapImageView.isUserInteractionEnabled = true
        self.contentView.addSubview(self.mapImageView)

        self.updateConstraintsIfNeeded()
    }

    override func updateConstraints() {
        self.mapImageView.translatesAutoresizingMaskIntoConstraints = false
        constrain(mapImageView, contentView) { mapImageView, contentView in
            mapImageView.leading == contentView.leading
            mapImageView.trailing == contentView.trailing
            mapImageView.top == contentView.top
            mapImageView.height == 300
        }
        super.updateConstraints()
    }

    func centerMapOnLocation() {
        let location = CLLocationCoordinate2DMake(CurrentLocation.latitude, CurrentLocation.longitude)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location,
            kRadiusInMeter, kRadiusInMeter)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }

    func addLocationAnnotationsToMapView(_ mapView: MKMapView) {
        mapView.frame = self.mapImageView.frame
        mapView.isScrollEnabled = false
        mapView.isPitchEnabled = false
        mapView.isZoomEnabled = false
        mapView.showsUserLocation = false
        self.mapView = mapView
        self.mapImageView.addSubview(self.mapView)
        if CurrentLocation.latitude != 0 {
            self.addCurrentLocationAnimationView()
            self.centerMapOnLocation()
        }

    }

    func addCurrentLocationAnimationView() {
        let view:UIView = UIView(frame: CGRect(x: (self.contentView.superview?
            .frame.size.width)!/2 - 50, y: 100, width: 100, height: 100))
        view.layer.cornerRadius = 50
        view.backgroundColor = UIColor.primaryColorWithAlpha(0.4)
        let scaleAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 1
        scaleAnimation.repeatCount = 3000000000.0
        scaleAnimation.autoreverses = true
        scaleAnimation.fromValue = 1.2
        scaleAnimation.toValue = 0.8
        view.layer.add(scaleAnimation, forKey: "scale")
        self.currentLocationView = view
        self.mapImageView.addSubview(self.currentLocationView)
    }
}
