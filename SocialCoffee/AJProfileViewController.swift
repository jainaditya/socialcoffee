//
//  AJChatViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/19/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import MapKit

enum ProfileViewCell: Int {
    case map = 0
    case profile = 1
}
let kProfileCell = "ProfileCell"

class AJProfileViewController : AJViewController, UITableViewDataSource, RequestNearByPlacesDelegate
{
    @IBOutlet weak var tableView : UITableView!
    var requestNearByPlaces: AJRequestNearByPlaces?
    var mapView: MKMapView!
    var feedViewController: AJFeedViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 68.0
        self.tableView.register(AJProfileMapTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(AJProfileMapTableViewCell.self))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if CurrentLocation.available && CurrentLocation.latitude != 0 {
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
        if (self.requestNearByPlaces == nil) {
            self.requestNearByPlaces = AJRequestNearByPlaces()
            self.requestNearByPlaces?.delegate = self
        } else if (CurrentLocation.changed) {
            self.requestNearByPlaces?.fetchNearByPlaces()
        }
    }

    func reuseIdentifierForIndexPath(_ indexPath: IndexPath) ->  String {
        var reuseIdentifier: String
        let row: Int = indexPath.row

        switch row {
        case ProfileViewCell.map.rawValue:
            reuseIdentifier = NSStringFromClass(AJProfileMapTableViewCell.self)
            break
        case ProfileViewCell.profile.rawValue:
            reuseIdentifier = kProfileCell
            break
        default:
            reuseIdentifier = kProfileCell
        }
        return reuseIdentifier
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier: String = self.reuseIdentifierForIndexPath(indexPath)

        if (reuseIdentifier == NSStringFromClass(AJProfileMapTableViewCell.self)) {
            let cell: AJProfileMapTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AJProfileMapTableViewCell.self)) as! AJProfileMapTableViewCell
            cell.contentView.frame = self.view.frame
            if let mapView = self.mapView {

                cell.addLocationAnnotationsToMapView(mapView)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: kProfileCell, for: indexPath)
            cell.selectionStyle = .none
            cell.textLabel?.text = "My activity"
//            if indexPath.row == 2 {
//                cell.textLabel?.text = "Settings"
//            }

            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let reuseIdentifier: String = self.reuseIdentifierForIndexPath(indexPath)

        if (reuseIdentifier == NSStringFromClass(AJProfileMapTableViewCell.self)) {
            return 300
        } else {
            return 68
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            if feedViewController == nil {
                self.feedViewController = AJFeedViewController()
            }
            self.feedViewController!.loadUserFeeds  = true
            self.performSegue(withIdentifier: "userFeedViewControllerSegue", sender: indexPath)
        } else if indexPath.row == 2 {
            //show settings view controller
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        print(segue.identifier)
        if segue.destination is AJFeedViewController {
            let destinationVC = segue.destination as! AJFeedViewController
            destinationVC.loadUserFeeds  = true
            destinationVC.hidesBottomBarWhenPushed = true
        }
    }
    
    private func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    private func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func didReceiveNearByPlaces(_ responseObjects: JSON) {
        if (self.mapView == nil) {
            self.mapView = MKMapView()
            self.mapView?.delegate = self
        }
        for (_, placeDictionary): (String, JSON) in responseObjects {
            self.mapView?.addAnnotation(sippingLocationRepresentationOfJSONObject(placeDictionary))
        }
        self.tableView.reloadData()

    }
    func didFailNearByPlaces() {

    }

    func sippingLocationRepresentationOfJSONObject (_ placeDictionary: JSON) -> AJSippingLocationAnnotation {
        let latitude = placeDictionary["geometry"]["location"]["lat"].double!
        let longitude = placeDictionary["geometry"]["location"]["lng"].double!
        let coordiate = CLLocationCoordinate2DMake(latitude, longitude)
        var discipline = "bar"
        let types = placeDictionary["types"].array
        if types?.contains("cafe") == true {
            discipline = "cafe"
        }

        return AJSippingLocationAnnotation(title: placeDictionary["name"].string!, locationName: placeDictionary["name"].string!, discipline: discipline, coordinate: coordiate)
    }

    
}
