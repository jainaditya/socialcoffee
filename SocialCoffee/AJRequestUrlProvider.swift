//
//  AJRequestManager.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/16/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import Alamofire
extension Alamofire.Request {
    
    /** Response serializer for images from: http://www.raywenderlich.com/85080/beginning-alamofire-tutorial */
//    public static func imageResponseSerializer() -> DataResponseSerializer<UIImage> {
//        return DataResponseSerializer { request, response, data, error in
//            
//            guard let validData = data else {
//                let failureReason = "Data could not be serialized. Input data was nil."
////                let error = Error.errorWithCode(.DataSerializationFailed, failureReason: failureReason)
////                return .failure(error)
//            }
//            
//            if let image = UIImage(data: validData, scale: UIScreen.main.scale) {
//                return .success(image)
//            }
//            else {
////                return .failure(Error.errorWithCode(.DataSerializationFailed, failureReason: "Unable to create image."))
//            }
//            
//        }
//    }
}
struct RequestUrlProvider {
    enum Router: URLRequestConvertible {

        static let baseURLString = kBaseUrl
        static let consumerKey = "fa"
        static let parseApplicationId = "ZiS1lrP0H86D20tYFopirZQX5jZ3xrylz8B92n24"
        static let parseRestApiKey = "9iudn2re803WHv6KYLyFxKLoLXMwcjzD9v6xOTWR"
        static var OAuthToken: String?

        case users
        case createUser([String: AnyObject])
        case createFeed([String: AnyObject])
        case createVote([String: AnyObject])
        case createFeedFlag([String: AnyObject])
        case readFeed(String)
        case readFeedsNearMe([String: AnyObject])
        case updateUser([String: AnyObject])
        case readSettings()
        case createComment([String: AnyObject])
        case readComment(String)
        case readPlaceMetrics([String: AnyObject])

        var method: Alamofire.HTTPMethod {
            switch self {
            case .createFeed,
                .createUser,
                .createVote,
                .createFeedFlag,
                .createComment:
                return .post
            case .readFeedsNearMe,
                 .readSettings,
                 .readPlaceMetrics:
                return .get
            case .updateUser:
                return .put
            default:
                return .get
            }
        }

        var path: String {
            switch self {
            case .createFeed:
                return "/feed/"
            case .readFeedsNearMe:
                    return "/feed/findNearGeoPoint"
            case .readFeed(let feedId):
                    return "/feed/\(feedId)"
            case .createUser:
                return "/user/"
            case .createVote:
                return "/vote"
            case .createFeedFlag:
                return "/flag"
            case .updateUser:
                return "/user/\((AJSessionService.currentUser()?.objectId)!)"
            case .readSettings:
                return "/settings"
            case .createComment:
                return "/comment/"
            case .readComment(let commentFeedId):
                return "/comment/feed/\(commentFeedId)"
            case .readPlaceMetrics:
                return "/placeMetrics"
            default:
                return "/feed/"
            }
        }
        
        func asURLRequest() throws -> URLRequest {
            let url = URL(string: Router.baseURLString)!
            var mutableURLRequest = URLRequest(url: url.appendingPathComponent(path))
            mutableURLRequest.httpMethod = method.rawValue

            if let token = AJSessionService.currentUser()?.token {
                mutableURLRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }

            switch self {
            case .createUser(let parameters):
                return try Alamofire.JSONEncoding.default.encode(mutableURLRequest, with: parameters)
            case .readFeedsNearMe(let parameters):
                return try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .createFeed(let parameters):
                return try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .createVote(let parameters):
                return try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .createFeedFlag(let parameters):
                return try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .updateUser(let parameters):
                return try Alamofire.JSONEncoding.default.encode(mutableURLRequest, with: parameters)
            case .readSettings():
                return try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: nil)
            case.createComment(let parameters):
                return try Alamofire.JSONEncoding.default.encode(mutableURLRequest, with: parameters)
            case .readComment( _):
                return try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: nil)
            case .readPlaceMetrics(let parameters):
                return try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: parameters)
            default:
                return mutableURLRequest
            }
        }
    }
    
    enum ImageSize: Int {
        case tiny = 1
        case small = 2
        case medium = 3
        case large = 4
        case xLarge = 5
    }
}
