
//
//  AJSessionService.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 12/21/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation

class AJSessionService: NSObject {

    override init() {
        super.init()
    }

    class func currentUser() -> User? {
        var user: User?
        if let uuid = self.getUUIDFromUserDefaultSettings() {
            user = self.fetchUserObjectByUUID(uuid)!
        }
        return user
    }

    class func getUUIDFromUserDefaultSettings() -> String? {
        var uuid: String?
        let userDefaults = UserDefaults.standard
        uuid = userDefaults.string(forKey: UserAttributes.uuid.rawValue)

        return uuid;
    }

    class func addUUIDToUserDefaultSettings(_ user: User) {
        let userDefaults = UserDefaults.standard
        if userDefaults.object(forKey: UserAttributes.uuid.rawValue) == nil  {
            userDefaults.set(user.uuid, forKey: UserAttributes.uuid.rawValue)
            userDefaults.synchronize()
        }
    }

    class func fetchUserObjectByUUID(_ uuid: String) -> User? {
        var user: User?
//        if SESSION_TOKEN != nil && UUID != nil {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: User.entityName())
            fetchRequest.returnsObjectsAsFaults = false
            let resultPredicate = NSPredicate(format: "uuid = %@ or objectId = %@", uuid, uuid)
            fetchRequest.predicate = resultPredicate
            fetchRequest.fetchLimit = 1
        
            do {
                let results = try AJCachingService.sharedInstance.managedObjectContext!.fetch(fetchRequest)
                if let result = results.first {
                    user = result as? User
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            } catch {
                print("User fetch error")
            }
//        }

        return user
    }

}
