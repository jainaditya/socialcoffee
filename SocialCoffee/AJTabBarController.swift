//
//  AJTabBarController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/19/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit


class AJTabBarController : UITabBarController, UITabBarControllerDelegate
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tabBar.tintColor = UIColor.primaryColor()
        self.tabBar.addTopBorderWithHeight(1.0, color: UIColor.primaryColor(), leftOffset: 0, rightOffset: 0, topOffset: 0)
    }
}