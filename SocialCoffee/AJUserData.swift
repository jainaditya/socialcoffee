//
//  AJUser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/8/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DigitsKit

class AJUserData: NSObject
{
    let forceCreate = false
    override init() {
        super.init()
//        self.createNewUser()
    }
    
    internal func createNewUser(_ completion:@escaping (User) -> Void)
    {

        if AJSessionService.currentUser()?.objectId == nil || forceCreate  {
            let uuid = UUID().uuidString
            let paramDictionary = ["username": uuid, "uuid": uuid, "password": uuid]
            
            Alamofire.request(RequestUrlProvider.Router.createUser(paramDictionary as [String : AnyObject])).validate().responseJSON() {
                response in
                if response.result.isSuccess {
                    if let responseValue = response.result.value {
                        let responseObject = JSON(responseValue)
                        let user = User.updateDatabaseWithUserResponse(responseObject.dictionaryObject!, cachingService: AJCachingService.sharedInstance)
                        AJSessionService.addUUIDToUserDefaultSettings(user)
                        completion(user)
                    }

                } else {
//                    print(response.result.value ?? <#default value#> ?? "Error in create user")
                }
            }
        } else {
            self.loginUser()
        }
    }

    internal func updateCurrentUserVerification(_ session: DGTSession?) {
        if let _session = session {
            Alamofire.request(RequestUrlProvider.Router.updateUser([UserAttributes.phone.rawValue: _session.phoneNumber as AnyObject, UserAttributes.verified.rawValue: true as AnyObject, "digitsId": _session.userID as AnyObject])).validate().responseJSON(completionHandler: { (response) in
                if response.result.isSuccess {
                    if let responseValue = response.result.value {
                        let responseObject = JSON(responseValue)
                        let user = User.updateDatabaseWithUserResponse(responseObject.dictionaryObject!, cachingService: AJCachingService.sharedInstance)
                        AJSessionService.addUUIDToUserDefaultSettings(user)
                    }
                } else {
                    print(response.result.value)
                }
            })
        }
    }
    
    fileprivate func loginUser()
    {
        //kCurrentUser = User.fetchUserObjectByUUID()
    }

    internal func currentUser() -> AJUser? {

        var userObject = [AnyHashable: Any]()
        let userDictionary = AJSessionService.currentUser()
        
        userObject[UserAttributes.objectId.rawValue] = userDictionary?.objectId
        userObject[UserAttributes.phone.rawValue] = userDictionary?.phone
        userObject[UserAttributes.name.rawValue] = userDictionary?.name
        userObject[UserAttributes.verified.rawValue] = userDictionary?.verified


        let user = (try! MTLJSONAdapter.model(of: AJUser.self, fromJSONDictionary: userObject, error: ())) as! AJUser

        return user
    }

}
