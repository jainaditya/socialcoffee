//
//  AJViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/20/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class AJViewController: UIViewController
{
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tabBarController?.tabBar.isTranslucent = false
        self.edgesForExtendedLayout = UIRectEdge()
        isModal()
    }
    
    func isModal() -> Bool
    {
        
        let isModal : Bool = ((self.presentingViewController != nil && self.presentingViewController?.presentedViewController == self) ||  (self.navigationController != nil && self.navigationController?.presentingViewController != nil && self.navigationController?.presentingViewController?.presentedViewController == self.navigationController))
        return isModal
    }
    
    func isRootViewController() -> Bool
    {
//        if (self.navigationController?.viewControllers.count > 0 && ( (self.navigationController?.viewControllers.contains(self))! || 
        if (self.navigationController?.viewControllers.count > 0 &&  (self.navigationController?.viewControllers.contains(self.parent!))!) {
            return false;
        }
            return true;
    }
    
    func isModalRootViewController() -> Bool
    {
        if (isModal() && isRootViewController()) {
            return true;
        }
        return false;
    }
    
    func returnToPreviousView()
    {
        if (self.isModalRootViewController()) {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        } else {
            //[self.navigationController popViewControllerAnimated:YES];
        }
    }
    
    
}
