//
//  AJFeedRequestSynchronizer.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/20/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
@objc
protocol FeedRequestSynchronizerDelegate {
    @objc optional func feedSaveOperationWithImage(_ progress: Float)
    @objc optional func didEndFeedFetchOperation(_ fetchedObjects: [AnyObject])
    func feedSaveOperationDidEnd()
    @objc optional func feedSaveOperationFailed(_ error: AnyObject)
}

class AJFeedRequestSynchronizer: NSObject
{
    var feedSaveDelegate: FeedRequestSynchronizerDelegate?
    var feedMessage: String?
    var feedImage: UIImage?
    var place: AJPlace?
    var params = [String: AnyObject]()
    
    
    override init() {
        super.init()
    }
    
    convenience init(feedImage: UIImage?, feedMessage: String, place: AJPlace, params: [String: AnyObject]) {
        self.init()
        self.feedMessage = feedMessage
        self.feedImage  = feedImage
        self.place = place
        self.params = params
    }
    
    func updateDatabaseForFetchRequest(_ loadUserFeeds: Bool, params: [String: AnyObject])
    {
        var params = params
        params[FeedAttributes.longitude.rawValue] = CurrentLocation.longitude as AnyObject?
        params[FeedAttributes.latitude.rawValue] = CurrentLocation.latitude as AnyObject?
        params["radius"] = String(10) as AnyObject?
        params["userFeeds"] = loadUserFeeds as AnyObject?

        //self.willDeleteLocalFeeds()
        Alamofire.request(RequestUrlProvider.Router.readFeedsNearMe(params))
            .responseJSON { response in
                self.feedSaveDelegate?.didEndFeedFetchOperation!([])
                if let responseValue = response.result.value {
                    let responseObjects = JSON(responseValue)
                    Feed.updateDatabaseWithFeedResponse(responseObjects, cachingService: AJCachingService.sharedInstance)
                    self.feedSaveDelegate?.didEndFeedFetchOperation!([])
                } else {
                    print("ERROR")
                    print(response.result.error.debugDescription)
                    self.feedSaveDelegate?.didEndFeedFetchOperation!([])
                }
        }
    }
    
    func performFeedSaveOperation()
    {
        updateDatabaseToSaveMessage(nil, feedObject: params)
    }
    
    func updateDatabaseToSaveMessage(_ imageObject: AnyObject?, feedObject: [String: AnyObject])
    {
        // Make a new post
        var feedObject = feedObject
        feedObject[FeedAttributes.message.rawValue] = self.feedMessage as AnyObject?
//        feedObject["userId"] = AJUserData().currentUser()?.objectId
        feedObject[FeedAttributes.latitude.rawValue] = String(format:"%f", CurrentLocation.latitude) as AnyObject?
        feedObject[FeedAttributes.longitude.rawValue] = String(format:"%f", CurrentLocation.longitude) as AnyObject?
        feedObject["place.name"] = place?.name as AnyObject?
        feedObject["place.vicinity"] = place?.vicinity as AnyObject?
        feedObject["place.id"] = place?.id as AnyObject?
        feedObject["place.placeId"] = place?.placeId as AnyObject?
        feedObject["place.latitude"] = String(format: "%f", (place?.location.latitude)!) as AnyObject?
        feedObject["place.longitude"] = String(format: "%f", (place?.location.longitude)!) as AnyObject?

        var tokenHeader:[String:String]! = ["":""]
        if let token = AJSessionService.currentUser()?.token {
            tokenHeader = ["Authorization":"Bearer \(token)"]
        }

        Alamofire.upload(multipartFormData: { (MultipartFormData) in
            for (key, value) in feedObject {
//                MultipartFormData.appendBodyPart(data: value.dataUsingEncoding(String.Encoding.utf8)!, name: key)
                MultipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            if let _ = self.feedImage {
                let image = UIImageJPEGRepresentation(self.feedImage!, 0.8)!
                MultipartFormData.append(image, withName: "image", fileName: "\(NSUUID().uuidString).jpg", mimeType: "image/jpeg")
            }
        }, to: "\(kBaseUrl)/feed", method: .post, headers: tokenHeader, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    DispatchQueue.main.async {
                        self.feedSaveDelegate?.feedSaveOperationWithImage!(Float(Progress.fractionCompleted))
                    }
                })
                upload.responseJSON { response in

                    if response.response?.statusCode == 401 {
                        self.feedSaveDelegate?.feedSaveOperationFailed!(JSON(response.result.value!).dictionaryObject! as AnyObject)
                    } else if response.result.isFailure {
                        self.feedSaveDelegate?.feedSaveOperationFailed!(response.result.error as AnyObject)
                    } else {
                        if let responseValue = response.result.value {
                            let responseObject = JSON([responseValue])
                            Feed.updateDatabaseWithFeedResponse(responseObject, cachingService: AJCachingService.sharedInstance)
                            self.feedSaveDelegate!.feedSaveOperationDidEnd()
                        }
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }

    func fetchPlaceMetrics(params: [String: AnyObject], completion: @escaping (JSON) -> Void) {
        //self.willDeleteLocalFeeds()
        Alamofire.request(RequestUrlProvider.Router.readPlaceMetrics(params))
            .responseJSON { response in
                self.feedSaveDelegate?.didEndFeedFetchOperation!([])
                if let responseValue = response.result.value {
                    let responseObjects = JSON(responseValue)
                    completion(responseObjects)
                } else {
                    print("ERROR")
                    print(response.result.error.debugDescription)
//                    self.feedSaveDelegate?.didEndFeedFetchOperation!([])
                }
        }
    }
    
    func willDeleteLocalFeeds() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: AJFeed.managedObjectEntityName(), in: AJCachingService.sharedInstance.managedObjectContext! )
        fetchRequest.includesPropertyValues = false
        
        do {
            let results = try AJCachingService.sharedInstance.managedObjectContext?.fetch(fetchRequest) as? [NSManagedObject]
            
            for result in results! {
                AJCachingService.sharedInstance.managedObjectContext?.delete(result)
            }
            
            try AJCachingService.sharedInstance.managedObjectContext?.save()
            
        } catch {
            print("error: \(error)")
        }
    }
}
