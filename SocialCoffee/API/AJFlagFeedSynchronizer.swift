//
//  AJFlagFeedSynchronizer.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 4/9/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AJFlagFeedSynchronizer: NSObject
{
    var feedSaveDelegate: FeedRequestSynchronizerDelegate?
    var feedObject: Feed?

    override init() {
        super.init()
    }

    convenience init(feed: Feed) {
        self.init()
        self.feedObject = feed
        self.requestCreateFeedFlag()
    }

    func requestCreateFeedFlag() {
        var flagObject = [String: AnyObject]()

        flagObject["feedId"] = self.feedObject!.objectId as AnyObject?
//        if let currentUser = AJUserData().currentUser() {
//            flagObject["userId"] = currentUser.objectId
//        }

        Alamofire.request(RequestUrlProvider.Router.createFeedFlag(flagObject))
        .responseJSON { response in
            if response.result.isSuccess {
                if let responseValue = response.result.value {
                    let responseObject = JSON([responseValue])
                    Feed.willDeleteLocalFeedObject((self.feedObject?.objectId)!)

                }
            }
        }
    }
}
