//
//  AJRequestFourSquarePlaces.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/14/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

class  AJRequestFourSquarePlaces :AJRequestNearByPlaces {

    let fsBaseUrl = "https://api.foursquare.com/v2"
    let fsApiVersion = 20161016
    let fsIntent="checkin"
    let fsCategoryIds="4bf58dd8d48988d1e0931735,4bf58dd8d48988d16d941735,4bf58dd8d48988d116941735,4d4b7105d754a06376d81259,4d4b7105d754a06374d81259,4bf58dd8d48988d1cc941735"
    let fsClientId = "PVCNAIL43QP104ZTPJOVMABFHLTSREXNVOZMGLBZTME5NVLC"
    let fsClientSecret = "AF4BIJN3UJ0KOCD1GGIRZMOYPJNAREDM45Q25QD0CFQBLT4U"
    var searchQuery = String()
    var params: [String: AnyObject] = [:]
    var uri = "/venues/search"

    init(uri: String, params: [String: AnyObject], searchQuery: String?) {
        self.params = params
        self.uri = uri
        if let _query = searchQuery {
            self.searchQuery = _query
        }
        super.init()
    }

    override init() {
        super.init()
    }

    override func getBaseUrl () -> String {
        return self.fsBaseUrl + uri
    }

    override func getRequestParams() -> [String: AnyObject] {

        params["v"] = fsApiVersion as AnyObject?
        params["ll"] = String(CurrentLocation.latitude) + "," + String(CurrentLocation.longitude) as AnyObject?
        if params["radius"] == nil {
            params["radius"] = String(5200) as AnyObject?
        }
//        params["categoryId"] = fsCategoryIds as AnyObject?
        params["limit"] = String(limit) as AnyObject?
        params["intent"] = fsIntent as AnyObject?
        params["client_id"] = fsClientId as AnyObject?
        params["client_secret"] = fsClientSecret as AnyObject?
        if searchQuery.characters.count > 2 {
//            params["radius"] = String(4000) as AnyObject?
            params["query"] = searchQuery as AnyObject?
        }

        return params
    }

    override func handleResponse(_ response: JSON) {
        if uri == "/venues/search" {
            self.delegate?.didReceiveNearByPlaces(response["response"]["venues"])
        } else {
            self.delegate?.didReceiveNearByPlaces(response["response"]["groups"][0]["items"])
        }
    }
    
}
