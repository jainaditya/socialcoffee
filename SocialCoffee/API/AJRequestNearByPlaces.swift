
//
//  AJRequestNearByPlaces.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/25/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol RequestNearByPlacesDelegate {
    func didReceiveNearByPlaces(_ responseObjects: JSON)
    func didFailNearByPlaces()
}
class AJRequestNearByPlaces: NSObject {
    var baseURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    let googleApikey = "AIzaSyCgzU3vJgGZwVmnax0KzhvlPrxySCST8oc"
    let radius = kRadiusInMeter //  5 mile radius
    let placeType = "bar|night_club|restaurant|cafe|bowling_alley"
    var delegate: RequestNearByPlacesDelegate?
    let limit = 50

    override init() {
        super.init()
        self.fetchNearByPlaces()
    }

    func fetchNearByPlaces() {
        Alamofire.request(self.getBaseUrl(), method: .get, parameters: getRequestParams())
        .responseJSON { response in
            if let responseValue = response.result.value {
                let responseObjects = JSON(responseValue)
                self.handleResponse(responseObjects)
            } else {
                print("ERROR")
                print(response.result.error.debugDescription)
            }
        }
    }

    func handleResponse(_ response: JSON) {
        self.delegate?.didReceiveNearByPlaces(response["results"])
    }

    func getBaseUrl () -> String {
        return baseURL
    }

    func getRequestParams() -> [String: AnyObject] {
        return self.getGoogleRequestParams()

    }

    func getGoogleRequestParams() -> [String: AnyObject] {
        var params: [String: AnyObject] = [:]
        params["key"] = self.googleApikey as AnyObject?
        params["location"] = String(CurrentLocation.latitude) + "," + String(CurrentLocation.longitude) as AnyObject?
        params["radius"] = String(radius) as AnyObject?
        params["type"] = placeType as AnyObject?

        return params
    }
}
