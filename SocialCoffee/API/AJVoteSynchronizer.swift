//
//  AJVoteSynchronizer.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/3/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AJVoteSynchronizer: NSObject
{
    var feedId: String?
    var feedObject: Feed?
    var ajFeedObject: AJFeed?
    var voteUpDown: NSNumber?
    
    override init() {
        super.init()
    }
    
    convenience init(feedId: String?, voteUpDown: NSNumber, feedObject: Feed) {
        self.init()
        self.feedId = feedId
        self.voteUpDown = voteUpDown
        self.feedObject = feedObject
        
        self.willSaveVoteToDatabase()
    }
    
    convenience init(feedId: String?, feedObject: AJFeed) {
        self.init()
        self.feedId = feedId
        self.ajFeedObject = feedObject
//        updateDatabaseForFetchRequest()
    }
    
    func updateDatabaseForFetchRequest() {
//        self.buildVoteQueryAndSaveLocally(nil)
    }
    
    func willSaveVoteToDatabase() {
        let vote =  self.feedObject!.votes.filtered(using: NSPredicate(format: "user.objectId = %@", (AJSessionService.currentUser()?.objectId)! ))

        var voteObject = [String: AnyObject]()
        if vote.count > 0 {
            let vote = vote.first as! Vote
            // UPDATE VOTE
            voteObject[VoteAttributes.objectId.rawValue] = vote.objectId as AnyObject?
            if vote.updown != self.voteUpDown! {
                voteObject["vote"] = self.voteUpDown!
            } else {
                voteObject["vote"] = 0 as AnyObject?
            }
        } else {
            //create vote
            voteObject["vote"] = self.voteUpDown
        }

        voteObject["feedId"] = self.feedObject!.objectId as AnyObject?
        
        var feed = AJFeed()
        do {
            feed = try MTLManagedObjectAdapter.model(of: AJFeed.self, from: self.feedObject) as! AJFeed
        } catch let error as NSError {
            print(error)
        } catch {
            fatalError()
        }

        Alamofire.request(RequestUrlProvider.Router.createVote(voteObject))
        .responseJSON { response in
            if response.result.isSuccess {
                if let responseValue = response.result.value {
                    let responseObject = JSON([responseValue])
                    Vote.convertVotesArrayToDictionary(votesArray: responseObject, feed: feed)
                }
            }
        }
    }
    
//    func buildVoteQueryAndSaveLocally(_ objectId: String?) {
//        let innerQuery = PFQuery(className: Feed.entityName())
//        
//        if objectId != nil {
//            innerQuery.whereKey("objectId", equalTo: objectId!)
//        } else {
//            innerQuery.whereKey("location", nearGeoPoint: PFGeoPoint(latitude: CurrentLocation.latitude, longitude: CurrentLocation.longitude), withinMiles: kFilterDistanceInMiles)
//            innerQuery.whereKey("createdAt", greaterThanOrEqualTo: LastFetchedFeedsAt)
//        }
//
//        let query = PFQuery(className: Vote.entityName())
//        query.includeKey("feedId")
//        query.includeKey("feedId.imageId")
//        query.includeKey("feedId.User")
//        query.includeKey("userId")
//        query.whereKey("feedId", matchesQuery: innerQuery)
//        query.findObjectsInBackgroundWithBlock {
//            (objects, error) -> Void in
//            if error == nil {
//                if let objects = objects as? [PFObject] {
//                    //Vote.updateDatabaseWithVoteResponse(objects, cachingService: AJCachingService.sharedInstance)
//                }
//            }
//        }
//    }
    
//    func saveVoteLocally(_ voteObject: PFObject) {
//        let entity =  NSEntityDescription.entity(forEntityName: Vote.entityName(),
//            in:AJCachingService.sharedInstance.managedObjectContext!)
//        
//        let vote = NSManagedObject(entity: entity!,
//            insertInto: AJCachingService.sharedInstance.managedObjectContext!)
//        
//        vote.setValue(voteObject[VoteAttributes.updown.rawValue], forKey: VoteAttributes.updown.rawValue)
//        vote.setValue(voteObject.objectId, forKey: VoteAttributes.objectId.rawValue)
//        //Feed.feedRepresentationOfFeedDictionary(voteObject)
//        
//        let feed = voteObject["feedId"] as! PFObject
//        vote.setValue(Feed.fetchFeedObject(feed.objectId!), forKey: VoteRelationships.feed.rawValue)
//
//        let user = voteObject["userId"] as! PFUser
////        if user ==  PFUser.currentUser() {
//            //vote.setValue(User.fetchUserObject(user), forKey: VoteRelationships.user.rawValue )
////        }
//        
//        do {
//            try AJCachingService.sharedInstance.managedObjectContext!.save()
//        } catch let error as NSError  {
//            print("Could not save \(error), \(error.userInfo)")
//        }
//    }
}
