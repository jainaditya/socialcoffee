//
//  CommentRequestSynchronizer.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/21/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CommentRequestSynchronizer: NSObject {

    func createCommentWithMessage(_ message: String, feedId: String, completion: @escaping (NSArray) -> Void, failure: @escaping (HTTPURLResponse, NSError) -> Void) {
        var params = [String: AnyObject]()
        params["message"] = message as AnyObject?
        params["feedId"] = feedId as AnyObject?
        params["userId"] = AJSessionService.currentUser()?.objectId as AnyObject?
        Alamofire.request(RequestUrlProvider.Router.createComment(params))
            .responseJSON { response in
                if response.result.isSuccess {
                    if let responseValue = response.result.value {
                        let responseObjects = JSON(responseValue)
                        let comments = CommentParser.convertCommentResponseToComments(responseObjects)
                        completion(comments)
                    }
                } else {
                    if let httpResponse = response.response {
                        failure(httpResponse, response.result.error! as NSError)
                    }
                }
        }
    }

    func fetchComments(_ feedId: String, completion: @escaping (NSArray) -> Void, failure: @escaping (HTTPURLResponse, NSError) -> Void) {
//        var params = [String: AnyObject]()
//        params["feedId"] = feedId
        Alamofire.request(RequestUrlProvider.Router.readComment(feedId))
            .responseJSON { response in
                if response.result.isSuccess {
                    if let responseValue = response.result.value {
                        let responseObjects = JSON(responseValue)
                        let comments = CommentParser.convertCommentResponseToComments(responseObjects)
                        completion(comments)
                    }
                } else {
                    if let httpResponse = response.response {
                        failure(httpResponse, response.result.error! as NSError)
                    }
                }
        }
    }
}
