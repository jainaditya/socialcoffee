//
//  AJFeedCommentsViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/18/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class AJFeedCommentsViewController: AJViewController, UITableViewDataSource, UITextViewDelegate {

    var tableView: UITableView!
    var feed: Feed!
    var messageTextView: UITextView!
    var sendButton: UIButton!
    var placeholderLabel: UILabel!
    var keyboardHeight = CGFloat(0)
    let messageTextViewHolderHeight = CGFloat(55)
    var comments: [Comment] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeSubviews()
        tableView.register(AJFeedTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(AJFeedTableViewCell.self))
        tableView.register(AJFeedImageTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(AJFeedImageTableViewCell.self))
        tableView.register(AJNoResultsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(AJNoResultsTableViewCell.self))
        tableView.register(CommentTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(CommentTableViewCell.self))

        fetchAllComments()
        attachKeyboardHideGesture()
    }

    func attachKeyboardHideGesture() {
        let swipeDownGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(AJFeedCommentsViewController.handleSwipeDown(_:)))
        swipeDownGesture.direction = .down
        tableView.addGestureRecognizer(swipeDownGesture)
        messageTextView.addGestureRecognizer(swipeDownGesture)
        swipeDownGesture.numberOfTouchesRequired = 1

        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleSwipeDown(_:)))
        tapGesture.numberOfTapsRequired = 1
        tableView.addGestureRecognizer(tapGesture)
    }

    func handleSwipeDown(_ gesture: UISwipeGestureRecognizer) {
        messageTextView.resignFirstResponder()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(AJFeedCommentsViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)

        NotificationCenter.default.addObserver(self, selector: #selector(AJFeedCommentsViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }

    func initializeSubviews() {
        view.backgroundColor = UIColor.defaultBarColor()
        tableView = UITableView(frame: view.frame)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        view.addSubview(tableView)
        tableView.backgroundColor = UIColor.defaultImageBackgroundColor()

        messageTextView = UITextView()
        messageTextView.delegate = self
        view.addSubview(messageTextView)
        messageTextView.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(18)
        messageTextView.textColor = UIColor.primaryTextColor()


        placeholderLabel = UILabel()
        placeholderLabel.text = "Say or ask something about this place..."
        placeholderLabel.font = messageTextView.font
        placeholderLabel.textColor = UIColor.secondaryTextColor()
        messageTextView.addSubview(placeholderLabel)

        sendButton = UIButton()
        sendButton.setImage(UIImage(named: "send"), for: .normal)
        sendButton.setImage(UIImage(named: "send-active"), for: .highlighted)
        sendButton.backgroundColor = UIColor.white
        sendButton.addTarget(self, action: #selector(AJFeedCommentsViewController.sendButtonClicked), for: .touchUpInside)
        view.addSubview(sendButton)
        sendButton.isEnabled = false

        view.setNeedsUpdateConstraints()
    }

    override func updateViewConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        messageTextView.translatesAutoresizingMaskIntoConstraints = false
        sendButton.translatesAutoresizingMaskIntoConstraints = false

        constrain(view, tableView) { view, tableView in
            tableView.width == view.width
            tableView.height == view.height - messageTextViewHolderHeight
        }

        constrain(tableView, messageTextView, sendButton) { tableView, messageTextView, sendButton in
            messageTextView.top == tableView.bottom + 5
            messageTextView.leading == tableView.leading + 5
            messageTextView.width == tableView.width - 70
            messageTextView.height == 45

            sendButton.leading == messageTextView.trailing
            sendButton.height == messageTextView.height
            sendButton.centerY == messageTextView.centerY
            sendButton.trailing == tableView.trailing - 5
        }
        
        constrain(placeholderLabel) { placeholderLabel in
            placeholderLabel.leading == (placeholderLabel.superview?.leading)! + 7
            placeholderLabel.centerY == (placeholderLabel.superview?.centerY)!
            placeholderLabel.height == (placeholderLabel.superview?.height)!
            messageTextView.textContainerInset = UIEdgeInsetsMake(CGFloat((placeholderLabel.superview?.centerY.attribute.rawValue)!), 0, 0, 0)

        }

        super.updateViewConstraints()
        view.layoutIfNeeded()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if comments.count > 0 {
            return comments.count + 1
        }
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.rowHeight = UITableViewAutomaticDimension
        if indexPath.row == 0 {
            return feedCell()
        } else if comments.count > 0 && indexPath.row > 0 {
            let cell: CommentTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(CommentTableViewCell.self)) as! CommentTableViewCell
            let comment = comments[indexPath.row-1]
            cell.messageLabel.text = comment.message
            cell.timeLabel.text = comment.createdAt.relativeDateString
            return cell
        } else {
            let cell:AJNoResultsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AJNoResultsTableViewCell.self)) as! AJNoResultsTableViewCell
            var frame = self.view.frame
            frame.size.height = 300
            cell.contentView.frame = frame
            self.tableView.rowHeight = 180
            cell.createSubviews()
            cell.populateNoResultView("NO COMMENTS YET.\nYOU CAN START A CONVERSATION BY SAYING OR ASKING SOMETHING ABOUT THIS PLACE.", icon: UIImage(named: "chat")!)
            return cell
        }

    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 500
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }


    func feedCell() -> UITableViewCell {
        if let _ = feed.image?.path {
            let cell: AJFeedImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AJFeedImageTableViewCell.self)) as! AJFeedImageTableViewCell
            setupCell(cell, feed: feed)
            return cell
        } else {
            let cell:AJFeedTableViewCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AJFeedTableViewCell.self)) as! AJFeedTableViewCell
            setupCell(cell, feed: feed)
            return cell
        }
    }

    func setupCell(_ cell: AJFeedTableViewCell, feed: Feed) {
        cell.contentView.frame = view.frame
        cell.feedVoteCount.text = "\(feed.votes.value(forKeyPath: "@sum.updown")!)"
        cell.populateWithFeed(feed, feedViewController: self)
        cell.feedVoteCount.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(20)
        cell.downVoteButton.isHidden = true
        cell.upVoteButton.isHidden = true
        cell.flagFeedButton.isHidden = true
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let string = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if text == "\n" || string.characters.count == 0 && textView.text.characters.count == 0 {
            return false
        }

        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {

    }

    func textViewDidChange(_ textView: UITextView) {
        if textView.text.characters.count == 0 {
            textView.textColor = UIColor.secondaryTextColor()
            placeholderLabel.isHidden = false
        }

        let string = textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if string.characters.count > 0  {
            placeholderLabel.isHidden = true
            toggleSendButton(true)
        }else {
            toggleSendButton(false)
        }
    }

    func toggleSendButton(_ toggle: Bool) {
        sendButton.isEnabled = toggle
        sendButton.isHighlighted = toggle
    }

    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardFrameStart: CGRect = ((userInfo[UIKeyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue)!

        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.frame.size.height -= keyboardFrameStart.size.height
            self.view.setNeedsUpdateConstraints()
        })
    }

    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = sender.userInfo!
        let keyboardFrameEnd: CGRect = ((userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue)!

        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.frame.size.height = keyboardFrameEnd.origin.y - (self.messageTextViewHolderHeight +  10)
        })

    }

    func sendButtonClicked() {
        CommentRequestSynchronizer().createCommentWithMessage(messageTextView.text, feedId: feed.objectId!, completion: createCommentSuccess, failure: fail)
        sendButton.isEnabled = false
        messageTextView.text = ""
    }

    func createCommentSuccess(_ comments: NSArray) -> Void {
        self.comments.append(comments[0] as! Comment)
        tableView.reloadData()
        let indexPath = IndexPath(row: self.comments.count, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }

    func fail(_ response: HTTPURLResponse, error: NSError) -> Void {

    }

    func fetchAllComments() -> Void {
        CommentRequestSynchronizer().fetchComments(feed.objectId!, completion: fetchCommentSuccess, failure: fail)
    }

    func fetchCommentSuccess(_ comments: NSArray) -> Void {
        self.comments = comments as! [Comment]
        tableView.reloadData()
    }

}
