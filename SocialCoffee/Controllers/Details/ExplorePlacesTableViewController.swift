//
//  ExplorePlacesTableViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/23/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import XLPagerTabStrip
import SwiftyJSON
import UIKit
import APESuperHUD

class ExplorePlacesTableViewController: AJViewController, IndicatorInfoProvider, RequestNearByPlacesDelegate, AJGeoLocationProviderDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchControllerDelegate {
    var tableView: UITableView!
    var blackTheme = false
    var itemInfo = IndicatorInfo(title: "View")
    lazy var places: [AJPlaceFourSquare]  = []
    lazy var filteredPlaces: [AJPlaceFourSquare]  = []
    var requestNearByPlaces: AJRequestFourSquarePlaces!
    var placeType = String()
    let searchController = UISearchController(searchResultsController: nil)
    var fetchedResultsController : NSFetchedResultsController<NSFetchRequestResult>?
    var placeIds = [String]()
    var placeMetricsObject: JSON?

    lazy var locationProvider : AJGeoLocationProvider = {
        let locationProvider = AJGeoLocationProvider.sharedInstance
        locationProvider.delegate = self
        return locationProvider
    }()
    let refreshControl = UIRefreshControl()

    init(style: UITableViewStyle, itemInfo: IndicatorInfo, placeType: String) {
        self.itemInfo = itemInfo
        self.placeType = placeType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        refreshControl.addTarget(self, action: #selector(ExplorePlacesTableViewController.refreshFeeds(_:)), for: UIControlEvents.valueChanged)
        tableView = UITableView(frame: view.frame)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        view.addSubview(tableView)
        tableView.register(ExploreTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(ExploreTableViewCell.self))
        tableView.register(CommentTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(CommentTableViewCell.self))
        tableView.estimatedRowHeight = 44.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = .groupTableViewBackground
        self.tableView.addSubview(refreshControl)
        self.tableView.separatorStyle = .none
        refreshFeeds(refreshControl)
//        setupSearchController()
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Loading...", presentingView: self.view)
    }

    func setupSearchController() {
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        self.parent?.navigationItem.titleView = searchController.searchBar
        searchController.searchBar.sizeToFit()
        searchController.searchBar.barTintColor = UIColor.defaultBarColor()
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.placeholder = "Search by name"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if CurrentLocation.changed {
            loadPlaces(query: nil)
        }

        if kSubmittedLiveFeed {
            kSubmittedLiveFeed = false
            fetchPlaceMetrics()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        let searchField = searchController.searchBar.value(forKey: "_searchField")
        if let searchField = searchField as? UITextField {
            searchField.textAlignment = .left
            let placeholderLabel = searchField.value(forKey: "_placeholderLabel") as? UILabel
            placeholderLabel?.textAlignment = .left
        }
        var frame = view.frame
        frame.origin.x = 0
        frame.size.height = view.frame.size.height
        tableView.frame = frame
        self.parent?.navigationItem.title = "Explore"
    }

    func refreshFeeds(_ sender: UIRefreshControl) {
        sender.beginRefreshing()
        AJGeoLocationProvider.sharedInstance.delegate = self
        self.locationProvider.startUpdatingLocation()
    }

    func loadPlaces(query: String?) {
        var params: [String: AnyObject] = [:]
        if query == nil {
            params["section"] = placeType as AnyObject?
        }
        params["time"] = "any" as AnyObject?
        params["day"] = "any" as AnyObject?
        params["venuePhotos"] = "1" as AnyObject?
        params["sortByDistance"] = "1" as AnyObject?

        requestNearByPlaces = AJRequestFourSquarePlaces(uri: "/venues/explore", params: params, searchQuery: query)
//        if let _query = query {
//            requestNearByPlaces.searchQuery = _query
//        }
        requestNearByPlaces.delegate = self
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching() {
            return filteredPlaces.count
        }
        return places.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(ExploreTableViewCell.self)) as! ExploreTableViewCell
        let place = getPlaceForIndexPath(indexPath: indexPath)
        cell.addButtonTapped = {
            self.addButtonClicked(cell: cell)
        }
        cell.populateCellWithPlace(place: place, type: placeType, placeMetrics: placeMetricsObject)
        return cell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    @nonobjc internal func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    @nonobjc internal func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    private func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = getPlaceForIndexPath(indexPath: indexPath)
        let feedViewController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "AJFeedViewController") as! AJFeedViewController
        feedViewController.loadUserFeeds  = false
        feedViewController.selectedPlace = place
        feedViewController.hideFloatingButton = true
//        DispatchQueue.main.async(execute: {
            feedViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(feedViewController, animated: true)
//        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        print(segue.identifier)
        if segue.destination is AJFeedViewController {
            let destinationVC = segue.destination as! AJFeedViewController
            destinationVC.loadUserFeeds  = true
            destinationVC.hidesBottomBarWhenPushed = true
            destinationVC.selectedPlace = sender as? AJPlace
        }
    }

    // MARK: - IndicatorInfoProvider

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

    func addButtonClicked(cell: UITableViewCell) {
        let indexPath = self.tableView.indexPath(for: cell)
        let place = getPlaceForIndexPath(indexPath: indexPath!)
        let feedOptionsViewController = FeedOptionsViewController(title: "Report", place:  place)
//        let navigationController = AJNavigationController(rootViewController: feedOptionsViewController)
        feedOptionsViewController.hidesBottomBarWhenPushed = true
        DispatchQueue.main.async(execute: {
            self.navigationController?.pushViewController(feedOptionsViewController, animated: true)
//            (feedOptionsViewController, animated: true, completion: nil)
            self.hidesBottomBarWhenPushed = true
            if #available(iOS 10.0, *) {
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.success)
            } else {
                // Fallback on earlier versions
            }
        })
    }

    func getPlaceForIndexPath(indexPath: IndexPath) -> AJPlaceFourSquare {
        let place: AJPlaceFourSquare
        if  isSearching() && filteredPlaces.count > 0 {
            place = filteredPlaces[indexPath.row]
        } else {
            place = places[indexPath.row]
        }
        return place
    }



    // MARK: RequestNearByPlacesDelegate
    func didReceiveNearByPlaces(_ responseObjects: JSON) {
        endRefresh()
        if !isSearching() {
            places.removeAll()
        }
        
        for (_, placeDictionary): (String, JSON) in responseObjects {
            if let place: AJPlaceFourSquare =  AJPlaceFourSquare(json: placeDictionary["venue"]) {
                placeIds.append(place.id)
                if (isSearching()) {
                    filteredPlaces.append(place)
                } else {
                    self.places.append(place)
                }
            }
        }
        fetchPlaceMetrics()
        hideHUD()
    }

    func fetchPlaceMetrics() {
        if placeIds.count > 0 {
            var params = [String: AnyObject]()
            params["placeIds"] = placeIds.joined(separator: ",") as AnyObject
            AJFeedRequestSynchronizer().fetchPlaceMetrics(params: params, completion: didReceivePlaceMetrics)
        }
    }

    func didReceivePlaceMetrics(_ response: JSON) -> Void {
        placeMetricsObject = response
        var filtered: [AJPlaceFourSquare] = []

        if let _ids = (placeMetricsObject?.dictionaryObject as? NSDictionary)?.allKeys {
            filtered = (places as NSArray).filtered(using: NSPredicate(format: "id IN %@", _ids)) as! [AJPlaceFourSquare]
        }
        var _places = filtered
        for item in filtered {
           places.remove(at: places.index(of: item)!)
        }
        _places.append(contentsOf: places)
        places = _places
        tableView.reloadData()
    }

    func didFailNearByPlaces() {
        endRefresh()
    }

    // MARK: AJGeoLocationProviderDelegate
    
    func didUpdateFeedLocation() {
        AJGeoLocationProvider.sharedInstance.stopUpdatingLocation()
        loadPlaces(query: nil)
    }

    func endRefresh() {
        self.refreshControl.endRefreshing()
    }

    func didFailToUpdateLocation() {
        self.tableView.reloadData()
        refreshControl.endRefreshing()
        hideHUD()
    }

    //MARK: SearchController

    func isSearching() -> Bool {
        return searchController.isActive && searchController.searchBar.text != ""
    }

    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        if (searchController.searchBar.text?.characters.count)! > 3 {
            filteredPlaces.removeAll()
            loadPlaces(query: searchController.searchBar.text)
        } else {
            self.filteredPlaces = self.places.filter { place in
                return place.name.lowercased().contains(searchText.lowercased())
            }
            tableView.reloadData()
        }
    }

    func willPresentSearchController(_ searchController: UISearchController) {
    }

    func didPresentSearchController(_ searchController: UISearchController) {
        var frame = searchController.view.frame
        frame.origin.y = 0
        searchController.view.frame = frame
    }

    //    func didDismissSearchController(_ searchController: UISearchController) {
    //
    //    }
    func updateSearchResults(for searchController: UISearchController) {
        UIApplication.shared.setStatusBarHidden(true, with: .none)
        filterContentForSearchText(searchController.searchBar.text!)
    }

    func hideHUD() {
        APESuperHUD.removeHUD(animated: true, presentingView: self.view)
    }
}
