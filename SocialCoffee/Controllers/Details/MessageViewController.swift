//
//  MessageViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/9/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class MessageViewController: AJViewController, UITextViewDelegate {

    var messageTextView = UITextView()
    var messagePlaceHolder = UILabel()
    var place: AJPlaceFourSquare!

    init(place: AJPlaceFourSquare) {
        self.place = place
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        createSubviews()
         self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.tappedDoneButton)), animated: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        messageTextView.becomeFirstResponder()
    }

    func tappedDoneButton() {
        let _ = self.navigationController?.popViewController(animated: true)
    }

    func createSubviews() {
        messageTextView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        messageTextView.textColor = .white
        messageTextView.textAlignment = .left
        messageTextView.layer.masksToBounds = true
        messageTextView.layer.cornerRadius = 5
        messageTextView.layer.borderColor = UIColor.black.cgColor
        messageTextView.layer.borderWidth = 1
        messageTextView.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(18)
        view.addSubview(messageTextView)
        messageTextView.delegate = self

        messagePlaceHolder.textColor = UIColor.lightGray
        messagePlaceHolder.text = "Add a comment"
        messagePlaceHolder.font = messageTextView.font
        messageTextView.addSubview(messagePlaceHolder)

        let bgImageView = UIImageView()
        bgImageView.frame = view.frame
        bgImageView.contentMode = .scaleAspectFill
        view.addSubview(bgImageView)
        view.sendSubview(toBack: bgImageView)
        let bgVenuePhotoUrl = place.venuePhotoUrl.replacingOccurrences(of: place.venuePhotoSize, with: "350x800")
        bgImageView.clipsToBounds = true
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bgImageView.addSubview(blurEffectView)

        bgImageView.af_setImage(withURL: URL(string: bgVenuePhotoUrl)!, placeholderImage: UIImage(named: "tab_home") , filter: nil, imageTransition: .crossDissolve(0.2),runImageTransitionIfCached: false)

        view.setNeedsUpdateConstraints()
        
    }

    override func updateViewConstraints() {
        messageTextView.translatesAutoresizingMaskIntoConstraints = false
        messagePlaceHolder.translatesAutoresizingMaskIntoConstraints = false

        constrain(messageTextView, messagePlaceHolder, view) { messageTextView, messagePlaceHolder, view in
            messageTextView.leading == view.leading + 15
            messageTextView.trailing == view.trailing - 15
            messageTextView.top == view.top + 15
            messageTextView.height == view.height / 2
            messagePlaceHolder.leading == messageTextView.leading + 5
            messagePlaceHolder.top == messageTextView.top + 10

        }
        super.updateViewConstraints()
        view.layoutIfNeeded()
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let string = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if text == "\n" || string.characters.count == 0 && textView.text.characters.count == 0 {
            return false
        }

        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {

    }

    func textViewDidChange(_ textView: UITextView) {
        if textView.text.characters.count == 0 {
            messagePlaceHolder.isHidden = false
        }

        let string = textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if string.characters.count > 0  {
            messagePlaceHolder.isHidden = true
            toggleSendButton(true)
        }else {
            toggleSendButton(false)
        }
    }

    func toggleSendButton(_ toggle: Bool) {
//        sendButton.isEnabled = toggle
//        sendButton.isHighlighted = toggle
    }

    


}
