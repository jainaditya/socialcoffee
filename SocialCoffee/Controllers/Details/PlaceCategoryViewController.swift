//
//  PlaceCategoryViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/30/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit

class PlaceCategoryViewController: AJViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var placeCategorySelectionView: PlaceCategorySelectionView!
    var placeCategoryContentProvider: AJPlaceCategoryContentProvider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeCategorySelectionView = PlaceCategorySelectionView(frame: view.frame)
        view.addSubview(placeCategorySelectionView)
        placeCategorySelectionView.placeCategoryCollectionView.delegate = self
        placeCategorySelectionView.placeCategoryCollectionView.dataSource = self
        placeCategoryContentProvider = AJPlaceCategoryContentProvider()

        placeCategorySelectionView.continueButton.addTarget(self, action: #selector(clickedSaveCategory(_:)), for: .touchUpInside)
    }

    func clickedSaveCategory(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(AJPlaceCategoryCollectionViewCell.self), for: indexPath) as! AJPlaceCategoryCollectionViewCell
        let category = placeCategoryContentProvider.titleForIndexPath(indexPath)
        let selected = placeCategoryContentProvider.isSelectedCategory(category)
        cell.configureCell(category, icon: category, background: "", tag: indexPath.row, selected: selected)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! AJPlaceCategoryCollectionViewCell
        if let category = placeCategoryContentProvider.saveSelectedCategory(indexPath) {
            if placeCategoryContentProvider.isSelectedCategory(category) {
                cell.selectImageView.image = UIImage(named: "heart-filled")
            } else {
                cell.selectImageView.image = UIImage(named: "heart")
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenRect:CGRect = UIScreen.main.bounds
        let screenWidth:CGFloat = screenRect.size.width
        let cellWidth:CGFloat = screenWidth / 3.6
        let size: CGSize = CGSize(width: cellWidth, height: cellWidth)

        return size;
    }
}
