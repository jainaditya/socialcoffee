//
//  PlaceLiveReportViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/27/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography
import APESuperHUD

class PlaceLiveFeedViewController: AJViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var placeLiveReportContentProvider: PlaceLiveReportContentProvider!
    var place: AJPlaceFourSquare!
    var liveFeedType: String!
    var reportTitle: String!
    var headerView: PlaceLiveFeedHeaderView!
    var collectionView: PlaceCategoryCollectionView!
    var lastSelectedCell: AJPlaceCategoryCollectionViewCell!
    var feedImageView: UIImageView!
    var messageLabel: UILabel!
    var messageIcon: UIImageView!
    var imagePickerController = UIImagePickerController()
    var selectImageButton: UIButton!
    let defaultImageIdentifier = "sippersCameraImage"
    var sendButton: UIButton!
    var messageViewController: MessageViewController!
    let messageLabelPlaceholderText = "Add a comment"
    var feedRequestSynchronizer: AJFeedRequestSynchronizer!


    init(liveFeedType: String, title: String, place: AJPlaceFourSquare, provider: Int) {
        super.init(nibName: nil, bundle: nil)
        self.liveFeedType = liveFeedType
        self.reportTitle = title
        self.place = place
        placeLiveReportContentProvider = PlaceLiveReportContentProvider(providerType: provider)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        createSubviews()

//        self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(self.tappedReturnHome)), animated: false)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        collectionView.delegate = self
        collectionView.dataSource = self
        self.title = place.name
        imagePickerController.delegate = self
        selectImageButton.addTarget(self, action: #selector(self.tappedSelectImageButton(_:)), for: .touchUpInside)
        let messageLabelSingleTap = UITapGestureRecognizer(target: self, action: #selector(self.presentMessageComposeView(_:)))
        messageLabelSingleTap.numberOfTapsRequired = 1
        messageLabel.isUserInteractionEnabled = true
        messageLabel.addGestureRecognizer(messageLabelSingleTap)

        sendButton.addTarget(self, action: #selector(self.tappedSendButton(_:)), for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if messageViewController != nil {
            if messageViewController.messageTextView.text.characters.count > 0 {
                messageLabel.text = messageViewController.messageTextView.text
            } else {
                messageLabel.text = messageLabelPlaceholderText
            }
        }
    }

    func tappedReturnHome() {
        super.returnToPreviousView()
    }

    func presentMessageComposeView(_ sender: UITapGestureRecognizer) {
        if self.messageViewController == nil {
            messageViewController = MessageViewController(place: self.place)
        }
        DispatchQueue.main.async(execute: {
            self.navigationController?.pushViewController(self.messageViewController, animated: true)
        })
    }

    func createSubviews() {
        headerView = PlaceLiveFeedHeaderView(frame: .zero)
        headerView.populatePlaceInfo(name: place.name, title: reportTitle, bgUrl: getVenuePhotoUrl(place))
        view.addSubview(headerView)

        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionView = PlaceCategoryCollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.addSubview(collectionView)
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 0
        collectionView.backgroundColor = .clear
        collectionView.alwaysBounceVertical = false
        collectionView.bounces = false

        self.view.clipsToBounds = true

        let bgImageView = UIImageView()
        bgImageView.frame = view.frame
        bgImageView.contentMode = .scaleAspectFill
        view.addSubview(bgImageView)
        view.sendSubview(toBack: bgImageView)
        bgImageView.clipsToBounds = true
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bgImageView.addSubview(blurEffectView)

        bgImageView.af_setImage(withURL: URL(string: getVenuePhotoUrl(place))!, placeholderImage: UIImage(named: "tab_home") , filter: nil, imageTransition: .crossDissolve(0.2),runImageTransitionIfCached: false)

        feedImageView = UIImageView()
        setFeedImageViewDefaults()
        view.addSubview(feedImageView)

        messageLabel = UILabel()
        messageLabel.text = messageLabelPlaceholderText
        messageLabel.numberOfLines = 0
        messageLabel.sizeToFit()
        messageLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(16)
        messageLabel.textColor = .gray
        view.addSubview(messageLabel)

        messageIcon = UIImageView()
        messageIcon.image = UIImage(named: "chat")
        view.addSubview(messageIcon)

        selectImageButton = UIButton()
        selectImageButton.isUserInteractionEnabled = true
        feedImageView.addSubview(selectImageButton)

        sendButton = UIButton(type: .custom)
        sendButton.setTitle("Send", for: .normal)
        sendButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(18)
        sendButton.setTitleColor(.white, for: .normal)
        sendButton.layer.cornerRadius = 2.0
        sendButton.layer.borderColor = UIColor.secondaryColor().cgColor
//        sendButton.layer.borderWidth = 1
        sendButton.isEnabled = false
        toggleSendButtonState(false)
        view.addSubview(sendButton)
        view.setNeedsUpdateConstraints()
    }

    func getVenuePhotoUrl(_ place: AJPlaceFourSquare) -> String {
        var photoUrl = place.iconUrl
        if place.venuePhotoUrl.characters.count > 0 {
            photoUrl = place.venuePhotoUrl.replacingOccurrences(of: place.venuePhotoSize, with: "350x800")
        }
        return photoUrl
    }

    func toggleSendButtonState(_ enabled: Bool) {
        if enabled  == true {
            sendButton.backgroundColor = UIColor.secondaryColor()
        } else {
            sendButton.backgroundColor = UIColor.secondaryColor().withAlphaComponent(0.3)
        }
        sendButton.isEnabled = enabled
    }

    override func updateViewConstraints() {
        headerView.translatesAutoresizingMaskIntoConstraints = false
        feedImageView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageIcon.translatesAutoresizingMaskIntoConstraints = false
        selectImageButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.translatesAutoresizingMaskIntoConstraints = false

        let screenSize = UIScreen.main.bounds.size
        constrain(headerView) { headerView in
            headerView.leading == headerView.superview!.leading
            headerView.width == headerView.superview!.width
            headerView.height == 200
            headerView.top == headerView.superview!.top + 10
        }
        constrain(headerView, collectionView) { headerView, collectionView in
            collectionView.leading == headerView.leading - 0.5
            collectionView.width == headerView.width
            collectionView.top == headerView.bottom + 10
            collectionView.height == screenSize.width / 3
        }

        constrain(collectionView, feedImageView, messageIcon, messageLabel) { collectionView, feedImageView, messageIcon, messageLabel in
            feedImageView.top == collectionView.bottom + 20
            feedImageView.width == collectionView.width / 3 - 10
            feedImageView.height == collectionView.width / 3
            feedImageView.leading == collectionView.leading + 10

            messageIcon.centerY == feedImageView.centerY
            messageIcon.width == 32
            messageIcon.height == 32
            messageIcon.leading == feedImageView.trailing + 20
            messageLabel.centerY == feedImageView.centerY
            messageLabel.leading == messageIcon.trailing + 20
            messageLabel.width == collectionView.width / 2 - 20
            messageLabel.height == collectionView.width / 3
        }

        constrain(selectImageButton) { selectImageButton in
            selectImageButton.edges == selectImageButton.superview!.edges
        }

        constrain(sendButton) { sendButton in
            sendButton.leading == sendButton.superview!.leading + 20
            sendButton.trailing == sendButton.superview!.trailing - 20
            sendButton.bottom == sendButton.superview!.bottom - 10
            sendButton.height == 50
        }

        super.updateViewConstraints()
        view.layoutIfNeeded()
        feedImageView.addRightBorderWithWidth(1, color: .darkGray, rightOffset: 0, topOffset: feedImageView.frame.size.height / 3 - 15, bottomOffset: feedImageView.frame.size.height / 3 - 15 )

    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(AJPlaceCategoryCollectionViewCell.self), for: indexPath) as! AJPlaceCategoryCollectionViewCell
        let category = placeLiveReportContentProvider.titleForIndexPath(indexPath)

        cell.selectImageView.isHidden = true
        cell.backgroundColor = UIColor.clear
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 0.5
        cell.configureCell(category, icon: liveFeedType + "-" + String(indexPath.row), background: "", tag: indexPath.row, selected: false)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if lastSelectedCell != nil {
            lastSelectedCell.cellFadeView.backgroundColor = lastSelectedCell.cellFadeViewBGColor
        }
        toggleSendButtonState(true)
        let cell = collectionView.cellForItem(at: indexPath) as! AJPlaceCategoryCollectionViewCell
        cell.cellFadeView.backgroundColor = UIColor.defaultBarColor()
        lastSelectedCell = cell;
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenRect:CGRect = UIScreen.main.bounds
        let screenWidth:CGFloat = screenRect.size.width
        let cellWidth:CGFloat = screenWidth / 3
        let size: CGSize = CGSize(width: cellWidth, height: cellWidth)

        return size;
    }

}


extension PlaceLiveFeedViewController: UIAlertViewDelegate, FeedRequestSynchronizerDelegate {
    func tappedSelectImageButton(_ sender: UIButton) {
        let alert: UIAlertController = UIAlertController(title: "Select an Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)

        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) {
            UIAlertAction in self.openCamera()
        }

        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in self.openPhotoLibrary()
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }

        if feedImageView.accessibilityIdentifier != defaultImageIdentifier {
            let removeImageAction = UIAlertAction(title: "Remove", style: .destructive) {
                UIAlertAction in self.setFeedImageViewDefaults()
            }
            alert.addAction(removeImageAction)
        }

        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)

        self.present(alert, animated: true, completion: nil)
    }

    func setFeedImageViewDefaults() {
        feedImageView.image = UIImage(named: "feed-camera")
        feedImageView.contentMode = .center
        feedImageView.clipsToBounds = true
        feedImageView.isUserInteractionEnabled = true
        feedImageView.accessibilityIdentifier = defaultImageIdentifier
    }

    func tappedSendButton(_ sender: UIButton) {
        showProgressHUD()
        feedRequestSynchronizer = AJFeedRequestSynchronizer(feedImage: getFeedImage(), feedMessage: getFeedMessage(), place: self.place, params: getParams())
        self.feedRequestSynchronizer!.feedSaveDelegate = self
        self.feedRequestSynchronizer!.performFeedSaveOperation()

    }

    func showProgressHUD() {
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Sending...", presentingView: (self.navigationController?.view)!)
    }

    func hideProgressHUD() {
        APESuperHUD.removeHUD(animated: true, presentingView: (self.navigationController?.view)!)
    }

    func getFeedImage() -> UIImage? {
        if feedImageView.accessibilityIdentifier != defaultImageIdentifier {
            return feedImageView.image
        }
        return nil
    }

    func getFeedMessage() -> String {
        var message = String()
        if messageLabel.text != messageLabelPlaceholderText {
            message = messageLabel.text!
        }
        return message
    }

    func feedSaveOperationDidEnd() {
        APESuperHUD.showOrUpdateHUD(icon: .checkMark, message: "Done", duration: 0.01, particleEffectFileName: nil, presentingView: self.navigationController!.view, completion: { () -> () in
            kSubmittedLiveFeed = true
            self.navigationController!.popToRootViewController(animated: true)
        })
        //let _ = self.navigationController?.popToRootViewController(animated: true)
//        if let _ = rootViewController?.first {
//            if (rootViewController?.first?.isKind(of: PlaceLiveFeedViewController.self))! {
//                let _rootViewController = rootViewController?.first as? PlaceLiveFeedViewController
//
//            }
//        }
    }

    func feedSaveOperationFailed(_ error: AnyObject) {
        let _error = error as! Error
        //APESuperHUD.showOrUpdateHUD(icon: .sadFace, message: _error.localizedDescription, duration: 4.0, presentingView: self.view)
        APESuperHUD.showOrUpdateHUD(icon: .info, message: _error.localizedDescription, duration: 4.0, particleEffectFileName: nil, presentingView: self.navigationController!.view, completion:  { () -> Void in self.navigationController!.popViewController(animated: true) } )
    }

    func feedSaveOperationWithImage(_ progress: Float) {
        print("%f", progress)
    }

    func getParams() -> [String: AnyObject] {
        var params = [String: AnyObject]()
        let indexPath = collectionView.indexPath(for: lastSelectedCell)
        let _liveFeedType = liveFeedType as String
        switch _liveFeedType {
        case Constants.LiveFeedType.Crowd:
            params[Constants.LiveFeedType.CrowdParam] = String((indexPath?.row)! + 1) as AnyObject?
            break
        case Constants.LiveFeedType.WaitTime:
            params[Constants.LiveFeedType.WaitTimeParam] = String(((indexPath?.row)! + 1) * 10000 * 60) as AnyObject?
            break
        default:
            params[Constants.LiveFeedType.Crowd] = String((indexPath?.row)! + 1) as AnyObject?
        }
        
        return params
    }
}
