//
//  ExplorePagerTabViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/23/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import XLPagerTabStrip

class ExplorePagerTabViewController: ButtonBarPagerTabStripViewController {
    var isReload = false

    override func viewDidLoad() {


        buttonBarView.selectedBar.backgroundColor = UIColor.defaultBarColor()
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = UIColor.defaultBarColor()
        settings.style.buttonBarItemFont = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(14)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .gray
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        buttonBarView.addBottomBorderWithHeight(1, color: UIColor.defaultBarColor())

        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.darkText.withAlphaComponent(0.6)
            newCell?.label.textColor = UIColor.defaultBarColor()
        }

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.presentSearchView(_:)))

        super.viewDidLoad()
    }

    func presentSearchView(_ sender: UIBarButtonItem) {
        let placePickerController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "AJPlacePickerController") as! AJPlacePickerController
        placePickerController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(placePickerController, animated: true)
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let restaurantViewController = ExplorePlacesTableViewController(style: .plain, itemInfo: getTabInfo("Restaurants"), placeType: Constants.PlaceType.Food)
        let drinksViewController = ExplorePlacesTableViewController(style: .plain, itemInfo: getTabInfo("Bars"), placeType: Constants.PlaceType.Drinks)
        let coffeeViewController = ExplorePlacesTableViewController(style: .plain, itemInfo: getTabInfo("Cafes"), placeType: Constants.PlaceType.Coffee)
//        let trendingViewController = ExplorePlacesTableViewController(style: .plain, itemInfo: getTabInfo("Trending"), placeType: Constants.PlaceType.Trending)
        return [restaurantViewController, drinksViewController, coffeeViewController]
    }

    func getTabInfo(_ name: String) -> IndicatorInfo {
        return IndicatorInfo(title: String(name).uppercased())
    }
}
