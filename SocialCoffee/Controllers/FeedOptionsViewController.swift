//
//  FeedOptionsViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/2/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Cartography

class FeedOptionsViewController: AJViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var place: AJPlaceFourSquare!
    var liveFeedType: String!
    var reportTitle: String!
    var collectionView: PlaceCategoryCollectionView!
    var lastSelectedCell: UICollectionViewCell?
    var bgImageView: UIImageView!
    let defaultImageIdentifier = "sippersCameraImage"
    let feedOptionsContentProvider = FeedOptionsContentProvider()
    var headerView: PlaceLiveFeedHeaderView!

    init(title: String, place: AJPlaceFourSquare) {
        super.init(nibName: nil, bundle: nil)
        self.reportTitle = title
        self.place = place
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        createSubviews()
//        self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(self.tappedReturnHome)), animated: false)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "share") , style: .plain, target: self, action: #selector(self.presentShareView(sender:)))
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title =  place.name
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
    }

    func tappedReturnHome() {
        super.returnToPreviousView()
    }

    func presentShareView(sender: UIBarButtonItem) {
        let textToShare = place.name

        if let website = NSURL(string: "http://sippersapp.com/") {
            let objectsToShare = [textToShare, place.vicinity, headerView.venueImageView.image, website] as [Any]

            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            self.present(activityViewController, animated: true, completion: nil)
        }
    }

    func createSubviews() {
        headerView = PlaceLiveFeedHeaderView(frame: .zero)
        headerView.populatePlaceInfo(name: place.name, title: reportTitle, bgUrl: getVenuePhotoUrl(place))
        view.addSubview(headerView)

        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionView = PlaceCategoryCollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.addSubview(collectionView)
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 0
        collectionView.backgroundColor = .clear
        collectionView.alwaysBounceVertical = false
        collectionView.bounces = false

        let bgImageView = UIImageView()
        bgImageView.frame = view.frame
        bgImageView.contentMode = .scaleAspectFill
        view.addSubview(bgImageView)
        view.sendSubview(toBack: bgImageView)
        bgImageView.clipsToBounds = true
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bgImageView.addSubview(blurEffectView)

        bgImageView.af_setImage(withURL: URL(string: getVenuePhotoUrl(place))!, placeholderImage: UIImage(named: "tab_home") , filter: nil, imageTransition: .crossDissolve(0.2),runImageTransitionIfCached: false)

        view.setNeedsUpdateConstraints()
    }

    func getVenuePhotoUrl(_ place: AJPlaceFourSquare) -> String {
        var photoUrl = place.iconUrl
        if place.venuePhotoUrl.characters.count > 0 {
            photoUrl = place.venuePhotoUrl.replacingOccurrences(of: place.venuePhotoSize, with: "350x800")
        }
        return photoUrl
    }

    override func updateViewConstraints() {
        headerView.translatesAutoresizingMaskIntoConstraints = false
        constrain(headerView) { headerView in
            headerView.leading == headerView.superview!.leading
            headerView.width == headerView.superview!.width
            headerView.height == 200
            headerView.top == headerView.superview!.top + 10
        }
        constrain(headerView, collectionView) { headerView, collectionView in
            collectionView.leading == headerView.leading - 0.5
            collectionView.width == headerView.width
            collectionView.top == headerView.bottom + 10
            collectionView.height == collectionView.superview!.height - 200
        }

        super.updateViewConstraints()
        view.layoutIfNeeded()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(AJPlaceCategoryCollectionViewCell.self), for: indexPath) as! AJPlaceCategoryCollectionViewCell
        let category = feedOptionsContentProvider.titleForIndexPath(indexPath)

        cell.selectImageView.isHidden = true
        cell.backgroundColor = UIColor.clear
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 1
        cell.configureCell(category, icon: category + "-" + String(indexPath.row), background: "", tag: indexPath.row, selected: false)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _lastSelectedCell = self.lastSelectedCell {
            _lastSelectedCell.backgroundColor = .clear
        }
        let cell = collectionView.cellForItem(at: indexPath)
        self.lastSelectedCell = cell
        cell?.backgroundColor = UIColor.secondaryColor()
        switch indexPath.row {
        case 0:
            presentLiveFeedView(liveFeedType: Constants.LiveFeedType.Crowd, title: "Crowded?", provider: ReportType.crowd.rawValue)
            break
        case 1:
            presentLiveFeedView(liveFeedType: Constants.LiveFeedType.WaitTime, title: "Wait Time?", provider: ReportType.wait.rawValue)
            break
        default:
            presentLiveFeedView(liveFeedType: "crowd", title: "Crowded?", provider: ReportType.crowd.rawValue)
        }
    }

    func presentLiveFeedView(liveFeedType: String, title: String, provider: Int) {
        let liveFeedTypeController = PlaceLiveFeedViewController(liveFeedType: liveFeedType, title: title, place: place, provider: provider)
        self.navigationController?.pushViewController(liveFeedTypeController, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenRect:CGRect = UIScreen.main.bounds
        let screenWidth:CGFloat = screenRect.size.width
        let cellWidth:CGFloat = screenWidth
        let size: CGSize = CGSize(width: cellWidth, height: cellWidth / 3)
        
        return size;
    }
}
