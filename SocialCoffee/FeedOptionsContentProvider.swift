//
//  FeedOptionsContentProvider.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/2/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation

class FeedOptionsContentProvider: NSObject {
    override init() {

    }

    func titleForIndexPath(_ indexPath: IndexPath) -> String {
        var title = String()
        switch indexPath.row {
        case 0:
            title = Constants.LiveFeedType.Crowd
        case 1:
            title = Constants.LiveFeedType.WaitTime
        case 2:
            title = "Service"
        default:
            title = "Crowd"
        }
        return title
    }
}
