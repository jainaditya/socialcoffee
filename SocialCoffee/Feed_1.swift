//
//  Feed.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/5/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import CoreData
import Foundation

class Feed_1: NSManagedObject {
   
    @NSManaged var message: String
    @NSManaged var image: Image
}
