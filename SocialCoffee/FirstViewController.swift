//
//  FirstViewController.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/16/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import Alamofire

class FirstViewController: UIViewController {
    
    var locationProvider : AJGeoLocationProvider!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationProvider = AJGeoLocationProvider()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let req = Alamofire.request(RequestUrlProvider.Router.users).validate().responseJSON() {
            response in
            if response.result.isSuccess {
//                print(response.result.value)
            } else {
                print(response.debugDescription)
            }
        }
        debugPrint(req)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    
//    func getCoffeePlacesNearYou() {
//        let parameters = [ "location": "\(self.locationProvider.currentLocation.lattitude!),\(CurrentLocation.lattitude, CurrentLocation.longitude)", "rankby": "distance", "name": "starbucks", "type": "cafe", "key": kGoogleApiKey ]
//        println(parameters);
//        
//        Alamofire.request(.GET, kGooglePlacesApiBaseUrl , parameters: parameters).validate().responseJSON { (req, res, json, error) in
//            if(error != nil) {
//                NSLog("Error: \(error)")
//                println(req)
//                println(res)
//            }
//            else {
//                NSLog("Success: \(json)")
//            }
//        }
//    }


}

