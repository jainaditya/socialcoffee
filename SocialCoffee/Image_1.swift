//
//  Image.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/5/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import CoreData

enum ImageDownloadStatus: Int16 {
    case empty = 0
    case downloaded = 1
    case error = 2
}

class Image_1: NSManagedObject {
    @NSManaged var name: String
    @NSManaged var path: String
    @NSManaged var data: Data?
    @NSManaged var height: NSNumber
    @NSManaged var width: NSNumber
    @NSManaged var mimetype: String
    @NSManaged var status: Int16
    @NSManaged var feed: Feed
}
