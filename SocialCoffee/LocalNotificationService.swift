//
//  LocalNotificationService.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/7/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class LocalNotificationService: NSObject {
    let localNotificationKey = "LocalNotification"
    let userDefaults = UserDefaults.standard

    override init() {
        super.init()
        registerForLocalNotification()
    }

    func registerForLocalNotification() {
        if isRegisteredForLocalNotification() {
            return
        } else {
            setUserDefaultLocalNotification()
            registerNotification()
        }
    }

    func registerNotification() {
        let calendar: Calendar = Calendar.current
        let date: Date = Date()
        let unitFlags: NSCalendar.Unit = [.day, .minute, .hour, .month,.second, .year]
        var dateComponents: DateComponents = (calendar as NSCalendar).components(unitFlags, from: date)
        dateComponents.hour = 17
        dateComponents.minute = 0
        dateComponents.second = 0

        let localNotification: UILocalNotification = UILocalNotification()
        localNotification.alertBody = "Don't miss the fun around you. Share, explore, and party. It's all anonymous."
        localNotification.fireDate = calendar.date(from: dateComponents)
        localNotification.repeatInterval = .day
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }

    func setUserDefaultLocalNotification() {
        userDefaults.set(true, forKey: localNotificationKey)
        userDefaults.synchronize()
    }

    func nativeNotificationPermission() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in

                // Enable or disable features based on authorization.
                if granted == true {
                    print("Allow")
                    UIApplication.shared.registerForRemoteNotifications()
                } else {
                    print("Don't Allow")
                }
            }
        } else {
            // Fallback on earlier versions
        }

    }

    func isRegisteredForLocalNotification() -> Bool {
        return userDefaults.bool(forKey: localNotificationKey) == true
    }
}
