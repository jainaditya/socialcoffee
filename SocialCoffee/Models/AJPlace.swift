//
//  AJPlace.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 12/14/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

class AJPlace: NSObject {
    var id: String = ""
    var placeId: String = ""
    var name: String = ""
    var location: (latitude: Double, longitude: Double) = (0,0)
    var iconUrl: String = ""
    var vicinity: String = ""

    override init() {
        super.init()
    }

    init?(json: JSON) {
        super.init()
        if self.parsePlaceDictionaryToPlace(json) == nil {
            return nil
        }
    }

    func parsePlaceDictionaryToPlace(_ json: JSON) -> Any? {
        guard let name = json["name"].string,
            let latitude = json["geometry"]["location"]["lat"].double,
            let longitude = json["geometry"]["location"]["lng"].double,
            let vicinity = json["vicinity"].string,
            let id = json["id"].string,
            let placeId = json["place_id"].string,
            let iconUrl = json["icon"].string
            else {
                return nil
        }

        self.name = name
        self.location = (latitude, longitude)
        self.vicinity = vicinity
        self.iconUrl =  iconUrl.replacingOccurrences(of: "\\", with: "")
        self.id = id
        self.placeId = placeId

        return self
    }
}
