//
//  AJPlaceFourSquare.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/14/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

class AJPlaceFourSquare:AJPlace {
    var category: String = ""
    var venuePhotoUrl: String = ""
    var priceTier: Int = 0
    var rating: Float = 0
    var ratingSignals: Int = 0
    var checkinsCount: Int = 0
    var menuMobileUrl: String = ""
    let venuePhotoSize = "900x350"
    var distanceInMile: Float = 0

    override init?(json: JSON) {
        super.init()
        if self.parsePlaceDictionaryToPlace(json) == nil {
            return nil
        }
    }

    override func parsePlaceDictionaryToPlace(_ json: JSON) -> Any? {
        guard let name = json["name"].string,
            let latitude = json["location"]["lat"].double,
            let longitude = json["location"]["lng"].double,
            let vicinity = json["location"]["address"].string,
            let id = json["id"].string,
            let placeId = json["id"].string,
            let iconUrl = getPlaceIconUrlFromCategory(json["categories"]),
            let category = getPlaceCategory(json["categories"]),
            let venuePhotoUrl = getVenuePhotoUrl(json["photos"]),
            let priceTier = getPriceTier(json["price"]),
            let rating = getRating(json),
            let ratingSignals = getRatingSignals(json),
            let distance = getDistanceInMiles(json["location"]),
            let checkinsCount = getCheckinsCount(json["stats"]),
            let menuUrl = getMenuMopbileUrl(json["menu"])
            else {
                return nil
        }

        self.name = name
        self.location = (latitude, longitude)
        self.vicinity = vicinity
        self.iconUrl =  iconUrl
        self.id = id
        self.placeId = placeId
        self.category = category
        self.venuePhotoUrl = venuePhotoUrl
        self.priceTier = priceTier
        self.rating = rating
        self.ratingSignals = ratingSignals
        self.distanceInMile = distance
        self.checkinsCount = checkinsCount
        self.menuMobileUrl = menuUrl
        return self
    }

    func getPlaceCategory(_ categories: JSON) -> String? {
        for (_, category): (String, JSON) in categories {
            if (category["primary"] == true) {
                return category["shortName"].string
            }
        }
        return nil
    }

    func getPlaceIconUrlFromCategory(_ categories: JSON) -> String? {
        for (_, category): (String, JSON) in categories {
            if (category["primary"] == true) {
                var iconUrl : String
                iconUrl = category["icon"]["prefix"].string! + "bg_64" + category["icon"]["suffix"].string!
                return iconUrl
            }
        }
        return nil
    }

    func getVenuePhotoUrl(_ photos: JSON) -> String? {
        var venueUrl = String()
        for (_, groups): (String, JSON) in photos {
            if (groups.count > 0) {
                venueUrl = groups[0]["items"][0]["prefix"].string! + venuePhotoSize + groups[0]["items"][0]["suffix"].string!
            }
        }
        return venueUrl
    }

    func getPriceTier(_ price: JSON) -> Int? {
        var tier = 0
        if let _tier = price["tier"].int {
            tier = _tier
        }
        return tier
    }

    func getRating(_ json: JSON) -> Float? {
        if let _rating = json["rating"].float {
            rating = _rating
        }
        return rating
    }

    func getRatingSignals(_ json: JSON) -> Int? {
        if let _rating = json["ratingSignals"].int {
            ratingSignals = _rating
        }
        return ratingSignals
    }

    func getDistanceInMiles(_ location: JSON) -> Float? {
        if let _distance = location["distance"].float {
            distanceInMile = (_distance / 1609.0)
        }
        return distanceInMile
    }

    func getCheckinsCount(_ stats: JSON) -> Int? {
        if let _checkins = stats["checkinsCount"].int {
            checkinsCount = _checkins
        }
        return checkinsCount
    }

    func getMenuMopbileUrl(_ menu: JSON) ->  String? {
        if let _mobileUrl = menu["mobileUrl"].string {
            menuMobileUrl = _mobileUrl
        }
        return menuMobileUrl
    }
}
