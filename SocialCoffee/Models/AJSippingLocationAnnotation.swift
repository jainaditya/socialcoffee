//
//  AJPlaceLocationAnnotation.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/1/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import MapKit

class AJSippingLocationAnnotation: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D

    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate

        super.init()
    }

    func pinColor() -> MKPinAnnotationColor {
        switch discipline {
            case  "bar":
                return .purple
            case "cafe":
                return .green
        default:
            return .red
        }
    }

    func pinImage() -> UIImage {
        switch discipline {
        case  "bar":
            return UIImage(named: "bar_pin")!
        case "cafe":
            return UIImage(named: "coffee_pin")!
        default:
            return UIImage(named: "coffee_pin")!
        }
    }

}
