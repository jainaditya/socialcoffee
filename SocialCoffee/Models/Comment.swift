//
//  Comment.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/21/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

class Comment: NSObject {
    var id: String!
    var message: String!
    var feedId: String!
    var userId: String!
    var username: String!
    var avatar: String!
    var createdAt: Date!

    override init() {
        super.init()
    }

    init?(json: JSON) {
        super.init()
        if self.parseCommentDictionary(json) == nil {
            return nil
        }
    }

    func parseCommentDictionary(_ json: JSON) -> Any? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"

        guard let id = json["id"].string,
            let message = json["message"].string,
            let feedId = json["feedId"].string,
            let userId = json["userId"].string,
//            let username = json["userId"]["username"].string,
            let createdAt = json["createdAt"].date(dateFormatter)
        else {
                return nil
        }

        self.id = id
        self.message = message
        self.userId = userId
//        self.username =  username
        self.feedId = feedId
        self.createdAt = createdAt as Date!

        return self
    }
}
