import MapKit

@objc(Feed)
open class Feed: _Feed {

	// Custom logic goes here.
    func relativeDateString() ->  String {
        let interval = self.createdAt?.timeIntervalSinceNow
        if interval != nil {
            let beforeDate = Date().addingTimeInterval(interval!)
            let unitFlags: NSCalendar.Unit = [NSCalendar.Unit.day, NSCalendar.Unit.minute, NSCalendar.Unit.hour, NSCalendar.Unit.month, NSCalendar.Unit.second]
            var calendar: Calendar = Calendar.current
            calendar.timeZone = TimeZone(identifier: "GMT")!
            (calendar as NSCalendar).components(unitFlags, from: beforeDate, to: Date(), options: [])

            return beforeDate.relativeDateString
        }
        return ""
    }

    func getDistanceInMilesFromCurrentLocation() -> Double {
        return CLLocation(latitude: CurrentLocation.latitude, longitude: CurrentLocation.longitude).distance(from: CLLocation(latitude: (self.latitude?.doubleValue)!, longitude: (self.longitude?.doubleValue)!)) / 1069.344
    }

    func waitTimeInMinutes() -> Int {
        var wait = 0
        if let _wait = self.waitTime {
            wait = _wait.intValue / (1000 * 60)
        }
        return wait
    }
    
}
