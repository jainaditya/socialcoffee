// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Feed.swift instead.

import Foundation
import CoreData

public enum FeedAttributes: String {
    case createdAt = "createdAt"
    case crowd = "crowd"
    case crowdText = "crowdText"
    case latitude = "latitude"
    case longitude = "longitude"
    case message = "message"
    case objectId = "objectId"
    case waitTime = "waitTime"
}

public enum FeedRelationships: String {
    case image = "image"
    case place = "place"
    case user = "user"
    case votes = "votes"
}

open class _Feed: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Feed"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Feed.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var createdAt: Date?

    @NSManaged open
    var crowd: NSNumber?

    @NSManaged open
    var crowdText: String?

    @NSManaged open
    var latitude: NSNumber?

    @NSManaged open
    var longitude: NSNumber?

    @NSManaged open
    var message: String?

    @NSManaged open
    var objectId: String?

    @NSManaged open
    var waitTime: NSNumber?

    // MARK: - Relationships

    @NSManaged open
    var image: Image?

    @NSManaged open
    var place: Place?

    @NSManaged open
    var user: User?

    @NSManaged open
    var votes: NSSet

    open func votesSet() -> NSMutableSet {
        return self.votes.mutableCopy() as! NSMutableSet
    }

}

extension _Feed {

    open func addVotes(_ objects: NSSet) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.votes = mutable.copy() as! NSSet
    }

    open func removeVotes(_ objects: NSSet) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.votes = mutable.copy() as! NSSet
    }

    open func addVotesObject(_ value: Vote) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.votes = mutable.copy() as! NSSet
    }

    open func removeVotesObject(_ value: Vote) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.votes = mutable.copy() as! NSSet
    }

}

