// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Image.swift instead.

import Foundation
import CoreData

public enum ImageAttributes: String {
    case data = "data"
    case height = "height"
    case mimetype = "mimetype"
    case name = "name"
    case objectId = "objectId"
    case path = "path"
    case status = "status"
    case width = "width"
}

public enum ImageRelationships: String {
    case feed = "feed"
}

open class _Image: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Image"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Image.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var data: NSData?

    @NSManaged open
    var height: NSNumber?

    @NSManaged open
    var mimetype: String?

    @NSManaged open
    var name: String?

    @NSManaged open
    var objectId: String?

    @NSManaged open
    var path: String?

    @NSManaged open
    var status: NSNumber?

    @NSManaged open
    var width: NSNumber?

    // MARK: - Relationships

    @NSManaged open
    var feed: Feed?

}

