// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Place.swift instead.

import Foundation
import CoreData

public enum PlaceAttributes: String {
    case category = "category"
    case id = "id"
    case name = "name"
    case placeId = "placeId"
    case vicinity = "vicinity"
}

public enum PlaceRelationships: String {
    case feeds = "feeds"
}

open class _Place: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Place"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Place.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var category: String?

    @NSManaged open
    var id: String?

    @NSManaged open
    var name: String?

    @NSManaged open
    var placeId: String?

    @NSManaged open
    var vicinity: String?

    // MARK: - Relationships

    @NSManaged open
    var feeds: NSSet

    open func feedsSet() -> NSMutableSet {
        return self.feeds.mutableCopy() as! NSMutableSet
    }

}

extension _Place {

    open func addFeeds(_ objects: NSSet) {
        let mutable = self.feeds.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.feeds = mutable.copy() as! NSSet
    }

    open func removeFeeds(_ objects: NSSet) {
        let mutable = self.feeds.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.feeds = mutable.copy() as! NSSet
    }

    open func addFeedsObject(_ value: Feed) {
        let mutable = self.feeds.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.feeds = mutable.copy() as! NSSet
    }

    open func removeFeedsObject(_ value: Feed) {
        let mutable = self.feeds.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.feeds = mutable.copy() as! NSSet
    }

}

