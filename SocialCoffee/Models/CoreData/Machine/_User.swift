// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.swift instead.

import Foundation
import CoreData

public enum UserAttributes: String {
    case name = "name"
    case objectId = "objectId"
    case phone = "phone"
    case token = "token"
    case tokenExpiration = "tokenExpiration"
    case username = "username"
    case uuid = "uuid"
    case verified = "verified"
}

public enum UserRelationships: String {
    case feedItems = "feedItems"
    case votes = "votes"
}

open class _User: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "User"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _User.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var name: String?

    @NSManaged open
    var objectId: String?

    @NSManaged open
    var phone: String?

    @NSManaged open
    var token: String?

    @NSManaged open
    var tokenExpiration: Date?

    @NSManaged open
    var username: String?

    @NSManaged open
    var uuid: String?

    @NSManaged open
    var verified: NSNumber?

    // MARK: - Relationships

    @NSManaged open
    var feedItems: NSSet

    open func feedItemsSet() -> NSMutableSet {
        return self.feedItems.mutableCopy() as! NSMutableSet
    }

    @NSManaged open
    var votes: NSSet

    open func votesSet() -> NSMutableSet {
        return self.votes.mutableCopy() as! NSMutableSet
    }

}

extension _User {

    open func addFeedItems(_ objects: NSSet) {
        let mutable = self.feedItems.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.feedItems = mutable.copy() as! NSSet
    }

    open func removeFeedItems(_ objects: NSSet) {
        let mutable = self.feedItems.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.feedItems = mutable.copy() as! NSSet
    }

    open func addFeedItemsObject(_ value: Feed) {
        let mutable = self.feedItems.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.feedItems = mutable.copy() as! NSSet
    }

    open func removeFeedItemsObject(_ value: Feed) {
        let mutable = self.feedItems.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.feedItems = mutable.copy() as! NSSet
    }

}

extension _User {

    open func addVotes(_ objects: NSSet) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.votes = mutable.copy() as! NSSet
    }

    open func removeVotes(_ objects: NSSet) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.votes = mutable.copy() as! NSSet
    }

    open func addVotesObject(_ value: Vote) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.votes = mutable.copy() as! NSSet
    }

    open func removeVotesObject(_ value: Vote) {
        let mutable = self.votes.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.votes = mutable.copy() as! NSSet
    }

}

