// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Vote.swift instead.

import Foundation
import CoreData

public enum VoteAttributes: String {
    case createdAt = "createdAt"
    case objectId = "objectId"
    case updown = "updown"
}

public enum VoteRelationships: String {
    case feed = "feed"
    case user = "user"
}

open class _Vote: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Vote"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Vote.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var createdAt: Date?

    @NSManaged open
    var objectId: String?

    @NSManaged open
    var updown: NSNumber?

    // MARK: - Relationships

    @NSManaged open
    var feed: Feed?

    @NSManaged open
    var user: User?

}

