//
//  AJFeed.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/14/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

class AJFeed: MTLModel, MTLJSONSerializing, MTLManagedObjectSerializing
{
    var objectId: String?
    var message: String?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var createdAt: Date?
    var image: AJImage?
    var user: AJUser?
    var place: AJPlaceMTL?
    var crowd: NSNumber?
    var crowdText: String?
    var waitTime: NSNumber?

    override init() {
        super.init()
    }
    
    
    override init(dictionary dictionaryValue: [AnyHashable: Any]!, error: ()) throws {
        try! super.init(dictionary: dictionaryValue, error: error)
    }

    
    init(objectId: String,message: String ,latitude: NSNumber, longitude: NSNumber) {
        self.objectId = objectId
        self.message = message
        self.latitude =  latitude
        self.longitude = longitude
        super.init()
    }
    
    class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        return [FeedAttributes.objectId.rawValue: FeedAttributes.objectId.rawValue
            ,FeedAttributes.message.rawValue: FeedAttributes.message.rawValue
            ,FeedAttributes.latitude.rawValue: FeedAttributes.latitude.rawValue
            ,FeedAttributes.longitude.rawValue: FeedAttributes.longitude.rawValue
            ,FeedAttributes.createdAt.rawValue: FeedAttributes.createdAt.rawValue
            ,FeedAttributes.crowd.rawValue: FeedAttributes.crowd.rawValue
            ,FeedAttributes.waitTime.rawValue: FeedAttributes.waitTime.rawValue
            ,FeedAttributes.crowdText.rawValue: FeedAttributes.crowdText.rawValue
        ]
    }
    
    // managed object
    class func managedObjectKeysByPropertyKey() -> [AnyHashable: Any]! {
        return [FeedAttributes.objectId.rawValue : FeedAttributes.objectId.rawValue
            ,FeedAttributes.message.rawValue: FeedAttributes.message.rawValue
            ,FeedAttributes.latitude.rawValue: FeedAttributes.latitude.rawValue
            ,FeedAttributes.longitude.rawValue: FeedAttributes.longitude.rawValue
            ,FeedAttributes.createdAt.rawValue: FeedAttributes.createdAt.rawValue
            ,FeedAttributes.crowd.rawValue: FeedAttributes.crowd.rawValue
            ,FeedAttributes.waitTime.rawValue: FeedAttributes.waitTime.rawValue
            ,FeedAttributes.crowdText.rawValue: FeedAttributes.crowdText.rawValue
        ]
    }
    
    class func managedObjectEntityName() -> String! {
        return Feed.entityName()
    }
    
    class func propertyKeysForManagedObjectUniquing() -> Set<NSObject>! {
        return NSSet(object: FeedAttributes.objectId.rawValue) as! Set<NSObject>
    }
    
    class func relationshipModelClassesByPropertyKey() -> [AnyHashable: Any]! {
        return [FeedRelationships.image.rawValue :  AJImage.self,
                FeedRelationships.user.rawValue : AJUser.self,
                FeedRelationships.votes.rawValue: AJVote.self,
                FeedRelationships.place.rawValue: AJPlaceMTL.self]
    }
}
