//
//  AJImage.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/23/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation

class AJImage: MTLModel, MTLJSONSerializing, MTLManagedObjectSerializing
{
    var objectId: String?
    var name: String?
    var path: String?
    
    
    override init()
    {
        super.init()
    }

    override init(dictionary dictionaryValue: [AnyHashable: Any]!, error: ()) throws {
        try! super.init(dictionary: dictionaryValue, error: error)
    }
    
    
    class func managedObjectEntityName() -> String!
    {
        return Image.entityName()
    }
    
    class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]!
    {
        return [ ImageAttributes.name.rawValue: ImageAttributes.name.rawValue
            , ImageAttributes.path.rawValue: ImageAttributes.path.rawValue ]
    }
    
    class func managedObjectKeysByPropertyKey() -> [AnyHashable: Any]!
    {
        return [ ImageAttributes.name.rawValue: ImageAttributes.name.rawValue
            , ImageAttributes.path.rawValue: ImageAttributes.path.rawValue ]
    }
    
    class func propertyKeysForManagedObjectUniquing() -> Set<NSObject>!
    {
        return NSSet(object: ImageAttributes.objectId.rawValue) as! Set<NSObject>
    }
    
    class func relationshipModelClassesByPropertyKey() -> [AnyHashable: Any]!
    {
        return [ ImageRelationships.feed.rawValue: AJFeed.self]
    }
    
}
