//
//  AJPlaceMTL.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 12/15/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation

class AJPlaceMTL: MTLModel, MTLJSONSerializing, MTLManagedObjectSerializing
{
    var id: String?
    var placeId: String?
    var name: String?
    var vicinity: String?
    var category: String?
//    var feed: Feed?

    override init() {
        super.init()
    }

    override init(dictionary dictionaryValue: [AnyHashable: Any]!, error: ()) throws {
        try! super.init(dictionary: dictionaryValue, error: error)
    }

    init(id: String, placeId: String, name: String, vicinity: String) {
        self.id = id
        self.placeId = placeId
        self.name = name
        self.vicinity = vicinity
        super.init()
    }

    class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        return [PlaceAttributes.id.rawValue: PlaceAttributes.id.rawValue
            ,PlaceAttributes.placeId.rawValue: PlaceAttributes.placeId.rawValue
            ,PlaceAttributes.name.rawValue: PlaceAttributes.name.rawValue
            ,PlaceAttributes.vicinity.rawValue: PlaceAttributes.vicinity.rawValue
            ,PlaceAttributes.category.rawValue: PlaceAttributes.category.rawValue
            ]
    }

    // managed object
    class func managedObjectKeysByPropertyKey() -> [AnyHashable: Any]! {
        return [PlaceAttributes.id.rawValue: PlaceAttributes.id.rawValue
            ,PlaceAttributes.placeId.rawValue: PlaceAttributes.placeId.rawValue
            ,PlaceAttributes.name.rawValue: PlaceAttributes.name.rawValue
            ,PlaceAttributes.vicinity.rawValue: PlaceAttributes.vicinity.rawValue
            ,PlaceAttributes.category.rawValue: PlaceAttributes.category.rawValue
        ]
    }

    class func managedObjectEntityName() -> String! {
        return Place.entityName()
    }

    class func propertyKeysForManagedObjectUniquing() -> Set<NSObject>! {
        return NSSet(object: PlaceAttributes.id.rawValue) as! Set<NSObject>
    }

    class func relationshipModelClassesByPropertyKey() -> [AnyHashable: Any]! {
        return [PlaceRelationships.feeds.rawValue :  AJFeed.self]
    }
}
