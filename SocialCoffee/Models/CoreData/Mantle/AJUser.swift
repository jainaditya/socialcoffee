//
//  AJUser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/18/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation

class AJUser: MTLModel, MTLJSONSerializing, MTLManagedObjectSerializing
{
    var objectId: String?
    var phone: String?
    var name: String?
    var verified: NSNumber?
    var feedItems: NSSet?
    var votes: NSSet?
    var username: NSString?
    var uuid: NSString?
    var token: NSString?
    var tokenExpiration: Date?

    override init()
    {
        super.init()
    }
    
    override init(dictionary dictionaryValue: [AnyHashable: Any]!, error: ()) throws {
        try! super.init(dictionary: dictionaryValue, error: error)
    }
    
    class func managedObjectEntityName() -> String!
    {
        return User.entityName()
    }
    
    class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]!
    {
        return [ UserAttributes.objectId.rawValue: "id"
            ,UserAttributes.phone.rawValue: UserAttributes.phone.rawValue
            ,UserAttributes.name.rawValue: UserAttributes.name.rawValue
            ,UserAttributes.verified.rawValue: UserAttributes.verified.rawValue
            ,UserAttributes.username.rawValue: UserAttributes.username.rawValue
            ,UserAttributes.uuid.rawValue: UserAttributes.uuid.rawValue
            ,UserAttributes.token.rawValue: UserAttributes.token.rawValue
            ,UserAttributes.tokenExpiration.rawValue: UserAttributes.tokenExpiration.rawValue]
    }
    
    class func managedObjectKeysByPropertyKey() -> [AnyHashable: Any]!
    {
        return [ UserAttributes.objectId.rawValue: UserAttributes.objectId.rawValue
            ,UserAttributes.phone.rawValue: UserAttributes.phone.rawValue
            ,UserAttributes.name.rawValue: UserAttributes.name.rawValue
            ,UserAttributes.verified.rawValue: UserAttributes.verified.rawValue
            ,UserAttributes.username.rawValue: UserAttributes.username.rawValue
            ,UserAttributes.uuid.rawValue: UserAttributes.uuid.rawValue
            ,UserAttributes.token.rawValue: UserAttributes.token.rawValue
            ,UserAttributes.tokenExpiration.rawValue: UserAttributes.tokenExpiration.rawValue]
    }
    
    class func propertyKeysForManagedObjectUniquing() -> Set<NSObject>!
    {
        return NSSet(object: UserAttributes.objectId.rawValue) as! Set<NSObject>
    }
    
    class func relationshipModelClassesByPropertyKey() -> [AnyHashable: Any]!
    {
        return [UserRelationships.feedItems.rawValue : AJFeed.self
            ,UserRelationships.votes.rawValue: AJVote.self]
    }
}
