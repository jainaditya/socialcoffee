//
//  AJVote.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/2/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation

class AJVote: MTLModel, MTLJSONSerializing, MTLManagedObjectSerializing
{
    var objectId: String?
    var updown: NSNumber?
    var createdAt: Date?
    var feed: AJFeed?
    var user: AJUser?
    
    override init() {
        super.init()
    }
    
    override init(dictionary dictionaryValue: [AnyHashable: Any]!, error: ()) throws {
        try! super.init(dictionary: dictionaryValue, error: error)
    }
    
    init(objectId: String, updown: NSNumber) {
        self.objectId = objectId
        self.updown = updown
        super.init()
    }
    
    class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]!
    {
        return [VoteAttributes.objectId.rawValue: VoteAttributes.objectId.rawValue
            ,VoteAttributes.updown.rawValue: VoteAttributes.updown.rawValue
            ,VoteAttributes.createdAt.rawValue: VoteAttributes.createdAt.rawValue]
    }

    // managed object
    class func managedObjectKeysByPropertyKey() -> [AnyHashable: Any]!
    {
        return [VoteAttributes.objectId.rawValue: VoteAttributes.objectId.rawValue
            ,VoteAttributes.updown.rawValue: VoteAttributes.updown.rawValue
            ,VoteAttributes.createdAt.rawValue: VoteAttributes.createdAt.rawValue]
    }
    
    class func managedObjectEntityName() -> String! {
        return Vote.entityName()
    }
    
    class func propertyKeysForManagedObjectUniquing() -> Set<NSObject>! {
        return NSSet(object: VoteAttributes.objectId.rawValue) as! Set<NSObject>
    }
    
    class func relationshipModelClassesByPropertyKey() -> [AnyHashable: Any]! {
        return [VoteRelationships.feed.rawValue :  AJFeed.self
            ,VoteRelationships.user.rawValue: AJUser.self]
    }
    
}
