//
//  Feed+Parser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/15/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

let kFeedParserKeyObjectId = "objectId"
let kFeedParserKeyMessage = "message"
let kFeedParserKeyLatitude = "latitude"
let kFeedParserKeyLongitude = "longitude"
let kFeedParserKeyLocation = "location"
let kVotesParserKey = "votes"
var LastFetchedFeedsAt = ""
extension Feed
{

    class func updateDatabaseWithFeedResponse(_ responseObjects: JSON, cachingService: AJCachingService)
    {
        for (_, responseObject): (String, JSON) in responseObjects {
            let _feed = fetchFeedObject(responseObject["id"].string!)

            if  _feed == nil {
                let feed:AJFeed = self.feedRepresentationOfFeedDictionary(responseObject.dictionaryValue)
                do {
                    try MTLManagedObjectAdapter.managedObject(from: feed, insertingInto: cachingService.managedObjectContext)
                } catch let error as NSError {
                    print(error)
                } catch {
                    fatalError()
                }
                let votesArray = responseObject[kVotesParserKey]
                Vote.convertVotesArrayToDictionary(votesArray: votesArray, feed: feed)
            } else {
                let totalVotes = getTotalVotesFromVotes(responseObject.dictionaryValue[kVotesParserKey])
                let totalVotesFromFeed = getTotalVotesFromFeed(_feed)
                if totalVotes != totalVotesFromFeed {
                    let votesArray = responseObject[kVotesParserKey]
                    let __feed = (try! MTLManagedObjectAdapter.model(of: AJFeed.self, from: _feed)) as! AJFeed
                    Vote.convertVotesArrayToDictionary(votesArray: votesArray, feed: __feed)
                }
            }
        }
        cachingService.saveContext()
    }
    
    class func feedRepresentationOfFeedDictionary(_ feedDictionary: [String: JSON]) -> AJFeed {
        var feedObject = [AnyHashable: Any]()
//        let ajFeed: AJFeed = (try !MTLJSONAdapter.modelOfClass(AJFeed.self, fromJSONDictionary: feedObject)) as! AJFeed

        feedObject[FeedAttributes.objectId.rawValue] = feedDictionary["id"]?.string
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"
        feedObject[FeedAttributes.createdAt.rawValue] = feedDictionary[FeedAttributes.createdAt.rawValue]?.date(dateFormatter)
        
        if let messgae = feedDictionary[FeedAttributes.message.rawValue]?.string {
            feedObject[FeedAttributes.message.rawValue] = messgae
        }

        if let geoPoint = feedDictionary[kFeedParserKeyLocation]?.dictionaryObject {
            feedObject[FeedAttributes.latitude.rawValue] = (geoPoint[FeedAttributes.latitude.rawValue]! as AnyObject).doubleValue
            feedObject[FeedAttributes.longitude.rawValue] = (geoPoint[FeedAttributes.longitude.rawValue]! as AnyObject).doubleValue
        }

        if let files = feedDictionary["files"] {
            for (_, file) in (files) {

                let dictionary: AJImage = Image.imageRepresentationOfImageDictionary(imageDictionary: file)
                feedObject[FeedRelationships.image.rawValue] = dictionary
            }

        }
        if let place:[AnyHashable: Any] = feedDictionary["place"]?.dictionaryObject {
            let dictionary: AJPlaceMTL = Place.placeRepresentationOfPlaceDictionary(place)
            feedObject[FeedRelationships.place.rawValue] = dictionary
        }

        if let userData = feedDictionary["userId"]?.dictionaryObject {
            if (userData["id"] as? String == AJSessionService.currentUser()?.objectId) {
                feedObject[FeedRelationships.user.rawValue] = (try! MTLManagedObjectAdapter.model(of: AJUser.self, from: AJSessionService.currentUser()!)) as! AJUser
            } else {
                let userDictionary = User.userRepresentationOfUserDictionary(userData)
                feedObject[FeedRelationships.user.rawValue] = userDictionary
            }
        }
        feedObject[FeedAttributes.crowd.rawValue] = feedDictionary[FeedAttributes.crowd.rawValue]?.int
        feedObject[FeedAttributes.crowdText.rawValue] = feedDictionary[FeedAttributes.crowdText.rawValue]?.string
        feedObject[FeedAttributes.waitTime.rawValue] = feedDictionary[FeedAttributes.waitTime.rawValue]?.intValue

        let feed = (try! MTLJSONAdapter.model(of: AJFeed.self, fromJSONDictionary: feedObject, error: () )) as! AJFeed

        return feed
        
    }
    
    class func fetchFeedObject(_ objectId: String) -> Feed? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Feed.entityName())
        fetchRequest.returnsObjectsAsFaults = false
        let resultPredicate = NSPredicate(format: "objectId = %@", objectId)
        
        fetchRequest.predicate = resultPredicate
        
        do {
            let results =
            try AJCachingService.sharedInstance.managedObjectContext!.fetch(fetchRequest)
            return results.first as? Feed
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return nil
    }

    class func getLastCreatedAt() -> String {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Feed.entityName())
        fetchRequest.returnsObjectsAsFaults = false

        let createAtSortDescriptor = NSSortDescriptor(key: FeedAttributes.createdAt.rawValue, ascending: false)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        do {
            var results =
            try AJCachingService.sharedInstance.managedObjectContext!.fetch(fetchRequest)
            results = (results as NSArray).sortedArray(using: [createAtSortDescriptor])
            if let feed = results.first as? Feed {
                LastFetchedFeedsAt = dateFormatter.string(from: feed.createdAt! as Date)
                return dateFormatter.string(from: feed.createdAt! as Date)
            }

        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

        LastFetchedFeedsAt = dateFormatter.string(from: Date(timeIntervalSince1970: 0))
        return dateFormatter.string(from: Date(timeIntervalSince1970: 0))
    }

    private class func getTotalVotesFromFeed(_ feed: Feed?) -> Int {
        if let _feed = feed {
            return _feed.votes.value(forKeyPath: "@sum.updown") as! Int
        }
        return 0
    }

    private class func getTotalVotesFromVotes(_ votesArray: JSON?) -> Int {
        var total = 0
        for (_, voteDictionary): (String, JSON) in votesArray! {
            total += voteDictionary["vote"].int!
        }

        return total
    }

    class func willDeleteLocalFeedObject(_ objectId: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: AJFeed.managedObjectEntityName(), in: AJCachingService.sharedInstance.managedObjectContext! )
//        fetchRequest.includesPropertyValues = false
        if  objectId.characters.count > 5 {
            let resultPredicate = NSPredicate(format: "objectId = %@", objectId)
            fetchRequest.predicate = resultPredicate
        }
        do {
            let results = try AJCachingService.sharedInstance.managedObjectContext?.fetch(fetchRequest) as? [NSManagedObject]

            for result in results! {
                AJCachingService.sharedInstance.managedObjectContext?.delete(result)
            }

            try AJCachingService.sharedInstance.managedObjectContext?.save()

        } catch {
            print("error: \(error)")
        }
    }

}
