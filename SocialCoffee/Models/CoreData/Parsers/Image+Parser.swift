//
//  Image+Parser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/23/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Image
{
    class func imageRepresentationOfImageDictionary(imageDictionary: JSON) -> AJImage
    {
        let imageObject: NSMutableDictionary = NSMutableDictionary()
        imageObject.setValue(imageDictionary[ImageAttributes.name.rawValue].stringValue, forKey: ImageAttributes.name.rawValue)
        imageObject.setValue(imageDictionary[ImageAttributes.path.rawValue].stringValue, forKey: ImageAttributes.path.rawValue)
        imageObject.setValue(imageDictionary[ImageAttributes.name.rawValue].stringValue, forKey: ImageAttributes.objectId.rawValue)

        let image = (try! MTLJSONAdapter.model(of: AJImage.self, fromJSONDictionary: imageObject as [NSObject : AnyObject], error: ())) as! AJImage
        
        return image
    }
}
