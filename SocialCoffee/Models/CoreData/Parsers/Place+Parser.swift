//
//  Place+Parser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 12/15/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Place
{
    class func placeRepresentationOfPlaceDictionary(_ placeDictionary: [AnyHashable: Any]) -> AJPlaceMTL {
        let place = (try! MTLJSONAdapter.model(of: AJPlaceMTL.self, fromJSONDictionary: placeDictionary as [AnyHashable: Any], error: ())) as! AJPlaceMTL

        return place
    }
}
