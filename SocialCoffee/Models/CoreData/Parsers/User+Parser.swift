//
//  User+Parser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/2/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON


extension User  {

    class func updateDatabaseWithUserResponse(_ responseObject: [AnyHashable: Any], cachingService: AJCachingService) -> User
    {
        var user: User?
        let ajUser:AJUser = self.userRepresentationOfUserDictionary(responseObject)
        do {
            try user = MTLManagedObjectAdapter.managedObject(from: ajUser, insertingInto: cachingService.managedObjectContext) as? User
        } catch let error as NSError {
            print(error)
        } catch {
            fatalError()
        }
        cachingService.saveContext()

        return user!;
    }

    class func userRepresentationOfUserDictionary(_ userDictionary: [AnyHashable: Any]) -> AJUser
    {
        let user = (try! MTLJSONAdapter.model(of: AJUser.self, fromJSONDictionary: userDictionary, error: ())) as! AJUser

        if let currentUser = AJSessionService.currentUser(){
            if currentUser.objectId == user.objectId {
                let _user = (try! MTLManagedObjectAdapter.model(of: AJUser.self, from: currentUser)) as! AJUser
                _user.phone = user.phone
                _user.verified = user.verified
                user.mergeValuesForKeys(from: _user)
            }
        }
        return user
    }
}
