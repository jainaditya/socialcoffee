//
//  Vote+Parser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/2/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Vote
{
    
    class func updateDatabaseWithVoteResponse(responseObjects: JSON, cachingService: AJCachingService) {
        
        for (_, responseObject): (String, JSON) in responseObjects {

            let vote:AJVote = self.voteRepresentationOfVoteDictionary(voteDictionary: responseObject)

            do {
                try MTLManagedObjectAdapter.managedObject(from: vote, insertingInto: cachingService.managedObjectContext)
            } catch let error as NSError {
                print(error)
            } catch {
                fatalError()
            }
            cachingService.saveContext()
        }
    }

    class func voteRepresentationOfVoteDictionary(voteDictionary: JSON) -> AJVote {

        var voteObject = [String: AnyObject]()

        voteObject[VoteAttributes.objectId.rawValue] = voteDictionary["id"].string as AnyObject?
        voteObject[VoteAttributes.updown.rawValue] = voteDictionary["vote"].int as AnyObject?
        if let _ = voteDictionary["feedId"].string {
            voteObject[VoteRelationships.feed.rawValue] = Feed.feedRepresentationOfFeedDictionary(voteDictionary["feedId"].dictionaryValue)
        }

        let vote = (try! MTLJSONAdapter.model(of: AJVote.self, fromJSONDictionary: voteObject, error: () )) as! AJVote

        return vote
    }

    class func convertVotesArrayToDictionary(votesArray: JSON, feed: AJFeed) {
        for (_, voteDictionary): (String, JSON) in votesArray {
            var voteObject = [AnyHashable: Any]()

            voteObject[VoteAttributes.objectId.rawValue] = voteDictionary["id"].string
            voteObject[VoteAttributes.updown.rawValue] = voteDictionary["vote"].int
            voteObject[VoteAttributes.createdAt.rawValue] = getDateFormatter().date(from:voteDictionary[VoteAttributes.createdAt.rawValue].string!)
            voteObject[VoteRelationships.feed.rawValue] = feed
            if (feed.user?.objectId == AJSessionService.currentUser()?.objectId) {
                voteObject[VoteRelationships.user.rawValue] = feed.user
            } else {
                voteObject[VoteRelationships.user.rawValue] = User.userRepresentationOfUserDictionary(["id": voteDictionary["userId"].string!]);
            }

            let vote = (try! MTLJSONAdapter.model(of: AJVote.self, fromJSONDictionary: voteObject, error: () )) as! AJVote
            self.saveVoteObject(vote: vote)
        }
    }

    class func voteRepresentationOfVoteDictionary(_ voteDictionary: [AnyHashable: Any], user: AJUser) -> AJVote {
        var voteObject = voteDictionary

        voteObject[VoteAttributes.createdAt.rawValue] = getDateFormatter().date(from: voteDictionary[VoteAttributes.createdAt.rawValue] as! String)
        voteObject[VoteAttributes.updown.rawValue] = voteDictionary["vote"] as! Int
        if (user.objectId == AJSessionService.currentUser()?.objectId) {
            voteObject[VoteRelationships.user.rawValue] = user
        } else {
            if let user = AJSessionService.fetchUserObjectByUUID(voteDictionary["userId"] as! String) {
                voteObject[VoteRelationships.user.rawValue] = (try! MTLManagedObjectAdapter.model(of: AJUser.self, from: user)) as! AJUser
            } else {
                voteObject[VoteRelationships.user.rawValue] = User.userRepresentationOfUserDictionary(["id": voteDictionary["userId"] as! String]);
            }
        }
        let vote = (try! MTLJSONAdapter.model(of: AJVote.self, fromJSONDictionary: voteObject as [AnyHashable: Any], error: ())) as! AJVote

        return vote
    }

    class func saveVoteObject(vote: AJVote) {
        do {
            try MTLManagedObjectAdapter.managedObject(from: vote, insertingInto: AJCachingService.sharedInstance.managedObjectContext)
        } catch let error as NSError {
            print(error)
        } catch {
            fatalError()
        }
//        AJCachingService.sharedInstance.saveContext()
    }

    class func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"
        return dateFormatter
    }

}
