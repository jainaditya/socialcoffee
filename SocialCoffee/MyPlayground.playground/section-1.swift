// Playground - noun: a place where people can play

import UIKit
import CoreLocation
import Foundation

var str = "Hello, playground"

// string interpolaton can also be used to combine strings
var a : String? = "BAT"
let b = "MAN"
//let param = ["location" : "\(a!)"]
//println("\(a) + \(b) = \(a + b)")

var evens = [Int]()
var odds = [Int]()



let numbers = [ 10000, 10303, 30913, 50000, 100000, 101039, 1000000 ]
let formattedNumbers = numbers.map { NSNumberFormatter.localizedStringFromNumber($0, numberStyle: .DecimalStyle) }
print(formattedNumbers)

let evenNumbers = numbers.filter { $0 % 3 == 0 }

let userDefaults = NSUserDefaults.standardUserDefaults()
let UUID : AnyObject! =  {
    
    if userDefaults.objectForKey("UUID") == nil  {
        userDefaults.setObject(NSUUID().UUIDString, forKey: "UUID")
        userDefaults.synchronize()
    }
    
    return userDefaults.objectForKey("UUID") }()

//print(UUID)


if userDefaults.objectForKey("ApplicationUniqueIdentifier") == nil {
    let UUID = NSUUID().UUIDString
    userDefaults.setObject(UUID, forKey: "ApplicationUniqueIdentifier")
    userDefaults.synchronize()
}

class MyClass : NSObject
{
    override init () {
        super.init()
        print("test")
        
    }
    
    func fetchFiltetedLocations()
    {
//        var testLocations: Array = [CLLocation(latitude: 33.22, longitude: 34.999), CLLocation(latitude: 35.444, longitude: 33.444)]
//        let maxRadius: CLLocationDistance = 8046
//        let userLocation = CLLocation(latitude: 33.222, longitude: 34.999)
//        
//        var predicateBlock = {
//            (testLocation: AnyObject!, bindings: NSDictionary!) -> Bool in
//                return testLocation.distanceFromLocation(userLocation) <= maxRadius
//        }

//        var test = testLocations.filter{
//            if let location = $0 {
//                return location.distanceFromLocation(userLocation) <= maxRadius
//            }else{
//                return false
//            }
//        }
//        
//        print(testLocations)
    }
}
MyClass().fetchFiltetedLocations()

let date  = NSDate()
let beforeDate = NSDate().dateByAddingTimeInterval(-5912902.77787495)
let calendar = NSCalendar.currentCalendar()

//let unitFlags = NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitMinute | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitSecond
//let info = calendar.components(unitFlags, fromDate: beforeDate, toDate: date, options: nil)info.hour

//NSCalendar.currentCalendar().components(.CalendarUnitMonth, fromDate: beforeDate, toDate: NSDate(), options: nil).month

@objc protocol Speaker {
    func Speak()
    optional func TellJoke()
}

class Vicki: Speaker {
    @objc func Speak() {
        print("Hello, I am Vicki!")
    }
    @objc func TellJoke() {
        print("Q: What did Sushi A say to Sushi B?")
    }
}

class Ray: Speaker {
    @objc func Speak() {
        print("Yo, I am Ray!")
    }
    @objc func TellJoke() {
        print("Q: Whats the object-oriented way to become wealthy?")
    }
    func WriteTutorial() {
        print("I'm on it!")
    }
}

//var speaker:Speaker
//speaker = Ray()
//speaker.Speak()
//// speaker.WriteTutorial() // error!
//(speaker as! Ray).WriteTutorial()
//speaker.TellJoke?()
//
//speaker = Vicki()
//speaker.Speak()
//speaker.TellJoke?()

protocol DateSimulatorDelegate {
    func dateSimulatorDidStart(sim:DateSimulator, a:Speaker, b:Speaker)
    func dateSimulatorDidEnd(sim:DateSimulator, a: Speaker, b:Speaker)
}

class DateSimulator {
    
    let a:Speaker
    let b:Speaker
    var delegate:DateSimulatorDelegate?
    
    init(a:Speaker, b:Speaker) {
        self.a = a
        self.b = b
    }
    
    func simulate() {
        delegate?.dateSimulatorDidStart(self, a:a, b: b)
        print("Off to dinner...")
        a.Speak()
        b.Speak()
        print("Walking back home...")
        a.TellJoke?()
        b.TellJoke?()
        delegate?.dateSimulatorDidEnd(self, a:a, b:b)
    }
}

class LoggingDateSimulator:DateSimulatorDelegate {

    func dateSimulatorDidStart(sim:DateSimulator, a:Speaker, b:Speaker) {
        print("Date started!")
    }
    func dateSimulatorDidEnd(sim:DateSimulator, a: Speaker, b: Speaker)  {
        print("Date ended!")
    }
}


let sim = DateSimulator(a:Vicki(), b:Ray())
sim.delegate = LoggingDateSimulator()
sim.simulate()

let message = ["Text" : "text", "IosVersion" : "9.0.4"]
NSJSONSerialization.dataWithJSONObject(message, options: nil, error: NSError.init())


