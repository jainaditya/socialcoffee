//
//  NSDate+RelativeString.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 6/7/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import Foundation
public extension Date {
    func xDays(_ x:Int) -> Date {
        return (Calendar.current as NSCalendar).date(byAdding: .day, value: x, to: self, options: [])!
    }
    func xWeeks(_ x:Int) -> Date {
        return (Calendar.current as NSCalendar).date(byAdding: .weekOfYear, value: x, to: self, options: [])!
    }
    var monthsFromNow: Int{
        return (Calendar.current as NSCalendar).components(.month, from: self, to: Date(), options: []).month!
    }
    var daysFromNow: Int{
        return (Calendar.current as NSCalendar).components(.day, from: self, to: Date(), options: []).day!
    }
    var hoursFromNow: Int{
        return (Calendar.current as NSCalendar).components(.hour, from: self, to: Date(), options: []).hour!
    }
    var minutesFromNow: Int{
        return (Calendar.current as NSCalendar).components(.minute, from: self, to: Date(), options: []).minute!
    }
    var secondsFromNow: Int{
        return (Calendar.current as NSCalendar).components(.second, from: self, to: Date(), options: []).second!
    }
    
    var relativeDateString: String {
        if monthsFromNow > 0 { return monthsFromNow > 1 ? "\(monthsFromNow) months" : "\(monthsFromNow) month"   }
        if daysFromNow > 0 { return daysFromNow > 1 ? "\(daysFromNow) days" : "\(daysFromNow) day"   }
        if hoursFromNow > 0 { return hoursFromNow > 1 ? "\(hoursFromNow) hours" : "\(hoursFromNow) hour"   }
        if minutesFromNow > 0 { return minutesFromNow > 1 ? "\(minutesFromNow) mins" : "\(minutesFromNow) minute"   }
        return "Just now"
    }
}
