//
//  CommentParser.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/21/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON

class CommentParser: NSObject {

    override init() {
        super.init()
    }

    class func convertCommentResponseToComments(_ responseObjects: JSON) -> NSArray {
        var comments = [Comment]()
        if let _ = responseObjects["message"].string {
            if let comment: Comment =  Comment(json: responseObjects) {
                comments.append(comment)
            }
        } else {
            for (_, commentDictionary): (String, JSON) in responseObjects {
                if let comment: Comment =  Comment(json: commentDictionary) {
                    comments.append(comment)
                }
            }
        }
        return comments as NSArray
    }


}
