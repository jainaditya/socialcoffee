//
//  PlaceInfoProvider.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 6/25/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation

class PlaceInfoProvider: NSObject {

    class func getWaitTimeText(waitTime: Int64, waitTimeCount: Int64) -> String {
        let count = waitTimeCount > 0 ? waitTimeCount : 1
        let wait = waitTime / (count * 1000 * 60)
        if wait > 60 {
            return "Wait: about 1 hr"
        } else {
            return "Wait: about \(wait) min"
        }
    }
}
