//
//  PlaceLiveFeedImagePicker.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/2/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit

extension PlaceLiveFeedViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        } else {
            self.openPhotoLibrary()
        }
    }

    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        feedImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        feedImageView.contentMode = .scaleAspectFill
        feedImageView.accessibilityIdentifier = info[UIImagePickerControllerMediaType] as! String?
        imagePickerController.dismiss(animated: true, completion: nil)
        if (picker.sourceType == UIImagePickerControllerSourceType.camera) {
            if let image = self.feedImageView.image {
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            }
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePickerController.dismiss(animated: true, completion: nil)
    }
}
