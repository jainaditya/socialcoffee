//
//  PlaceLiveReportContentProvider.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/28/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
enum ReportType: Int {
    case crowd = 0
    case wait = 1
    case party = 2
    case nightclub = 3
    case bar = 4
    case fun = 5
}
class PlaceLiveReportContentProvider: NSObject {
    var reportType: Int

    init(providerType: Int) {
        self.reportType = providerType
    }

    func titleForIndexPath(_ indexPath: IndexPath) -> String {
        var title = String()
        if reportType == ReportType.crowd.rawValue {
            title = getTitleForCrowdAtIndexPath(indexPath)
        } else if reportType == ReportType.wait.rawValue {
            title = getTitleForWaitTimeAtIndexPath(indexPath)
        } else if reportType == ReportType.crowd.rawValue {
            title = getTitleForCrowdAtIndexPath(indexPath)
        }
        return title
    }

    func getTitleForCrowdAtIndexPath(_ indexPath: IndexPath) -> String {
        var title = String()
        switch indexPath.row {
        case 0:
            title = "Calm"
        case 1:
            title = "Busy"
        case 2:
            title = "Crowded"
        default:
            title = "Calm"
        }
        return title
    }

    func getTitleForWaitTimeAtIndexPath(_ indexPath: IndexPath) -> String {
        var title = String()
        switch indexPath.row {
        case 0:
            title = "10 min"
        case 1:
            title = "20 min"
        case 2:
            title = "30+ min"
        default:
            title = "30+ min"
        }
        return title
    }
}
