//
//  UIApplication+OpenUrl.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/19/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import SwiftyJSON
extension UIApplication {
    class func tryURL(_ urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(NSURL(string: url)! as URL) {
                application.openURL(NSURL(string: url)! as URL)
                return
            }
        }
    }
}

extension JSON {
    public func date(_ dateFormatter: DateFormatter) -> NSDate? {
        if let string = self.string {
            if let date = dateFormatter.date(from: string) {
                return date as NSDate?
            }
        }
        return nil
    }
}
