//
//  UIColor+AJColors.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/16/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit

extension UIColor
{
    class func defaultBarColor() -> UIColor
    {
        return UIColor(red: 58/256, green: 213/256, blue: 175/256, alpha: 1)
    }
    
    class func primaryTextColor() -> UIColor
    {
        return UIColor(red: 42/256, green: 36/256, blue: 38/256, alpha: 1)
    }

    class func secondaryTextColor() -> UIColor
    {
        return UIColor(red: 135/256, green: 145/256, blue: 135/256, alpha: 1)
    }
    
    class func defaultImageBackgroundColor() -> UIColor
    {
        return UIColor(red: 248/256, green: 248/256, blue: 248/256, alpha: 1)
    }
    
    class func primaryColor() -> UIColor
    {
        return UIColor(red: 250/256, green: 90/256, blue: 95/256, alpha: 1)
            //UIColor(red: 233/256, green: 30/256, blue: 99/256, alpha: 1)
    }

    class func secondaryColor() -> UIColor {
        return self.defaultBarColor()
    }

    class func primaryColorWithAlpha(_ opacity: CGFloat) -> UIColor {
        return UIColor(red: 250/256, green: 90/256, blue: 95/256, alpha: opacity)
    }

    class func foodCategoryColor() ->  UIColor {
        return UIColor(red: 103/256, green: 58/256, blue: 183/256, alpha: 1)
    }

    class func drinkCategoryColor() ->  UIColor {
        return UIColor(red: 171/256, green: 71/256, blue: 188/256, alpha: 1)
    }

    class func cafeCategoryColor() ->  UIColor {
        return UIColor(red: 0/256, green: 188/256, blue: 212/256, alpha: 1)
    }

    class func trendingCategoryColor() ->  UIColor {
        return UIColor(red: 63/256, green: 81/256, blue: 181/256, alpha: 1)
    }

    class func calmColor() -> UIColor {
        return hexStringToUIColor(hex: "#FB8C00")
    }

    class func busyColor() -> UIColor {
        return hexStringToUIColor(hex: "#F44336")
    }

    class func crowdedColor() -> UIColor {
        return hexStringToUIColor(hex: "#E91E63")
    }

    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }

        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
