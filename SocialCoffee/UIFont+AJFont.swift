//
//  UIFont+AJFont.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/17/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit

extension UIFont
{
    class func ajCustomFontAppleSDGothicNeoRegularWithSize(_ size: CGFloat) ->  UIFont
    {
        return  UIFont(name: "AppleSDGothicNeo-Regular", size: size)!
    }
    
    class func ajCustomFontAppleSDGothicNeoSemiBoldWithSize(_ size: CGFloat) ->  UIFont
    {
        return  UIFont(name: "AppleSDGothicNeo-SemiBold", size: size)!
    }
    
    class func ajCustomFontAppleSDGothicNeoBoldWithSize(_ size: CGFloat) ->  UIFont
    {
        return  UIFont(name: "AppleSDGothicNeo-Bold", size: size)!
    }
    
    class func ajCustomFontAppleSDGothicNeoLightWithSize(_ size: CGFloat) ->  UIFont
    {
        return  UIFont(name: "AppleSDGothicNeo-Light", size: size)!
    }
    
    class func ajCustomFontAppleSDGothicNeoMediumWithSize(_ size: CGFloat) ->  UIFont
    {
        return  UIFont(name: "AppleSDGothicNeo-Medium", size: size)!
    }
}
