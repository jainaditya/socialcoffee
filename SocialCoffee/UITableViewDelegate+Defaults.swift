//
//  UITableViewDelegate+Defaults.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/18/16.
//  Copyright © 2016 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit

extension AJViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView:UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(18)
        headerView.textLabel?.textColor = UIColor.primaryColor()
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.textLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(16)
        cell.textLabel?.textColor = UIColor.primaryTextColor()
        cell.detailTextLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
    }
}
