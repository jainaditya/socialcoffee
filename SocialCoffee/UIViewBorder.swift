//
//  UIViewBorder.swift
//
//  Originally created by Aaron Ng on 12/28/13.
//  Migrated to Swift by Edouard Kaiser 12/03/14.
//  Copyright (c) 2013 Delve. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

extension UIView {
    
    //////////
    // Top
    //////////
    func createTopBorderWithHeight(_ height: CGFloat, color: UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: self.frame.size.width, height: height), color:color)
    }
    
    func createViewBackedTopBorderWithHeight(_ height: CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: self.frame.size.width, height: height), color:color)
    }
    
    func addTopBorderWithHeight(_ height: CGFloat, color:UIColor) {
        addOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: self.frame.size.width, height: height), color:color)
    }
    
    func addViewBackedTopBorderWithHeight(_ height: CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: self.frame.size.width, height: height), color:color)
    }
    
    
    //////////
    // Top + Offset
    //////////
    
    func createTopBorderWithHeight(_ height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) -> CALayer {
        // Subtract the bottomOffset from the height and the thickness to get our final y position.
        // Add a left offset to our x to get our x position.
        // Minus our rightOffset and negate the leftOffset from the width to get our endpoint for the border.
        return getOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    func createViewBackedTopBorderWithHeight(_ height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    func addTopBorderWithHeight(_ height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) {
        // Add leftOffset to our X to get start X position.
        // Add topOffset to Y to get start Y position
        // Subtract left offset from width to negate shifting from leftOffset.
        // Subtract rightoffset from width to set end X and Width.
        addOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    func addViewBackedTopBorderWithHeight(_ height:CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, topOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    
    //////////
    // Right
    //////////
    
    func createRightBorderWithWidth(_ width:CGFloat, color:UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    func createViewBackedRightBorderWithWidth(_ width:CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    func addRightBorderWithWidth(_ width:CGFloat, color:UIColor){
        addOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    func addViewBackedRightBorderWithWidth(_ width:CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    
    //////////
    // Right + Offset
    //////////
    
    func createRightBorderWithWidth(_ width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> CALayer {
        // Subtract bottomOffset from the height to get our end.
        return getOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width-rightOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func createViewBackedRightBorderWithWidth(_ width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width-rightOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func addRightBorderWithWidth(_ width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        // Subtract the rightOffset from our width + thickness to get our final x position.
        // Add topOffset to our y to get our start y position.
        // Subtract topOffset from our height, so our border doesn't extend past teh view.
        // Subtract bottomOffset from the height to get our end.
        addOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width-rightOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func addViewBackedRightBorderWithWidth(_ width: CGFloat, color:UIColor, rightOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: self.frame.size.width-width-rightOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    
    //////////
    // Bottom
    //////////
    
    func createBottomBorderWithHeight(_ height: CGFloat, color:UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(CGRect(x: 0, y: self.frame.size.height-height, width: self.frame.size.width, height: height), color:color)
    }
    
    func createViewBackedBottomBorderWithHeight(_ height: CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: 0, y: self.frame.size.height-height, width: self.frame.size.width, height: height), color:color)
    }
    
    func addBottomBorderWithHeight(_ height: CGFloat, color:UIColor) {
        return addOneSidedBorderWithFrame(CGRect(x: 0, y: self.frame.size.height-height, width: self.frame.size.width, height: height), color:color)
    }
    
    func addViewBackedBottomBorderWithHeight(_ height: CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: 0, y: self.frame.size.height-height, width: self.frame.size.width, height: height), color:color)
    }
    
    
    //////////
    // Bottom + Offset
    //////////
    
    func createBottomBorderWithHeight(_ height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) -> CALayer {
        // Subtract the bottomOffset from the height and the thickness to get our final y position.
        // Add a left offset to our x to get our x position.
        // Minus our rightOffset and negate the leftOffset from the width to get our endpoint for the border.
        return getOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: self.frame.size.height-height-bottomOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    func createViewBackedBottomBorderWithHeight(_ height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: self.frame.size.height-height-bottomOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    func addBottomBorderWithHeight(_ height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) {
        // Subtract the bottomOffset from the height and the thickness to get our final y position.
        // Add a left offset to our x to get our x position.
        // Minus our rightOffset and negate the leftOffset from the width to get our endpoint for the border.
        addOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: self.frame.size.height-height-bottomOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    func addViewBackedBottomBorderWithHeight(_ height: CGFloat, color:UIColor, leftOffset:CGFloat, rightOffset:CGFloat, bottomOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: self.frame.size.height-height-bottomOffset, width: self.frame.size.width - leftOffset - rightOffset, height: height), color:color)
    }
    
    
    
    //////////
    // Left
    //////////
    
    func createLeftBorderWithWidth(_ width: CGFloat, color:UIColor) -> CALayer {
        return getOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    func createViewBackedLeftBorderWithWidth(_ width: CGFloat, color:UIColor) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    func addLeftBorderWithWidth(_ width: CGFloat, color:UIColor) {
        addOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    func addViewBackedLeftBorderWithWidth(_ width: CGFloat, color:UIColor) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: 0, y: 0, width: width, height: self.frame.size.height), color:color)
    }
    
    
    
    //////////
    // Left + Offset
    //////////
    
    func createLeftBorderWithWidth(_ width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> CALayer {
        return getOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func createViewBackedLeftBorderWithWidth(_ width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) -> UIView {
        return getViewBackedOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    
    func addLeftBorderWithWidth(_ width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        addOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    func addViewBackedLeftBorderWithWidth(_ width:CGFloat, color:UIColor, leftOffset:CGFloat, topOffset:CGFloat, bottomOffset:CGFloat) {
        addViewBackedOneSidedBorderWithFrame(CGRect(x: 0 + leftOffset, y: 0 + topOffset, width: width, height: self.frame.size.height - topOffset - bottomOffset), color:color)
    }
    
    
    
    //////////
    // Private: Our methods call these to add their borders.
    //////////
    
    fileprivate func addOneSidedBorderWithFrame(_ frame: CGRect, color:UIColor) {
        let border = CALayer()
        border.frame = frame
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
    
    fileprivate func getOneSidedBorderWithFrame(_ frame: CGRect, color:UIColor) -> CALayer {
        let border = CALayer()
        border.frame = frame
        border.backgroundColor = color.cgColor
        return border
    }
    
    fileprivate func addViewBackedOneSidedBorderWithFrame(_ frame: CGRect, color: UIColor) {
        let border = UIView(frame: frame)
        border.backgroundColor = color
        self.addSubview(border)
    }
    
    fileprivate func getViewBackedOneSidedBorderWithFrame(_ frame: CGRect, color: UIColor) -> UIView {
        let border = UIView(frame: frame)
        border.backgroundColor = color
        return border
    }
    
}
