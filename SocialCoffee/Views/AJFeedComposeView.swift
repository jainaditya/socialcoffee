//
//  AJFeedComposeView.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 3/2/15.
//  Copyright (c) 2015 Aditya Jain. All rights reserved.
//

import UIKit
import Foundation
import Cartography

class AJFeedComposeView: UIView, UITextViewDelegate
{
    let locationIconImageView: UIImageView = UIImageView()
    let selectedPlaceLabel = UILabel()

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        createSubviews()
       // updateConstraintsIfNeeded()
//        layoutIfNeeded()
//        self.setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {
        self.locationIconImageView.image = UIImage(named: "location_pin")
        self.addSubview(self.locationIconImageView)

        self.selectedPlaceLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(16)
        self.selectedPlaceLabel.textColor = UIColor.black
        self.addSubview(self.selectedPlaceLabel)
    }

    override func updateConstraints() {
        self.locationIconImageView.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        constrain(self, self.locationIconImageView) { view, locationIconImageView in
            locationIconImageView.top == view.top + 10
            locationIconImageView.leading == view.leading + 10
            locationIconImageView.width ==  24
            locationIconImageView.height ==  24
        }
        constrain(self.locationIconImageView, self.selectedPlaceLabel) { locationIconImageView, selectedPlaceLabel in
            selectedPlaceLabel.leading == locationIconImageView.trailing
            selectedPlaceLabel.trailing == (locationIconImageView.superview?.trailing)! - 10
            selectedPlaceLabel.height == locationIconImageView.height
            selectedPlaceLabel.centerY == locationIconImageView.centerY
        }
        super.updateConstraints()
        self.layoutIfNeeded()
    }
    
    
}
