//
//  CommentTableViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/21/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class CommentTableViewCell: UITableViewCell  {
    var messageLabel: UILabel!
    var timeLabel: UILabel!
    var timeImageView:  AJImagedIconView!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier!)
        self.selectionStyle = .none
        self.isUserInteractionEnabled = true
        createSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {
        messageLabel = UILabel()
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(18)
        messageLabel.preferredMaxLayoutWidth = self.contentView.frame.size.width
        messageLabel.textColor =  UIColor.primaryTextColor()
        messageLabel.sizeToFit()
        contentView.addSubview(messageLabel)

        timeLabel = UILabel()
        timeLabel.numberOfLines = 0
        timeLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(12)
        timeLabel.textColor = UIColor.secondaryTextColor()
        contentView.addSubview(timeLabel)

        timeImageView = AJImagedIconView.init()
        timeImageView.configureFor(.time)
        contentView.addSubview(timeImageView)

        self.setNeedsUpdateConstraints()
    }

    override func updateConstraints() {
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.translatesAutoresizingMaskIntoConstraints = false

        constrain(self.contentView, messageLabel, timeLabel) { view, messageLabel, timeLabel in
            messageLabel.leading ==  view.leading + 20
            messageLabel.top == view.top + 10
            messageLabel.width == view.width - 20
        }
        constrain(timeImageView, timeLabel, messageLabel) { timeImageView, timeLabel, messageLabel in
            timeImageView.leading == messageLabel.leading
            timeImageView.width == 16
            timeImageView.height == 16
            timeImageView.top == messageLabel.bottom
            timeImageView.bottom == messageLabel.superview!.bottom - 10

            timeLabel.leading == timeImageView.trailing + 5
            timeLabel.centerY == timeImageView.centerY
        }
        super.updateConstraints()
        self.layoutIfNeeded()
    }
}
