//
//  ExploreSimpleTableViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 4/11/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography
import SwiftyJSON
import FloatRatingView

class ExploreSimpleTableViewCell: UITableViewCell {
    var venueImageView: UIImageView!
    var nameLabel: UILabel!
    var locationLabel: UILabel!
    var distanceLabel: UILabel!
    var categoryLabel: UILabel!
    var checkinsCountLabel: UILabel!
    var priceLabel: UILabel!
    var ratingLabel: UILabel!
    var ratingSignalsLabel: UILabel!
    var addButton: UIButton!
    var imageOverlayView: UIView!
    var cellSeperatorView: UIView!
    var addButtonTapped : (() -> Void)? = nil
    var progressView: UIProgressView!
    var followButton: UIButton!
    var shareButton: UIButton!
    var progressLabel: UILabel!
    var waitLabel: UILabel!
    var floatRatingView: RatingView!

    let addButtonSize = 40.0

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier!)
        self.selectionStyle = .none
        self.isUserInteractionEnabled = true
        createSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {
        venueImageView = UIImageView()
        venueImageView.contentMode = UIViewContentMode.scaleAspectFill
        venueImageView.backgroundColor = UIColor.defaultImageBackgroundColor()
        venueImageView.layer.cornerRadius = 3.0
        venueImageView.layer.masksToBounds = true
        contentView.addSubview(venueImageView)

        imageOverlayView = UIView()
        imageOverlayView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        imageOverlayView.layer.shadowOffset = CGSize(width: 500, height: 50)
        imageOverlayView.layer.shadowRadius = 10.0
        imageOverlayView.layer.masksToBounds = true
        venueImageView.addSubview(imageOverlayView)

        cellSeperatorView = UIView()
        cellSeperatorView.backgroundColor =  UIColor.groupTableViewBackground
        contentView.addSubview(cellSeperatorView)

        nameLabel = UILabel()
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(16)
        nameLabel.preferredMaxLayoutWidth = self.contentView.frame.size.width
        nameLabel.textColor =  UIColor.darkText
        nameLabel.sizeToFit()
        contentView.addSubview(nameLabel)

        categoryLabel = UILabel()
        categoryLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        categoryLabel.textColor = .gray
        categoryLabel.sizeToFit()
        contentView.addSubview(categoryLabel)

        checkinsCountLabel = UILabel()
        checkinsCountLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        checkinsCountLabel.textColor = .gray
        checkinsCountLabel.sizeToFit()
        contentView.addSubview(checkinsCountLabel)

        addButton = UIButton(type: .custom)
        addButton.backgroundColor = UIColor.trendingCategoryColor()
        addButton.layer.cornerRadius = CGFloat(addButtonSize / 2)
        addButton.setTitle("+", for: .normal)
        addButton.setTitleShadowColor(.black, for: .normal)
        addButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(30)
        addButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        addButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        addButton.layer.shadowOpacity = 1.0
        addButton.layer.shadowRadius = 0
        addButton.layer.masksToBounds = false
        addButton.contentEdgeInsets = UIEdgeInsetsMake(14, 10, 10, 10);
        addButton.addTarget(self, action:#selector(ExploreTableViewCell.addButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        contentView.addSubview(addButton)

        locationLabel = UILabel()
        locationLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        locationLabel.numberOfLines = 0
        locationLabel.preferredMaxLayoutWidth = contentView.frame.size.width
        locationLabel.textColor = UIColor.primaryTextColor()
        contentView.addSubview(locationLabel)

        ratingLabel = UILabel()
        ratingLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        ratingLabel.layer.masksToBounds = true
        ratingLabel.sizeToFit()
        ratingLabel.preferredMaxLayoutWidth = contentView.frame.size.width
        contentView.addSubview(ratingLabel)

        ratingSignalsLabel = UILabel()
        ratingSignalsLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        ratingSignalsLabel.textColor = UIColor.primaryTextColor()
        ratingSignalsLabel.textAlignment = .center
        ratingSignalsLabel.sizeToFit()
        ratingSignalsLabel.numberOfLines = 0
        ratingSignalsLabel.preferredMaxLayoutWidth = contentView.frame.size.width
        contentView.addSubview(ratingSignalsLabel)

        distanceLabel = UILabel()
        distanceLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(14)
        distanceLabel.numberOfLines = 0
        distanceLabel.preferredMaxLayoutWidth = contentView.frame.size.width
        distanceLabel.textColor = .gray
        distanceLabel.textAlignment = .right
        contentView.addSubview(distanceLabel)

        priceLabel = UILabel()
        priceLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(14)
        priceLabel.numberOfLines = 0
        priceLabel.preferredMaxLayoutWidth = contentView.frame.size.width
        priceLabel.textColor = .gray
        priceLabel.sizeToFit()
        contentView.addSubview(priceLabel)
        priceLabel.textAlignment = .right

        addProgressView()
        addFollowButton()
        addShareButton()
        addProgressLabel()
//        addWaitLabel()
        addStarRatingView()
        self.setNeedsUpdateConstraints()

    }

    func addStarRatingView() {
        floatRatingView = RatingView()
        contentView.addSubview(floatRatingView)
    }

    func addProgressView() {
        progressView = UIProgressView(progressViewStyle: .bar)
        //        rgb(233, 30, 99)
        let color = UIColor.white
        //            UIColor.init(red: 233.0/255, green: 30.0/255, blue: 99.0/255, alpha: 1)
        progressView.progressTintColor = color
        progressView.trackTintColor = color.withAlphaComponent(0.3)
        imageOverlayView.addSubview(progressView!)
    }

    func addFollowButton() {
        followButton = UIButton()
        followButton.setImage(UIImage(named: "follow"), for: .normal)
        followButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(16.0)
        followButton.setTitleColor(UIColor.gray, for: .normal)
        contentView.addSubview(followButton)
    }

    func addShareButton() {
        shareButton = UIButton()
        shareButton.setImage(UIImage(named: "share"), for: .normal)
        shareButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(16.0)
        shareButton.setTitleColor(UIColor.gray, for: .normal)
        contentView.addSubview(shareButton)
    }

    func addProgressLabel() {
        progressLabel = UILabel()
        progressLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(12.0)
        progressLabel.textColor = .white
        progressLabel.textAlignment = .center
        imageOverlayView.addSubview(progressLabel)
    }

    func addWaitLabel() {

    }

    override func updateConstraints() {
        venueImageView.translatesAutoresizingMaskIntoConstraints = false
        imageOverlayView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        locationLabel.translatesAutoresizingMaskIntoConstraints = false
        cellSeperatorView.translatesAutoresizingMaskIntoConstraints = false
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        checkinsCountLabel.translatesAutoresizingMaskIntoConstraints = false
        distanceLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        ratingLabel.translatesAutoresizingMaskIntoConstraints = false
        ratingSignalsLabel.translatesAutoresizingMaskIntoConstraints = false

        constrain(contentView, venueImageView) { view, venueImageView in
            venueImageView.leading == view.leading + 5
            venueImageView.top == view.top + 5
            venueImageView.height == 100
            venueImageView.width == 100
        }

        constrain(imageOverlayView) { imageOverlayView in
            imageOverlayView.edges == (imageOverlayView.superview?.edges)!
            imageOverlayView.bottom == (imageOverlayView.superview?.bottom)!
        }

        constrain(nameLabel, venueImageView) { nameLabel, venueImageView in
            nameLabel.top == venueImageView.top
            nameLabel.leading == venueImageView.trailing + 5
            nameLabel.trailing == nameLabel.superview!.trailing - 50
        }

        constrain(nameLabel, distanceLabel, priceLabel) { nameLabel, distanceLabel, priceLabel in
            distanceLabel.leading == nameLabel.trailing + 5
            distanceLabel.top == nameLabel.top
            distanceLabel.trailing == distanceLabel.superview!.trailing - 5
            priceLabel.leading == nameLabel.trailing + 5
            priceLabel.trailing == distanceLabel.trailing
            distribute(by: 0, vertically: distanceLabel, priceLabel)
        }

        constrain(categoryLabel, nameLabel, checkinsCountLabel, floatRatingView, locationLabel) { categoryLabel, nameLabel, checkinsCountLabel, floatRatingView, locationLabel in
            categoryLabel.leading == nameLabel.leading
            checkinsCountLabel.leading == nameLabel.leading
            floatRatingView.leading == nameLabel.leading
            floatRatingView.top == nameLabel.bottom
            locationLabel.top == floatRatingView.bottom + 5
            locationLabel.leading == nameLabel.leading
            floatRatingView.width == 70
            floatRatingView.height == 12
            distribute(by: 0, vertically: locationLabel, checkinsCountLabel, categoryLabel)
        }

        constrain(progressView) { checkinsCountLabel in
            checkinsCountLabel.bottom == checkinsCountLabel.superview!.bottom
            checkinsCountLabel.leading == checkinsCountLabel.superview!.leading
            checkinsCountLabel.height == 10
            checkinsCountLabel.trailing == checkinsCountLabel.superview!.trailing
        }

        constrain(venueImageView, addButton) { venueImageView, addButton in
            addButton.top == venueImageView.bottom - CGFloat(addButtonSize / 2)
            addButton.trailing == addButton.superview!.trailing - 10
            addButton.width == CGFloat(addButtonSize)
            addButton.height == CGFloat(addButtonSize)
        }

        constrain(floatRatingView, ratingSignalsLabel) { floatRatingView, ratingSignalsLabel in
            ratingSignalsLabel.leading == floatRatingView.trailing + 5
            ratingSignalsLabel.top == floatRatingView.top
        }

        constrain(venueImageView, cellSeperatorView) { categoryLabel, cellSeperatorView in
            distribute(by: 5, vertically: categoryLabel, cellSeperatorView)
        }

        constrain(cellSeperatorView) { cellSeperatorView in
            cellSeperatorView.bottom == (cellSeperatorView.superview?.bottom)!
            cellSeperatorView.height == 10
            cellSeperatorView.leading == (cellSeperatorView.superview?.leading)!
            cellSeperatorView.trailing == (cellSeperatorView.superview?.trailing)!
        }

        constrain(progressView, progressLabel) { progressView, progressLabel in
            progressLabel.leading == progressLabel.superview!.leading + 5
            progressLabel.width == 40
            progressLabel.bottom == progressView.top - 5
        }

        super.updateConstraints()
        self.layoutIfNeeded()
    }

    //TODO: Refactor to move the logic in individual content providers
    func populateCellWithPlace(place: AJPlaceFourSquare, type: String, placeMetrics: JSON?) {
        nameLabel.text = place.name
        if place.venuePhotoUrl.characters.count > 0  {
            let url = place.venuePhotoUrl.replacingOccurrences(of: "900x350", with: "100x100")
            venueImageView.af_setImage(withURL: URL(string: url)!, placeholderImage: UIImage(named: "tab_home") , filter: nil, imageTransition: .crossDissolve(0.2),runImageTransitionIfCached: false)
        } else {
            venueImageView.image =  UIImage(named: "icon-pattern")
        }

        var priceStr = ""
        for _ in 0..<place.priceTier {
            priceStr += "$"
        }
        priceLabel.text = priceStr
//        ratingLabel.text = String(format: "%.1f", place.rating)
        floatRatingView.rating = place.rating / 2
        ratingSignalsLabel.text = String(format: "%i reviews", place.ratingSignals )
        distanceLabel.text = String(format: "%.1f mi", place.distanceInMile)
        categoryLabel.text = place.category

        if place.checkinsCount > 0 {
            let checkins = place.checkinsCount
            if checkins > 10 && checkins < 1000 {
                checkinsCountLabel.text = String(format: "%i people checked in here", checkins)
            } else if checkins > 1000 {
                checkinsCountLabel.text = "1000+ people checked in here"
            }
            if type == Constants.PlaceType.Food {
                //                checkinsCountLabel.backgroundColor = UIColor.foodCategoryColor().withAlphaComponent(0.6)
                addButton.backgroundColor = UIColor.foodCategoryColor()
            } else if type == Constants.PlaceType.Drinks {
                //                checkinsCountLabel.backgroundColor = UIColor.drinkCategoryColor().withAlphaComponent(0.6)
                addButton.backgroundColor = UIColor.drinkCategoryColor()
            } else if type == Constants.PlaceType.Coffee {
                //                checkinsCountLabel.backgroundColor = UIColor.cafeCategoryColor().withAlphaComponent(0.6)
                addButton.backgroundColor = UIColor.cafeCategoryColor()
            } else {
                //                checkinsCountLabel.backgroundColor = UIColor.trendingCategoryColor().withAlphaComponent(0.6)
                addButton.backgroundColor = UIColor.trendingCategoryColor()
            }
        }
        progressView.progressTintColor = addButton.backgroundColor
        var _progress = Float(arc4random()) / Float(UINT32_MAX) / 10
        if let _placeMetrics = placeMetrics {
            if _placeMetrics[place.id].dictionary != nil {
                let crowded = _placeMetrics[place.id]["crowd"].floatValue
                print("\(place.name): \(crowded)")
                let crowdCountMaxScore = _placeMetrics[place.id]["crowdCount"].floatValue * 3
                _progress = Float(crowded/crowdCountMaxScore);
                if  crowded > 0 {
                    progressView.setProgress(_progress, animated: true)
                }

            } else {
                progressView.setProgress(_progress, animated: true)
            }
        } else {
            progressView.setProgress(_progress, animated: true)
        }
        updateProgressLabel(progress: _progress * 100)

        locationLabel.text = place.vicinity
    }

    func addButtonClicked(_ sender: UIButton) {

        if let addButtonTapped = self.addButtonTapped {
            addButtonTapped()
        }
        
    }
    
    func updateProgressLabel(progress: Float) {
        var progressText = "Calm"
        var bgColor = UIColor.calmColor()
        if progress > 32 && progress < 68 {
            progressText = "Busy"
            bgColor = UIColor.busyColor()
        } else if progress > 67 {
            progressText = "Crowded"
            bgColor = UIColor.crowdedColor()
        }
        progressLabel.text = progressText
        progressLabel.backgroundColor = bgColor
        progressView.progressTintColor = bgColor
    }
}

