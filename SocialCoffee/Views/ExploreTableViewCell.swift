//
//  ExploreTableViewCell.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/24/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography
import SwiftyJSON

class ExploreTableViewCell: UITableViewCell {
    var venueImageView: UIImageView!
    var nameLabel: UILabel!
    var locationLabel: UILabel!
    var distanceLabel: UILabel!
    var categoryLabel: UILabel!
    var checkinsCountLabel: UILabel!
    var priceLabel: UILabel!
    var ratingLabel: UILabel!
    var ratingSignalsLabel: UILabel!
    var addButton: UIButton!
    var imageOverlayView: UIView!
    var cellSeperatorView: UIView!
    var addButtonTapped : (() -> Void)? = nil
    var progressView: UIProgressView!
    var followButton: UIButton!
    var shareButton: UIButton!
    var progressLabel: UILabel!
    var waitLabel: UILabel!
    var floatRatingView: RatingView!
    
    let addButtonSize = 40.0

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier!)
        self.selectionStyle = .none
        self.isUserInteractionEnabled = true
        createSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {
        venueImageView = UIImageView()
        venueImageView.contentMode = UIViewContentMode.scaleAspectFill
//        venueImageView.clipsToBounds =  true
        venueImageView.backgroundColor = UIColor.defaultImageBackgroundColor()
        contentView.addSubview(venueImageView)

        imageOverlayView = UIView()
        imageOverlayView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        imageOverlayView.layer.shadowColor = UIColor.yellow.withAlphaComponent(0.7).cgColor
        imageOverlayView.layer.shadowOffset = CGSize(width: 500, height: 50)
        imageOverlayView.layer.shadowRadius = 10.0
        imageOverlayView.layer.masksToBounds = true
        venueImageView.addSubview(imageOverlayView)

        cellSeperatorView = UIView()
        cellSeperatorView.backgroundColor =  UIColor.groupTableViewBackground
        contentView.addSubview(cellSeperatorView)

        nameLabel = UILabel()
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(22)
        nameLabel.preferredMaxLayoutWidth = self.contentView.frame.size.width
        nameLabel.textColor =  .white
        nameLabel.sizeToFit()
        nameLabel.textAlignment = .center
        imageOverlayView.addSubview(nameLabel)

        categoryLabel = UILabel()
        categoryLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(14)
        categoryLabel.textColor = .white
        categoryLabel.textAlignment = .center
        categoryLabel.sizeToFit()
        imageOverlayView.addSubview(categoryLabel)

        checkinsCountLabel = UILabel()
        checkinsCountLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(14)
        checkinsCountLabel.textColor = .white
        checkinsCountLabel.textAlignment = .center
        checkinsCountLabel.sizeToFit()
        imageOverlayView.addSubview(checkinsCountLabel)

        addButton = UIButton(type: .custom)
        addButton.backgroundColor = UIColor.trendingCategoryColor()
        addButton.layer.cornerRadius = CGFloat(addButtonSize / 2)
        addButton.setTitle("+", for: .normal)
        addButton.setTitleShadowColor(.black, for: .normal)
        addButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(30)
        addButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        addButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        addButton.layer.shadowOpacity = 1.0
        addButton.layer.shadowRadius = 0
        addButton.layer.masksToBounds = false
//        addButton.contentVerticalAlignment = .fill
        addButton.contentEdgeInsets = UIEdgeInsetsMake(14, 10, 10, 10);
        addButton.addTarget(self, action:#selector(ExploreTableViewCell.addButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        contentView.addSubview(addButton)

        locationLabel = UILabel()
        locationLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        locationLabel.numberOfLines = 0
        locationLabel.preferredMaxLayoutWidth = contentView.frame.size.width
        contentView.addSubview(locationLabel)

        ratingLabel = UILabel()
        ratingLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(14)
        ratingLabel.textAlignment = .center
        ratingLabel.textColor = .white
        ratingLabel.backgroundColor =  UIColor.cafeCategoryColor()
        ratingLabel.layer.cornerRadius = 4.0
        ratingLabel.layer.masksToBounds = true
        ratingLabel.sizeToFit()
        contentView.addSubview(ratingLabel)

        ratingSignalsLabel = UILabel()
        ratingSignalsLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(14)
        ratingSignalsLabel.textColor = UIColor.primaryTextColor()
        ratingSignalsLabel.textAlignment = .center
        ratingSignalsLabel.sizeToFit()
        contentView.addSubview(ratingSignalsLabel)

        distanceLabel = UILabel()
        distanceLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(14)
        distanceLabel.numberOfLines = 0
        distanceLabel.preferredMaxLayoutWidth = contentView.frame.size.width
        distanceLabel.textColor = .gray
        contentView.addSubview(distanceLabel)
        contentView.frame = self.frame

        addProgressView()
        addWaitLabel()
        addFollowButton()
        addShareButton()
        addProgressLabel()
        addStarRatingView()
        self.setNeedsUpdateConstraints()

    }

    func addStarRatingView() {
        floatRatingView = RatingView()
        contentView.addSubview(floatRatingView)
    }

    func addProgressView() {
        progressView = UIProgressView(progressViewStyle: .bar)
//        rgb(233, 30, 99)
        let color = UIColor.white
//            UIColor.init(red: 233.0/255, green: 30.0/255, blue: 99.0/255, alpha: 1)
        progressView.progressTintColor = color
        progressView.trackTintColor = color.withAlphaComponent(0.3)
        imageOverlayView.addSubview(progressView!)
    }

    func addFollowButton() {
        followButton = UIButton()
        followButton.setImage(UIImage(named: "follow"), for: .normal)
        followButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(16.0)
        followButton.setTitleColor(UIColor.gray, for: .normal)
        contentView.addSubview(followButton)
    }

    func addShareButton() {
        shareButton = UIButton()
        shareButton.setImage(UIImage(named: "share"), for: .normal)
        shareButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(16.0)
        shareButton.setTitleColor(UIColor.gray, for: .normal)
        contentView.addSubview(shareButton)
    }

    func addProgressLabel() {
        progressLabel = UILabel()
        progressLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(16.0)
        progressLabel.textColor = .white
        progressLabel.textAlignment = .center
        imageOverlayView.addSubview(progressLabel)
    }

    func addWaitLabel() {
        waitLabel = UILabel()
        waitLabel.font = UIFont.ajCustomFontAppleSDGothicNeoMediumWithSize(16.0)
        waitLabel.textColor = .white
        waitLabel.textAlignment = .center
        imageOverlayView.addSubview(waitLabel)
    }

    override func updateConstraints() {
        venueImageView.translatesAutoresizingMaskIntoConstraints = false
        imageOverlayView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        locationLabel.translatesAutoresizingMaskIntoConstraints = false
        cellSeperatorView.translatesAutoresizingMaskIntoConstraints = false
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        checkinsCountLabel.translatesAutoresizingMaskIntoConstraints = false

        constrain(contentView, venueImageView) { view, venueImageView in
            venueImageView.leading == view.leading
            venueImageView.top == view.top
            venueImageView.height == 200
            venueImageView.width == view.width
        }
        constrain(imageOverlayView) { imageOverlayView in
            imageOverlayView.edges == (imageOverlayView.superview?.edges)!
            imageOverlayView.bottom == (imageOverlayView.superview?.bottom)!
        }
        constrain(nameLabel) {  nameLabel in
            nameLabel.top == (nameLabel.superview?.top)! + 10
            nameLabel.leading == nameLabel.superview!.leading + 10
            nameLabel.trailing == nameLabel.superview!.trailing - 10
        }

        constrain(categoryLabel, nameLabel, checkinsCountLabel) { categoryLabel, nameLabel, checkinsCountLabel in
            categoryLabel.centerX ==  nameLabel.centerX
            categoryLabel.width == nameLabel.width
            checkinsCountLabel.centerX == nameLabel.centerX
            checkinsCountLabel.height == 25
            distribute(by: 0, vertically: nameLabel, categoryLabel, checkinsCountLabel)

        }

        constrain(progressView) { checkinsCountLabel in
            checkinsCountLabel.bottom == checkinsCountLabel.superview!.bottom
            checkinsCountLabel.leading == checkinsCountLabel.superview!.leading
            checkinsCountLabel.height == 10
            checkinsCountLabel.trailing == checkinsCountLabel.superview!.trailing
        }

        constrain(venueImageView, addButton) { venueImageView, addButton in
            addButton.top == venueImageView.bottom - CGFloat(addButtonSize / 2)
            addButton.trailing == addButton.superview!.trailing - 10
            addButton.width == CGFloat(addButtonSize)
            addButton.height == CGFloat(addButtonSize)
        }

        constrain(venueImageView, floatRatingView, locationLabel, ratingSignalsLabel) { venueImageView, floatRatingView, locationLabel, ratingSignalsLabel in
            floatRatingView.top == venueImageView.bottom + 10
            floatRatingView.leading == floatRatingView.superview!.leading + 10
            floatRatingView.width == 70
            floatRatingView.height == 12
            ratingSignalsLabel.leading == floatRatingView.trailing + 5
            ratingSignalsLabel.top == floatRatingView.top
            locationLabel.leading == floatRatingView.leading
            locationLabel.top == floatRatingView.bottom + 5
        }

//        constrain(floatRatingView, locationLabel, ratingSignalsLabel) { ratingLabel, locationLabel, ratingSignalsLabel in
//            locationLabel.leading == ratingLabel.trailing + 5
//            ratingSignalsLabel.leading == locationLabel.leading
//            locationLabel.top == ratingLabel.top
//            distribute(by: 0, vertically: locationLabel, ratingSignalsLabel)
//        }

        constrain(locationLabel, cellSeperatorView) { locationLabel, cellSeperatorView in
            distribute(by: 5, vertically: locationLabel, cellSeperatorView)
        }

        constrain(cellSeperatorView) { cellSeperatorView in
            cellSeperatorView.bottom == (cellSeperatorView.superview?.bottom)!
            cellSeperatorView.height == 10
            cellSeperatorView.leading == (cellSeperatorView.superview?.leading)!
            cellSeperatorView.trailing == (cellSeperatorView.superview?.trailing)!
        }

        constrain(progressView, progressLabel) { progressView, progressLabel in
            progressLabel.leading == progressLabel.superview!.leading + 10
            progressLabel.width == 70
            progressLabel.bottom == progressView.top - 10
        }
        constrain(waitLabel, progressLabel) { waitLabel, progressLabel in
            waitLabel.leading == progressLabel.superview!.leading + 10
//            waitLabel.width == 70
            waitLabel.bottom == progressLabel.top - 10
        }
        super.updateConstraints()
        self.layoutIfNeeded()
    }

    //TODO: Refactor to move the logic in individual content providers
    func populateCellWithPlace(place: AJPlaceFourSquare, type: String, placeMetrics: JSON?) {
        nameLabel.text = place.name
        if place.venuePhotoUrl.characters.count > 0  {
            venueImageView.af_setImage(withURL: URL(string: place.venuePhotoUrl)!, placeholderImage: UIImage(named: "tab_home") , filter: nil, imageTransition: .crossDissolve(0.2),runImageTransitionIfCached: false)
        } else {
            venueImageView.image =  UIImage(named: "icon-pattern")
        }
        var infoString = String()
        if place.category.characters.count > 0 {
//            categoryLabel.text = place.category
//            categoryLabel.backgroundColor = UIColor.foodCategoryColor()
            infoString = place.category + " · "
        }
        var priceStr = "$"
        if place.priceTier > 1 {
            for _ in 2...place.priceTier {
                priceStr += "$"
            }
        }
        infoString += priceStr + " · "
//        ratingLabel.text = String(format: "%.1f", place.rating)
        floatRatingView.rating = place.rating / 2
        ratingSignalsLabel.text = String(format: "%i reviews", place.ratingSignals )
        infoString += String(format: "%.1f mi", place.distanceInMile)
        distanceLabel.text = String(format: "%.1f mi", place.distanceInMile)
        categoryLabel.text = infoString

        if place.checkinsCount > 0 {
            let checkins = place.checkinsCount
            if checkins > 10 && checkins < 1000 {
                checkinsCountLabel.text = String(format: "%i people checked in here", checkins)
            } else if checkins > 1000 {
                checkinsCountLabel.text = "1000+ people checked in here"
            }
        }
        if type == Constants.PlaceType.Food {
            addButton.backgroundColor = UIColor.foodCategoryColor()
        } else if type == Constants.PlaceType.Drinks {
            addButton.backgroundColor = UIColor.drinkCategoryColor()
        } else if type == Constants.PlaceType.Coffee {
            addButton.backgroundColor = UIColor.cafeCategoryColor()
        } else {
            addButton.backgroundColor = UIColor.trendingCategoryColor()
        }
        progressView.progressTintColor = addButton.backgroundColor
        var _progress = Float(arc4random()) / Float(UINT32_MAX) / 10
        if let _placeMetrics = placeMetrics {
            if _placeMetrics[place.id].dictionary != nil {
                let crowded = _placeMetrics[place.id]["crowd"].floatValue
                let crowdCountMaxScore = _placeMetrics[place.id]["crowdCount"].floatValue * 3
                _progress = Float(crowded/crowdCountMaxScore);
                if  crowded > 0 {
                    progressView.setProgress(_progress, animated: true)
                }

            } else {
                progressView.setProgress(_progress, animated: true)
            }
            if let waitTime = _placeMetrics[place.id]["waitTime"].int64 {
                if waitTime > 0 {
                    let count = _placeMetrics[place.id]["waitTimeCount"].int64Value
                    waitLabel.text = PlaceInfoProvider.getWaitTimeText(waitTime: waitTime, waitTimeCount: count)
                }
            }
        } else {
            progressView.setProgress(_progress, animated: true)
        }
        updateProgressLabel(progress: _progress * 100)

        locationLabel.text = place.vicinity
    }

    func addButtonClicked(_ sender: UIButton) {

        if let addButtonTapped = self.addButtonTapped {
            addButtonTapped()
        }

    }

    func updateProgressLabel(progress: Float) {
        var progressText = "Calm"
        var bgColor = UIColor.calmColor()
        if progress > 32 && progress < 68 {
            progressText = "Busy"
            bgColor = UIColor.busyColor()
        } else if progress > 67 {
            progressText = "Crowded"
            bgColor = UIColor.crowdedColor()
        }
        progressLabel.text = progressText
        progressLabel.backgroundColor = bgColor
        progressView.progressTintColor = bgColor
    }
}
