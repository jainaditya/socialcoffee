//
//  PlaceCategoryCollectionView.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/29/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit

class PlaceCategoryCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.register(AJPlaceCategoryCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(AJPlaceCategoryCollectionViewCell.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
