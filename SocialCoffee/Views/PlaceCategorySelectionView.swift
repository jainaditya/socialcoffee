//
//  PlaceCategorySelectionView.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 1/29/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import Cartography

class PlaceCategorySelectionView: UIView {
    var collectionView: UICollectionView!
    var placeCategoryContentProvider: AJPlaceCategoryContentProvider!
    var overlayView: UIView!
    var selectInfoLabel: UILabel!
    var continueButton: UIButton!
    let buttonHeight: Int = 40
    var placeCategoryCollectionView: PlaceCategoryCollectionView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func initializeSubviews() {
        let bgImageView = UIImageView(frame: self.frame)
        bgImageView.image = UIImage(named: "bg_city_blur")
        addSubview(bgImageView)
        overlayView = UIView(frame: self.frame)
        overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        addSubview(overlayView)

        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        placeCategoryCollectionView = PlaceCategoryCollectionView(frame: frame, collectionViewLayout: flowLayout)
        addSubview(placeCategoryCollectionView)
        placeCategoryCollectionView.backgroundColor = UIColor.clear
        placeCategoryCollectionView.alwaysBounceVertical =  true
        selectInfoLabel = UILabel()
        addSubview(selectInfoLabel)
        selectInfoLabel.font = UIFont.ajCustomFontAppleSDGothicNeoRegularWithSize(18)
        selectInfoLabel.textColor = UIColor.white
        selectInfoLabel.text = "Select the places of your interest for personalized experience."
        selectInfoLabel.numberOfLines = 0

        continueButton = UIButton()
        continueButton.setTitle("Save & Continue...", for: UIControlState.normal)
        continueButton.titleLabel?.font = UIFont.ajCustomFontAppleSDGothicNeoSemiBoldWithSize(18)
        continueButton.backgroundColor = UIColor.primaryColor().withAlphaComponent(0.9)
//        continueButton.layer.borderColor = UIColor.whiteColor().CGColor
//        continueButton.layer.borderWidth = CGFloat(1)
//        continueButton.layer.cornerRadius = CGFloat(buttonHeight/2)
        addSubview(continueButton)
        setNeedsUpdateConstraints()
    }

    override func updateConstraints() {
        placeCategoryCollectionView.translatesAutoresizingMaskIntoConstraints = false
        selectInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        continueButton.translatesAutoresizingMaskIntoConstraints = false

        constrain(self, selectInfoLabel) { view, selectInfoLabel in
            selectInfoLabel.top == view.centerY - self.frame.size.height / 3
            selectInfoLabel.leading == view.leading + 20
            selectInfoLabel.width == view.width - 40
        }
        constrain(self, continueButton, selectInfoLabel) { view, continueButton, selectInfoLabel in
            continueButton.bottom == view.bottom - 20
            continueButton.leading == selectInfoLabel.leading
            continueButton.width == selectInfoLabel.width
            continueButton.height == 40
        }
        constrain(continueButton, selectInfoLabel, placeCategoryCollectionView) { continueButton, selectInfoLabel, collectionView in
            collectionView.leading == selectInfoLabel.leading
            collectionView.width == selectInfoLabel.width
            collectionView.top == selectInfoLabel.bottom + 20
            collectionView.bottom == continueButton.top - 20
        }

        super.updateConstraints()
        self.layoutIfNeeded()
    }
}
