//
//  PlaceLiveFeedHeaderView.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 2/28/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class PlaceLiveFeedHeaderView: UIView {
    var nameLabel: UILabel!
    var titleLabel: UILabel!
    var venueImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        createSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func createSubviews() {
        venueImageView = UIImageView()
        venueImageView.contentMode = UIViewContentMode.scaleAspectFill
        venueImageView.clipsToBounds =  true
        venueImageView.backgroundColor = UIColor.defaultImageBackgroundColor()
        venueImageView.layer.cornerRadius = 50
        venueImageView.layer.borderWidth = 2
        venueImageView.layer.borderColor = UIColor.white.cgColor
        self.addSubview(venueImageView)

        nameLabel = UILabel()
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(20)
        nameLabel.preferredMaxLayoutWidth = self.frame.size.width
        nameLabel.textColor =  .white
        nameLabel.sizeToFit()
        nameLabel.textAlignment = .center

        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.ajCustomFontAppleSDGothicNeoBoldWithSize(20)
        titleLabel.preferredMaxLayoutWidth = self.frame.size.width
        titleLabel.textColor =  .white
        titleLabel.sizeToFit()
        titleLabel.textAlignment = .center
        titleLabel.backgroundColor = .clear
        self.addSubview(titleLabel)

        setNeedsUpdateConstraints()
    }

    override func updateConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        venueImageView.translatesAutoresizingMaskIntoConstraints = false

        constrain(self, venueImageView) { view, venueImageView in
            venueImageView.top == view.top + 10
            venueImageView.height == 100
            venueImageView.width == 100
            venueImageView.centerX == view.centerX
        }

        constrain(titleLabel, venueImageView) { titleLabel, venueImageView in
            titleLabel.width == titleLabel.superview!.width
            titleLabel.leading == titleLabel.superview!.leading
            titleLabel.height == 25
            titleLabel.top == venueImageView.bottom + 45
        }

        super.updateConstraints()
        self.layoutIfNeeded()
    }

    func populatePlaceInfo(name: String, title: String, bgUrl: String) {
        nameLabel.text = name
        titleLabel.text = title
        venueImageView.af_setImage(withURL: URL(string: bgUrl)!, placeholderImage: UIImage(named: "tab_home") , filter: nil, imageTransition: .crossDissolve(0.2),runImageTransitionIfCached: false)
    }
}
