//
//  RatingView.swift
//  SocialCoffee
//
//  Created by Aditya Jain on 4/13/17.
//  Copyright © 2017 Aditya Jain. All rights reserved.
//

import Foundation
import FloatRatingView

class RatingView: FloatRatingView {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required init(frame: CGRect) {
        super.init(frame: frame)
        // Required float rating view params
        self.emptyImage = UIImage(named: "star")
        self.fullImage = UIImage(named: "star-filled")

        // Segmented control init
        self.contentMode = UIViewContentMode.scaleAspectFit
        self.maxRating = 5
        self.minRating = 1
        self.rating = 2.5
        self.editable = false
        self.halfRatings = false
        self.floatRatings = true
    }
}
